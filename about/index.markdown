---
layout: titlepage
title: About Me
---

<div align="center">
<figure>
<img
src="/assets/img/mr_math_lambdaman.jpg" />
<figcaption>
Mr. Math as Lambdaman, wants YOU for Functional Programming (FP).
</figcaption>
</figure>
</div>

I am **Mr. Math**iesen (Ramón Soto Mathiesen), a passionate **datalogist** (*),
with flair for functional programming languages and business, that advocates for
correctness, quality and high-standards but, always with the customer in mind.

> (*) At the University of Copenhagen (KU), we don't study Computer Science, but
> rather **Datatology** ("data" as information or facts about something and
> "logos" (Greek) meaning study). This is because **Peter Naur**, the founder of
> the (first) department at KU and Denmark's only Turing Award winner, came up
> with the term after listening to his good friend **Edsger Dijkstra** when he
> said: _«Computer Science is no more about computers than astronomy is about
> telescopes»_. You are introduced to this concept already from the first course
> of the first semester. This is what we do and who we are (data is EVERYTHING
> and everything is DATA). Therefore, we are **not computer scientists** but
> rather **datalogists**.

<div align="center">
<figure>
<img
src="/assets/img/mr_math_friedman.jpg" />
<figcaption>
I was baptized Mr. Math at ICFP by Daniel P. Friedman, Professor at Indiana University.
</figcaption>
</figure>
</div>

In 2016-09-01, I created my own company: [SPISE MISU
ApS](http://spisemisu.com/), that will try to make software the way I see
it. Here's a LinkedIn post I wrote on the matter: [A Farewell to Delegate A/S
and Welcome SPISE MISU
ApS](https://www.linkedin.com/pulse/farewell-delegate-welcome-spise-misu-aps-ramón-soto-mathiesen).

As stated in the LinkedIn post, I had been working for the last 3 and half
years, before I created my company, with the [Microsoft Dynamics
CRM](http://crm.dynamics.com/) platform as a Managing Specialist @ [Delegate
A/S](http://www.delegate.dk/) where I among other things, contributed to start a
whole new CRM department from scratch and got promoted as the CTO of the CRM
department being responsible for the technical vision and solutions implemented
on customer projects. This was achieved through a variety of tools:
**XrmFramework**, **DAXIF#**, ... which under my leadership were maintained and
expanded with new and robust applications that provided a competitive advantage
over other CRM Partners and ultimately ensured savings of unnecessary manual
tasks as reflected in a lower price for the customers. The ultimate goal was
that the customers got the best possible return on investment. Info on tooling
can see seen on GitHub: [Delegate A/S @ GitHub](http://delegateas.github.io/)

* Education: I have a masters degree in computer science, with minors in
mathematics, from the University of Copenhagen. I was one semester abroad at the
University of Pisa, where I was introduced, to among others, Algorithmics as
Algorithm Engineering, which became the main topic of my Master’s thesis. For
more information on my thesis, please visit the [Performance Engineering Lab
website at the Copenhagen
University](http://www.diku.dk/forskning/performance-engineering/Ramon/)

* Background: I been around for a while and have tried almost every position
there is in IT. Starting as an IT-supporter back in 2000, to a System
Administrator, a Web Developer, a Kernel Developer, a Consultant, a Software
Architect, a CTO of a department and at the moment, a founder of an IT
company. While been in these positions I have done a variety of task related to
network, infrastructure, software development, consulting, teaching, technical
leadership and probably the most important of them all: inspiring.

* Founder of MF#K: F#unctional Copenhageners Meetup Group will try to get more
and more software projects to be based on functional programming languages. We
mainly focus on F# and Haskell, but other functional programming languages like
Scala, Lisp, Erlang, Clojure, etc. are more than welcome. For more information,
please visit [Mødegruppe for F#unktionelle
Københavnere](http://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/)

  - [Prosa Interview (Danish):](https://www.prosa.dk/artikel/hvorfor-proever-du-ikke-et-funktionelt-sprog/)
  
  - [Prosa Magazine (Danish):](/assets/about/2016-04-01_prosa-lambda-pages-26-30.pdf)

<div align="center">
<figure>
<a href="https://www.fsf.org/" target="_blank">
<img
src="http://static.fsf.org/nosvn/associate/crm/171169.png" />
</a>
</figure>
</div>
