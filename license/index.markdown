---
layout: titlepage
title: License Information
---

This blog is Open Source and available under [copyleft][copyleft]. You can
obtain the source code from its [Gitlab repository][gitlab].

### Code Examples

All code examples are released under [AGPL-3.0][agpl3].

### Other Content

The blog posts and any other textual content on this site are licensed under the
[CC BY-SA 4.0][ccbysa4].

[gitlab]:   https://gitlab.com/gentauro/gentauro.gitlab.io
[copyleft]: https://copyleft.org/
[agpl3]:    https://www.gnu.org/licenses/agpl.html
[ccbysa4]:  https://creativecommons.org/licenses/by-sa/4.0/
