--- 
layout: post
title: F# - MS CRM Online Stress Test (±150.000 accounts/hour)
categories:
  - English
tags:
  - snippet
  - f#
time: "21:28"
---

### NuGet Snippet:

{% highlight text %}
C:\_tmp\stressTest>nuget install -ExcludeVersion Microsoft.CrmSdk.CoreAssemblies
Attempting to resolve dependency 'Microsoft.IdentityModel (= 6.1.7600.16394)'.
Installing 'Microsoft.IdentityModel 6.1.7600.16394'.
Successfully installed 'Microsoft.IdentityModel 6.1.7600.16394'.
Installing 'Microsoft.CrmSdk.CoreAssemblies 6.1.0'.
Successfully installed 'Microsoft.CrmSdk.CoreAssemblies 6.1.0'.

C:\_tmp\stressTest>
{% endhighlight %}

{% highlight text %}
C:\_tmp\stressTest>nuget install -ExcludeVersion FSharp.Data
Attempting to resolve dependency 'Zlib.Portable (= 1.9.2)'.
Installing 'Zlib.Portable 1.9.2'.
Successfully installed 'Zlib.Portable 1.9.2'.
Installing 'FSharp.Data 2.0.4'.
Successfully installed 'FSharp.Data 2.0.4'.

C:\_tmp\stressTest>
{% endhighlight %}

### Script Snippet (DG.StressTest.cmd):

{% highlight bat %}
@echo off

:: Add the paths for the F# SDK 3.x (from higher version to lower)
set FSHARPSDK=^
C:\Program Files (x86)\Microsoft SDKs\F#\3.1\Framework\v4.0\;^
C:\Program Files (x86)\Microsoft SDKs\F#\3.0\Framework\v4.0\

cls

:: Execute the script "only" with the first "fsianycpu.exe" found
for %%i in (fsianycpu.exe) do "%%~$FSHARPSDK:i" DG.StressTest.fsx %*

pause
{% endhighlight %}

### Code Snippet (DG.StressTest.fsx):

{% highlight ocaml linenos %}
#r @"System.Runtime.Serialization"
#r @"System.ServiceModel"
 
// nuget install -ExcludeVersion Microsoft.CrmSdk.CoreAssemblies
#r @"Microsoft.CrmSdk.CoreAssemblies\lib\net40\Microsoft.Xrm.Sdk.dll"
 
// nuget install -ExcludeVersion FSharp.Data
#r @"FSharp.Data\lib\net40\FSharp.Data.dll"
 
#load @"DG.Auth.fsx" // Just contains let usr = "usr" and let pwd = "pwd"
 
open System
open System.Runtime.Serialization
open System.ServiceModel.Description
 
open Microsoft.Xrm.Sdk
open Microsoft.Xrm.Sdk.Client
open Microsoft.Xrm.Sdk.Messages
open Microsoft.Xrm.Sdk.Query
 
open FSharp.Data
 
/// Utils
let r = new System.Random()

let crmCount (proxy:OrganizationServiceProxy) logicalName (date:DateTime) = 
  let f = FilterExpression()
  f.AddCondition(@"createdon", ConditionOperator.GreaterEqual, date.ToUniversalTime())
 
  let q = QueryExpression(logicalName)
  q.ColumnSet <- ColumnSet(logicalName + "id")
  q.Criteria <- f
  q.PageInfo <- PagingInfo()
  q.PageInfo.PageNumber <- 1
 
  seq{ let resp = proxy.RetrieveMultiple(q)
       yield! resp.Entities
 
       let rec retrieveMultiple' (ec:EntityCollection) pn = seq{
         match ec.MoreRecords with
         | true ->
           q.PageInfo.PageNumber <- (pn + 1)
           q.PageInfo.PagingCookie <- ec.PagingCookie
 
           let resp' = proxy.RetrieveMultiple(q)
 
           yield! resp'.Entities
           yield! retrieveMultiple' resp' (pn + 1)
         | false -> () }
 
       yield! retrieveMultiple' resp 1 }
  |> Seq.length

/// Connection info:
let uri = Uri("https://org.api.crm4.dynamics.com/XRMServices/2011/Organization.svc");
 
let ac = AuthenticationCredentials()
ac.ClientCredentials.UserName.UserName <- DG.Auth.usr
ac.ClientCredentials.UserName.Password <- DG.Auth.pwd
 
let m = ServiceConfigurationFactory.CreateManagement<IOrganizationService>(uri)
let tc = m.Authenticate(ac)
let p = new OrganizationServiceProxy(m, tc.SecurityTokenResponse)
 
/// Test data:
let data = FreebaseData.GetDataContext()
 
let names = data.Society.People.``Family names`` |> Seq.take 250
let namesCache = names |> Seq.toArray |> Array.map(fun x -> x.Name)
 
let titles = data.``Products and Services``.Business.``Job titles`` |> Seq.take 250
let titlesCache = titles |> Seq.toArray |> Array.map(fun x -> x.Name)
 
let products = data.``Products and Services``.``Food & Drink``.Foods |> Seq.take 250
let productsCache = products |> Seq.toArray |> Array.map(fun x -> x.Name)
 
let countries = data.Commons.Location.Countries |> Seq.take 250
let countriesCache = countries |> Seq.toArray |> Array.map(fun x -> x.Name)
 
let cities = data.Commons.Location.``City/Town/Villages`` |> Seq.take 250
let citiesCache = cities |> Seq.toArray |> Array.map(fun x -> x.Name)
 
let companyName name = name + " Company"
let streetName name = name + " Street " + string(r.Next(1,1000))
let phoneNumber () = "555-" + string(r.Next(1000,10000))
let zipCode () = string(r.Next(1000,10000))
let email firstname lastname = (firstname + "." + lastname + "@mail.co.dk").ToLower()
 
/// Create as many accounts as possible
let createAccount () =
  let a = Entity("account")
  a.Attributes.Add("name", 
    namesCache.[r.Next(0,250)] + " " + 
    namesCache.[r.Next(0,250)] |> companyName)
  a.Attributes.Add("telephone1", phoneNumber())
  a.Attributes.Add("address1_line1", namesCache.[r.Next(0,250)] |> streetName)
  a.Attributes.Add("address1_city", citiesCache.[r.Next(0,250)])
  a.Attributes.Add("address1_postalcode", zipCode())
  a.Attributes.Add("address1_country", countriesCache.[r.Next(0,250)])
  a
 
// One account per thread
let createAccounts date concurrency =
  Array.Parallel.init concurrency (fun _ -> createAccount ())
  |> Array.Parallel.map(
    fun x -> 
      try
        match (date > DateTime.Now) with
        | true -> 
          let p' = new OrganizationServiceProxy(m, tc.SecurityTokenResponse)
          p'.Create(x) |> Some
        | false -> None
      with ex -> None)
 
// Ten accounts per thread
let createAccounts' date concurrency =
  Array.Parallel.init concurrency
    (fun _ -> 
      let em = new ExecuteMultipleRequest()
      em.Settings <- new ExecuteMultipleSettings()
      em.Settings.ContinueOnError <- true
      em.Settings.ReturnResponses <- true
      em.Requests <- new OrganizationRequestCollection()
      em)
  |> Array.Parallel.map(
    fun x -> 
      try
        Array.Parallel.init 10
          (fun _ -> 
            let cr = new CreateRequest()
            cr.Target <- createAccount()
            x.Requests.Add(cr)) |> ignore
        match (date > DateTime.Now) with
        | true -> 
          let p' = new OrganizationServiceProxy(m, tc.SecurityTokenResponse)
          p'.Execute(x) :?> ExecuteMultipleResponse |> Some
        | false -> None
      with ex -> None)
 
/// Stress Test
let rec stressTestCrm date concurrency = 
  match (date > DateTime.Now) with
  | true -> 
    createAccounts' date concurrency |> ignore
    stressTestCrm date concurrency
  | false -> ()
 
/// Concurrent users (threads)
let concurrency = (1 <<< 10) // 1024
 
/// Start and Stop DateTimes
let startDate = DateTime.Now
let stopDate  = startDate.AddMinutes(60.)
 
/// Perfom stress test and print outcome
stressTestCrm stopDate concurrency
 
(crmCount p "account" startDate, startDate.ToString("o"))
||> printfn "Accounts created: %i, since: %s"
{% endhighlight %}

### Code result:

{% highlight text %}
Accounts created: 141225, since: 2014-09-16T22:31:53.9798783+02:00
Press any key to continue . . .
{% endhighlight %}

### Architecture (Lenovo ThinkPad W540):

<img src="/assets/img/posts/2014-09-16-fsharp-ms-crm-online-stress-test-architecture.jpg"
alt="All">
