---
layout: post
title: How to make your own F# logo (Updated)
categories:
  -English
tags:
  -f#
  -fsharp
  -logo
  -foundation
time: "21:06"
---

### First thing first

Before you can create your own F# logo, you will need a drawing/editing program
with support for layers. If you work on a Windows box, you might as well go with
[Paint.NET - Free Software for Digital Photo Editing](http://www.getpaint.net/),
but if you are on a *nix box as myself, you might want to give [Pinta: Painting
Made Simple](http://pinta-project.com/) a try.

Your logo file should preferable be in a looseless format and with the following
measurements: 200 pixels x 200 pixels.

The last step before you can make your own logo is to download both:

* [PNG, Lighter variant, 100x100, Transparent
  Background](http://fsharp.org/img/logo/fsharp100mclight.png)

* [PNG, Darker variant, 100x100, Transparent
  Background](http://fsharp.org/img/logo/fsharp100mcdark.png)

from the [The F# Software Foundation Logo for
F#](http://fsharp.org/foundation/logo.html). You should have the following in a
local folder by now:

{% highlight text %}
[ mon@mbai7 logos ] ll
total 20K
-rw-r--r-- 1 mon staff 7.7K Oct 11 21:02 delegate_frm_logo.ora
-rw-r--r-- 1 mon staff 4.0K Oct 11 20:37 delegate_frm_logo.png
-rw-r--r-- 1 mon staff 1.3K Oct  6 14:56 fsharp100mcdark.png
-rw-r--r-- 1 mon staff 1.5K Oct  6 14:56 fsharp100mclight.png
[ mon@mbai7 logos ]
{% endhighlight %}

### Let's do it!

The initial logo looks like this:
<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_delegate_frm_logo.png"
alt="All" style="border:1px solid #66ED11;">

Once the file is open in **Pinta**, we will create a new file with the same
dimensions and save it with the **OpenRaster** format (.ora). This format
supports layers, which is indispensable for this kind of work. We will create an
empty layer called **frm** and we will import the **fsharp100mclight.png** as a layer
(**Menu |> Layers |> Import from File...**)

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_pinta_01.png"
alt="All">

We will now choose our logo file and select all and copy it.

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_pinta_02.png"
alt="All">

Afterwards, we will paste it into the **frm** layer. As the **frm** layer is below the
transparent F# logo layer, the F# logo will still be visible (slightly).

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_pinta_03.png"
alt="All">

The reason the logo is not blending in well is because I have a black
background. In order to make the logo visible, I will change the colors to dark
green and light green. 

**Remark**: This is actually not allowed as it will violate the usage
guidelines: "*Don't change the colors*". For more info, please contact
[@ReedCopsey ](https://twitter.com/ReedCopsey)

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_pinta_04.png"
alt="All">

And you are done:

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_delegate_frm_logo_new.png"
alt="All" style="border:1px solid #66ED11;">

Easy right? Btw I tried out changing a bit the colors in order to comply with the terms of use:

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_delegate_frm_logo_ugly.png"
alt="All">

... I'm not convinced though.

**Update**: [@ReedCopsey ](https://twitter.com/ReedCopsey) sent me the following
  logo which is pretty much awesome!!! 

<img
src="/assets/img/posts/2014-10-11-how-to-make-your-own-fsharp-logo_delegate_frm_logo_awesome.png"
alt="All" style="border:1px solid #66ED11;"> 

I have no clue how he has done it, but at least it doens't violate the terms of
use. The lesson learned here is that the F# Comunity will help you out if you
aren't able to do it yourself which is pretty cool.