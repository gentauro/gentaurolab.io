--- 
layout: post
title: F# - Trustpilot job advertisement
categories:
  - English
tags:
  - snippet
  - f#
time: "18:02"
---

### Code Snippet:

{% highlight ocaml linenos %}
type SeqMonad() =
  member t.Bind(m,f) = Seq.concat(Seq.map f m)
  member t.Return v = seq{ yield v }
let seqMonad = SeqMonad()

let permutations ls = 
  let rec insertions x = function
    | []             -> [[x]]
    | (y :: ys) as l -> (x::l)::(List.map (fun x -> y::x) (insertions x ys))
  let rec permutations' = function
    | []      -> seq [ [] ]
    | x :: xs -> Seq.concat (Seq.map (insertions x) (permutations' xs))
  ls |> permutations'

let md5 s =
  System.BitConverter
    .ToString(
      System.Security.Cryptography.MD5
        .Create()
        .ComputeHash(buffer = System.Text.Encoding.UTF8.GetBytes(s = s)))
    .Replace("-", System.String.Empty)
    .ToLower()

let factorial n = 
  let rec fact acc = function | 0 -> acc | i -> fact (acc * i) (i - 1)
  (1,n) ||> fact

let unitTestPermutations () = 
  "FooBar" 
  |> Seq.toList
  |> fun xs -> xs |> permutations |> Seq.length,
               xs |> Seq.length   |> factorial
  |> fun (x,y) -> x = y

let unitTestMD5 () =  
  // [ mon@mbai7 tmp ] md5 -s "FooBar"
  // MD5 ("FooBar") = f32a26e2a3a8aa338cd77b6e1263c535
  "FooBar" |> md5 |> fun x -> x = "f32a26e2a3a8aa338cd77b6e1263c535"

(unitTestPermutations() && unitTestMD5()) |> function 
  | true -> () 
  | false -> failwith "Must be n! permuations per string"

let cache file =
  use reader = System.IO.File.OpenText(file)
  let rec cache' acc = function
    | true -> acc
    | false -> cache' (acc |> Set.add(reader.ReadLine())) reader.EndOfStream
  cache' Set.empty reader.EndOfStream

let root     = __SOURCE_DIRECTORY__
let wordList = System.IO.Path.Combine(root,"wordlist.txt")
let anagram  = @"poultry outwits ants"
let hash     = @"4624d200580677270a54ccff86b9610e"
let words    = anagram.Split(char " ")
let cached   = wordList |> cache

words |> Array.map(fun x  -> x  |> Seq.toList |> permutations)
      |> Array.map(fun xs -> xs |> Seq.map(fun ys -> ys |> List.map string))
      |> Array.map(fun xs -> xs |> Seq.map(fun ys -> ys |> List.reduce(+)))
      |> Array.map(fun xs -> xs |> Seq.filter(fun x -> (x,cached) ||> Set.contains))
      |> fun xs -> xs.[0],xs.[1],xs.[2]
      |> fun (xs,ys,zs) -> seqMonad{let! x = xs
                                    let! y = ys
                                    let! z = zs
                                    return (x,y,z)}
      |> Seq.map(fun (x,y,z) -> x + " " + y + " " + z)
      |> Seq.map(fun x -> x |> md5, x)
      |> Seq.filter(fun (x,y) -> x = hash)
      |> Seq.map(fun (x,y) -> y)
      |> Seq.truncate 1
      |> fun x -> x |> printfn "%A"
{% endhighlight %}

### Code output:

{% highlight text %}
type SeqMonad =
  class
    new : unit -> SeqMonad
    member Bind : m:seq<'b> * f:('b -> #seq<'d>) -> seq<'d>
    member Return : v:'a -> seq<'a>
  end
val seqMonad : SeqMonad
val permutations : ls:'a list -> seq<'a list>
val md5 : s:string -> string
val factorial : n:int -> int
val unitTestPermutations : unit -> bool
val unitTestMD5 : unit -> bool
val cache : file:string -> Set<string>
val root : string = "/Users/mon/tmp"
val wordList : string = "/Users/mon/tmp/wordlist.txt"
val anagram : string = "poultry outwits ants"
val hash : string = "4624d200580677270a54ccff86b9610e"
val words : string [] = [|"poultry"; "outwits"; "ants"|]
val cached : Set<string> =
  set
    ["a"; "a's"; "ab's"; "abaci"; "aback"; "abacus"; "abacus's"; "abacuses";
     "abaft"; ...]
{% endhighlight %}

### Code result:

{% highlight text %}
> seq []
> val it : unit = ()
{% endhighlight %}

### References:

* Jobfinder.dk: [Kan du knække dette kodede
  jobopslag?](http://karriere.jobfinder.dk/artikel/kan-du-knaekke-dette-kodede-jobopslag-1912)

* Trustpilot: [follow the white
  rabbit](http://followthewhiterabbit.trustpilot.com/cs/step1.html)