--- 
layout: post
title: F# - Time Traveling Debugger
categories:
  - English
tags:
  - snippet
  - f#
time: "16:25"
---

### MVC + Agents (MailboxProcessor) + Immutable Model

<img
src="/assets/img/posts/2015-10-30-fsharp-time-traveling-debugger_mvc_role_diagram.png"
alt="All">

### Code Snippet:

{% highlight ocaml linenos %}
open System
open System.Drawing
open System.Windows.Forms

type Agent<'a> = MailboxProcessor<'a>

module Utils =
  let doEvents () = Application.DoEvents()

module Model =
  type ('a) Model = { state: 'a; events: 'a list }
  // Add functions here to load/persist state and events

module View =
  // Main form
  let title = @"Time Traveling Debugger"
  let width,height = 1024,768
  let form = 
    new Form(
      Visible=true, TopMost=true, ClientSize=Size(width,height),
      MaximizeBox=false, FormBorderStyle=FormBorderStyle.FixedDialog,Text=title)
  let canvas = new Rectangle(0, 0, width, height)
  form.Paint.Add(fun e -> e.Graphics.FillRectangle(Brushes.Black, canvas))

  // Time Traveling Debugger
  let debug = new TrackBar()
  debug.Location <- Point(0,height - 100)
  debug.TickStyle <- TickStyle.Both
  debug.AutoSize <- false
  debug.Width <- width
  debug.Height <- 100
  debug.Minimum <- 0
  debug.Maximum <- 0
  debug.Value <- 0
  form.Controls.Add(debug)

  // Time Traveling Debugger info text
  let debugText = new Label()
  debugText.Location <- Point(0, 50)
  debugText.Width <- width
  debugText.TextAlign <- ContentAlignment.MiddleCenter
  debug.Controls.Add(debugText)

  // Update functions "hardcoded" to above formular and formular controls
  let updateTitle point = form.Text <- sprintf "%s: %A" title point
  let updateCanvas p1 p2 =
    let prev = new Rectangle(fst p1, snd p1, 30, 30)
    form.Paint.Add(fun e -> e.Graphics.FillRectangle(Brushes.Black, prev))
    let next = new Rectangle(fst p2, snd p2, 30, 30)
    form.Paint.Add(fun e -> e.Graphics.FillRectangle(Brushes.LimeGreen, next))
    form.Invalidate(next)
    form.Invalidate(prev)
  let updateDebug n = 
    debug.Maximum <- n
    debug.Value <- n
  let updateDebugText n s p = 
    debugText.Text <- sprintf "Nr. events: %i\nDebug step: %i %A" (n+1) (s+1) p

module Controller =
  open Model
  open View

  type Action = | Event of Event | Debug of Debug
  and Event = int * int
  and Debug = int

  // By using Agents, the model stays inmutable
  let agent = Agent.Start(fun inbox ->
    let rec loop model = async {
      let! msg = inbox.Receive()
      match msg with
      | Event(point) ->
        let model' = { model with state = point; events = point::model.events }
        let n = (model'.events |> List.length) - 1
        updateTitle model'.state
        updateCanvas model.state model'.state
        updateDebug n
        updateDebugText n n point
        return! loop model'
      | Debug(index) ->
        let n = (model.events |> List.length) - 1
        let point = model.events |> List.skip (n-index) |> List.head
        let model' = { model with state = point }
        updateTitle model'.state
        updateCanvas model.state model'.state
        updateDebugText n index point
        return! loop model' }
    loop { Model.state = (0,0); events = [] })

  // Hook-up model events
  form.MouseClick
  |> Event.add (fun e -> agent.Post (Action.Event(e.X,e.Y)))

  // Hook-up Time Traveling Debugger events
  debug.Scroll
  |> Event.add(fun _ -> agent.Post (Action.Debug(debug.Value)))

// Program event loop, not to use with F# Interactive (FsiAnyCpu)
open Utils
open View

let rec main = function | true -> doEvents(); main form.Created | false -> ()

main form.Created
{% endhighlight %}

### Code output:

<img
src="/assets/img/posts/2015-10-30-fsharp-time-traveling-debugger_time_traveling_debugger.gif"
alt="All">

### References:

* Heavily inspired: [Elm's Time Traveling Debugger](http://debug.elm-lang.org)
