--- 
layout: post
title: F# - LeftPad (Updated)
categories:
  - English
tags:
  - snippet
  - f#
time: "04:22"
---
### Code Snippet

{% highlight ocaml linenos %}
let leftpad s n c =
  let l = s |> String.length // string length from O(N) to O(1)
  let c' = match c with | None -> "." | Some v -> v
  match l <= n with
    | true -> String.replicate (n-l) c' + s
    | false -> s

leftpad "foo" 6 None
leftpad "foo" 3 None
leftpad "fooBar" 3 None
leftpad "foo" 6 (Some "?")
{% endhighlight %}

### Code output:

{% highlight text %}
> 
val leftpad : s:string -> n:int -> c:string option -> string

> val it : string = "...foo"
> val it : string = "foo"
> val it : string = "fooBar"
> val it : string = "???foo"
{% endhighlight %}

### References:

* Implementing Left-Pad in 13 Languages:
  - [Left-Pad could be the next FizzBuzz so we coded it up in 13 Languages](https://www.educative.io/collection/page/10370001/520001/750001)

