--- 
layout: post
title: Haskell - Yao's Millionaires' Problem (no encryption)
categories:
  - English
tags:
  - snippet
  - haskell
  - yao
  - millionaires
  - problem
  - no
  - encryption
time: "13:33"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2019-02-12-haskell-yaos-millionaires-problem-no-encryption_hts-systems-ups-ground.jpg" />
<figcaption>
Author MobiusDaXter at Wikipedia (CC BY-SA 3.0)
</figcaption>
</figure>
</div>


### Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --package echo
   --package random
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.List
  ( sort
  )
import           Data.Maybe
  ( fromMaybe
  )
import           System.IO.Echo
  ( withoutInputEcho
  )
import           System.Random
  ( newStdGen
  , randomRs
  )
import           Text.Read
  ( readMaybe
  )

--------------------------------------------------------------------------------

-- The type `Box` should be moved to a common `library` used by both Bob and Ali
-- and it shouldn't expose the constructor `Box`. That way, we ensure that the
-- type `Box` can't implement an instance for `Show`.
--
-- Note: As this mimic a `physical` representation of `Boxes`, we aren't going
-- into further details on how to tranfer these `Boxes` over the wire.
newtype Box = Box { box :: (Integer, Ordering) }

type Boxes = [ Box ]
type Lower = Integer
type Upper = Integer

rands :: Int -> Lower -> Upper -> IO [ Integer ]
rands n l u =
  take n . randomRs (l,u) <$> newStdGen

action01 :: Integer -> IO Boxes
action01 x =
  -- Bob gives his labeled box to `UPS` at Location A. Afterwards, the driver
  -- takes further 10 boxes and randomly label them in the range (lower = x / 5,
  -- upper = x * 5), based on the amout `x` of Bobs fortune stated on his
  -- box. They drive the 11 boxes to Ali at Location B.
  map (Box . flip (,) undefined) . sort . (x:) <$> rands 10 l u
  where
    l = x `div` 5
    u = x   *   5

action02 :: Integer -> Boxes -> Boxes
action02 x =
  -- Ali, at Location B, put stickers (EQ,LT and GT) on all boxes based on her
  -- fortune compared to the values on each box. When this is done, the `UPS`
  -- driver, bring them all back to Bob at Location A.
  map $ Box . aux . box
  where
    aux = \(i,_) -> (i, compare x i)

action03 :: Integer -> Boxes -> Ordering
action03 x =
  -- Since Bob only knows the value of his Box, he can only request that to the
  -- driver of the `UPS` van. If he began to guess randomly and fails, the
  -- driver might get suspicious and will abort any delivery.
  snd . box . head . filter ((== x) . fst . box)

--------------------------------------------------------------------------------

main :: IO ()
main =
  putStr "Location A | Bob gives his box:   "   >>= \ _   ->
  help <$> withoutInputEcho (getLine)           >>= \ bob ->
  putStrLn "********"                           >>= \ _   ->
  putStr "Location B | Ali label the boxes: "   >>= \ _   ->
  help <$> withoutInputEcho (getLine)           >>= \ ali ->
  putStrLn "********"                           >>= \ _   ->
  action03 bob . action02 ali <$> action01 bob  >>= putStrLn . (info ++) . show
  where
    info = "Location A | Bob sees that Ali's fortune is: "
    help = fromMaybe 0 . readMaybe
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../ymp$ ./YaosMillionairesProblem.hs 
Location A | Bob gives his box:   ******** # Bobs fortune is 42
Location B | Ali label the boxes: ******** # Alis fortune is 84
Location A | Bob sees that Ali's fortune is: GT
{% endhighlight %}


### References:

* Wikipedia:
  - [Yao's Millionaires' Problem][yao]
* Twisted Oak Studios Blog:
  - [Explain it like I’m Five: The Socialist Millionaire Problem and Secure
    Multi-Party Computation][oak]
  
[yao]: https://en.wikipedia.org/wiki/Yao%27s_Millionaires%27_Problem
[oak]: http://twistedoakstudios.com/blog/Post3724_explain-it-like-im-five-the-socialist-millionaire-problem-and-secure-multi-party-computation
