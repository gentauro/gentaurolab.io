--- 
layout: post
title: Haskell - Kleroterion a democratic selection artifact
categories:
  - English
tags:
  - snippet
  - haskell
  - kleroterion
  - democratic
  - selection
  - artifact
time: "19:59"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2020-07-14-haskell-kleroterion-a-democratic-selection-artifact_kleroterion.jpg" />
<figcaption>
The kleroterion was used for the jury selection in Athens, author Marsyas at Wikipedia (CC BY-SA 2.5)
</figcaption>
</figure>
</div>


### Code Snippet

{% highlight haskell linenos %}
--------------------------------------------------------------------------------
--
-- Kleroterion, (c) 2020 SPISE MISU ApS
--
--------------------------------------------------------------------------------

{-# LANGUAGE Trustworthy #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.List
  ( sortBy
  )
import           Data.Maybe
  ( catMaybes
  )
import           System.Random
  ( StdGen
  , newStdGen
  , randoms
  )

--------------------------------------------------------------------------------

{-

KLEROTERION

┌║─────────────────────────────────────────────────┐
│║ Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▬ ▬ ▭ ▭ ▬ ▭ ▬ ▬ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▬ ▭ ▭ ▬ ▭ ▬ ▬ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▬ ▭ ▭ ▬ ▭ ▬ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▬ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ ▭ │
│║                                                 │
│║                                                 │
│║                                                 │
│║                                                 │
│╚═                                 ▪▫▫▪▫▪▪▫▫▪▫▫▫▫▫│
└──────────────────────────────────────────────────┘

REFERENCES:

* https://en.wikipedia.org/wiki/Kleroterion

* https://www.academia.edu/36510282/

* https://www.youtube.com/watch?v=gt9H7nbZjAw

* https://www.youtube.com/watch?v=1DhgkqJCIBA

-}

--------------------------------------------------------------------------------

data Token a b
  = UID a b
  deriving
    ( Eq
    , Ord
    , Show
    )

data Marble
  = Black
  | White
  deriving
    ( Eq
    , Ord
    , Show
    )

type Pinakia
  = Token
type Kyboi
  = Marble
type Slot a b
  = Maybe (Pinakia a b)

data Kleroterion a b
  = Slab [[Slot a b]] [Kyboi]
  deriving
    Show

--------------------------------------------------------------------------------

main :: IO ()
main =
  pinakias >>= \ps ->
  kybois   >>= \ks ->
  ( putStrLn "# Pinakias (pre-shuffled eligible people):"
  ) >>
  ( mapM_ (putStrLn . ("* " ++)) $
    map (concatMap (\ (UID a b) -> "(" ++ [a] ++ "," ++ zeropad b ++ ") ")) $
    ps
  ) >>
  ( putStrLn ""
  ) >>
  ( putStrLn "# Kybois (pre-shuffled binary marbles):"
  ) >>
  ( putStrLn . ("* " ++) $
    concatMap ((++ " ") . show) $
    ks
  ) >>
  ( putStrLn ""
  ) >>
  ( putStrLn "# Kleroterion (sorted elected people):"
  ) >>
  ( putStrLn . ("* " ++) $
    concatMap (\ (UID a b) -> "(" ++ [a] ++ "," ++ zeropad b ++ ") ") $
    sortBy compare $
    elected $
    kleroterion ps ks
  )
  where
    pinakias =
      go
      <$> shuffle αs
      <*> shuffle βs
      <*> shuffle γs
      <*> shuffle δs
      <*> shuffle εs
      <*> shuffle ζs
      <*> shuffle ηs
      <*> shuffle θs
      <*> shuffle ιs
      <*> shuffle κs
      <*> shuffle λs
      where
        go α β γ δ ε ζ η θ ι κ λ =
          α:β:γ:δ:ε:ζ:η:θ:ι:κ:λ:[]
        --
        αs = map (UID 'Α') $ take 11 ns
        βs = map (UID 'Β') $ take 13 ns
        γs = map (UID 'Γ') $ take 10 ns
        δs = map (UID 'Δ') $ take 10 ns
        εs = map (UID 'Ε') $ take 10 ns
        ζs = map (UID 'Ζ') $ take 16 ns
        ηs = map (UID 'Η') $ take 13 ns
        θs = map (UID 'Θ') $ take 12 ns
        ιs = map (UID 'Ι') $ take 10 ns
        κs = map (UID 'Κ') $ take 15 ns
        λs = map (UID 'Λ') $ take 10 ns
        --
        ns = [ 0 :: Integer .. ]
    kybois =
      shuffle ms
      where
        ms =
          take 5 bs ++ take 5 ws
          where
            bs = repeat Black
            ws = repeat White
    kleroterion ps ks =
      Slab ss ks
      where
        ss = map (map Just) ps
    zeropad =
      leftpad 2 '0' . show

--------------------------------------------------------------------------------

-- HELPERS

shuffle
  ::    [a]
  -> IO [a]
shuffle xs =
  aux <$> newStdGen
  where
    num = randoms :: StdGen -> [Integer]
    len = length xs
    fip = flip zip
    ord = \ x y -> fst x `compare` fst y
    aux =
      map snd .
      sortBy ord .
      fip xs .
      take len .
      num

elected
  :: Kleroterion a b
  -> [Pinakia a b]
elected (Slab ss ks) =
  catMaybes $ aux ks ss
  where
    aux [    ] __ = []
    aux (x:xs) ys =
      case x of
        Black ->                          zs
        White -> concatMap (take 1) ys ++ zs
      where
        zs = aux xs $ map (drop 1) ys

leftpad
  :: Int
  -> Char
  -> String
  -> String
leftpad n c x =
  if l > n
  then                      x
  else replicate (n-l) c ++ x
  where
    l = length x
{% endhighlight %}


### Code Output:

{% highlight text %}
# Pinakias (pre-shuffled eligible people):
* (Α,03) (Α,02) (Α,05) (Α,01) (Α,09) (Α,00) (Α,08) (Α,06) (Α,04) (Α,07) (Α,10) 
* (Β,08) (Β,10) (Β,11) (Β,05) (Β,03) (Β,04) (Β,01) (Β,06) (Β,09) (Β,00) (Β,02) (Β,12) (Β,07) 
* (Γ,03) (Γ,06) (Γ,07) (Γ,08) (Γ,04) (Γ,09) (Γ,01) (Γ,05) (Γ,02) (Γ,00) 
* (Δ,07) (Δ,06) (Δ,00) (Δ,04) (Δ,02) (Δ,09) (Δ,01) (Δ,05) (Δ,08) (Δ,03) 
* (Ε,07) (Ε,06) (Ε,05) (Ε,00) (Ε,03) (Ε,01) (Ε,09) (Ε,04) (Ε,02) (Ε,08) 
* (Ζ,14) (Ζ,06) (Ζ,05) (Ζ,15) (Ζ,04) (Ζ,08) (Ζ,00) (Ζ,11) (Ζ,03) (Ζ,10) (Ζ,12) (Ζ,13) (Ζ,07) (Ζ,09) (Ζ,01) (Ζ,02) 
* (Η,12) (Η,00) (Η,08) (Η,05) (Η,01) (Η,10) (Η,09) (Η,06) (Η,04) (Η,11) (Η,07) (Η,02) (Η,03) 
* (Θ,00) (Θ,02) (Θ,11) (Θ,10) (Θ,09) (Θ,06) (Θ,05) (Θ,01) (Θ,04) (Θ,08) (Θ,03) (Θ,07) 
* (Ι,04) (Ι,03) (Ι,09) (Ι,01) (Ι,05) (Ι,02) (Ι,06) (Ι,00) (Ι,07) (Ι,08) 
* (Κ,00) (Κ,13) (Κ,02) (Κ,10) (Κ,07) (Κ,14) (Κ,09) (Κ,11) (Κ,12) (Κ,05) (Κ,06) (Κ,08) (Κ,03) (Κ,04) (Κ,01) 
* (Λ,00) (Λ,02) (Λ,03) (Λ,04) (Λ,06) (Λ,09) (Λ,08) (Λ,01) (Λ,07) (Λ,05) 

# Kybois (pre-shuffled binary marbles):
* Black White White White Black White Black Black White Black 

# Kleroterion (sorted elected people):
* (Α,00) (Α,01) (Α,02) (Α,04) (Α,05) (Β,04) (Β,05) (Β,09) (Β,10) (Β,11) (Γ,02) (Γ,06) (Γ,07) (Γ,08) (Γ,09) (Δ,00) (Δ,04) (Δ,06) (Δ,08) (Δ,09) (Ε,00) (Ε,01) (Ε,02) (Ε,05) (Ε,06) (Ζ,03) (Ζ,05) (Ζ,06) (Ζ,08) (Ζ,15) (Η,00) (Η,04) (Η,05) (Η,08) (Η,10) (Θ,02) (Θ,04) (Θ,06) (Θ,10) (Θ,11) (Ι,01) (Ι,02) (Ι,03) (Ι,07) (Ι,09) (Κ,02) (Κ,10) (Κ,12) (Κ,13) (Κ,14) (Λ,02) (Λ,03) (Λ,04) (Λ,07) (Λ,09) 
{% endhighlight %}


### References:

* Wikipedia:
  - [Kleroterion][wiki]
* ACADEMIA:
  - [KLEROTERION. simulation of the allotment of dikastai][academia]
* YouTube
  - [KLEROTERION - Simulation of the draw with kleroterion][orlandini]
  - [Kleroterion - machine that selected the leaders of citizens of Athens][cnrs]

[wiki]:      https://en.wikipedia.org/wiki/Kleroterion
[academia]:  https://www.academia.edu/36510282/
[orlandini]: https://www.youtube.com/watch?v=gt9H7nbZjAw
[cnrs]:      https://www.youtube.com/watch?v=1DhgkqJCIBA
