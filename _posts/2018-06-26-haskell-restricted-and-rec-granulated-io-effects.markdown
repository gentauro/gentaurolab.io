--- 
layout: post
title: Haskell - Restricted and (rec) granulated IO effects
categories:
  - English
tags:
  - snippet
  - haskell
  - restricted
  - granulated
  - nested
  - side
  - effects
  - sideeffects
time: "17:01"
---

### Code Snippets

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-11.14
   --install-ghc
   runghc
   --
   -Wall -Werror
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
  
--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import Control.Monad (liftM, ap)

--------------------------------------------------------------------------------

newtype RIO a = RestrictedIO { rio :: IO a }

instance Functor RIO where
  fmap = liftM

instance Applicative RIO where
  pure  = return
  (<*>) = ap
  
instance Monad RIO where
  return  a = RestrictedIO $ return a
  (>>=) m f = RestrictedIO $ rio m >>= rio . f

--------------------------------------------------------------------------------

class (Monad m) => SubGranulatedCmdM m where
  sout
    :: String
    -> m ()
instance SubGranulatedCmdM RIO where
  sout x =
    gout $ x ++ " [Subnulated]"
    -- or (both valid options)
    -- rout $ x ++ " [Subnulated]"

class (SubGranulatedCmdM m) => GranulatedCmdM m where
  gout
    :: String
    -> m ()
instance GranulatedCmdM RIO where
  gout x =
    rout $ x ++ " [Granulated]"

class (GranulatedCmdM m) => RestrictedCmdM m where
  rout
    :: String
    -> m ()
instance RestrictedCmdM RIO where
  rout x =
    RestrictedIO $ putStrLn $ x ++ " [Restricted]"

--------------------------------------------------------------------------------

subgranulated
  :: (SubGranulatedCmdM m)
  => m ()
subgranulated =
  do
    -- We are still in the RIO monad, but now we have sub-granulated the
    -- effects, so we can only call those, but not Granulated, Restricted nor
    -- IO effects !!!
    sout "43"
    --gout     "NONONONO Cat"
    --rout     "NONONONO Cat"
    --putStrLn "NONONONO Cat"

granulated
  :: (GranulatedCmdM m)
  => m ()
granulated =
  do
    -- We are still in the RIO monad, but now we have granulated the effects, so
    -- we can only call those, but not Restricted nor IO effects !!!
    subgranulated
    gout "42"
    --rout     "NONONONO Cat"
    --putStrLn "NONONONO Cat"

restricted
  :: (RestrictedCmdM m)
  => m ()
restricted =
  do
    -- Now we are in the RIO monad, we can call Restricted and Granulated
    -- effects, but not IO !!!
    granulated
    rout "41"
    --putStrLn "NONONONO Cat"

main :: IO ()
main =
  do
    -- Here we are in the IO monad, we can do whatever we want !!!
    rio restricted
    putStrLn "40"
{% endhighlight %}


### Output:

{% highlight text %}
user@personal:~/.../subgranulate$ clear && ./Main.hs
43 [Subnulated] [Granulated] [Restricted]
42 [Granulated] [Restricted]
41 [Restricted]
40
user@personal:~/.../subgranulate$
{% endhighlight %}

### References:

* Microsoft Research Publications:
  - [Safe Haskell][safehaskell]
  
[safehaskell]: https://www.microsoft.com/en-us/research/publication/safe-haskell/
