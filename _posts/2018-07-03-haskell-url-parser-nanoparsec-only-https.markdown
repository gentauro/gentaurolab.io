---
layout: post
title: Haskell - URL-parser (NanoParsec) only HTTP(S)
categories:
  - English
tags:
  - snippet
  - haskell
  - url
  - parse
  - parsec
  - parser
  - combinator
  - nanoparsec
time: "20:50"
---

### Code Snippets

#### Parsers/NanoParsec.hs

{% highlight haskell linenos %}
{-# LANGUAGE FlexibleInstances #-}

--------------------------------------------------------------------------------
  
module Parsers.NanoParsec
  ( Parseable
  , Parser
  , item
  , some, many, sepBy, sepBy1
  , satisfy, oneOf, chainl, chainl1
  , char, string, token, reserved, spaces
  , runParser
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString                 as BS
import           Data.String
  ( IsString
  )
import           Control.Applicative.Alternative
  ( Alternative
    ( empty
    , (<|>)
    )
  )
import           Control.Monad.Plus
  ( MonadPlus
    ( mzero
    , mplus
    )
  )

--------------------------------------------------------------------------------

-- NanoParsec:
-- http://dev.stephendiehl.com/fun/002_parsers.html#nanoparsec

newtype Parser s a = Parser { parse :: s -> [ (a, s) ] }

class (Eq a, IsString a) => Parseable a where
  nil :: a -> Bool
  hd  :: a -> Char
  tl  :: a -> a

instance Parseable String where
  nil = (== [])
  hd  =  head
  tl  =  tail

instance Parseable BS.ByteString where
  nil =                         BS.null
  hd  = toEnum . fromIntegral . BS.head
  tl  =                         BS.tail

--------------------------------------------------------------------------------

instance (Parseable s) => Functor (Parser s) where
  fmap f (Parser cs) =
    Parser $ \s -> [(f a, b) | (a, b) <- cs s]

instance (Parseable s) => Applicative (Parser s) where
  pure = return
  (Parser cs1) <*> (Parser cs2) =
    Parser $ \s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1]

instance (Parseable s) => Monad (Parser s) where
  return = unit
  (>>=)  = bind

instance (Parseable s) => MonadPlus (Parser s) where
  mzero = failure
  mplus = combine

instance (Parseable s) => Alternative (Parser s) where
  empty = mzero
  (<|>) = option

--------------------------------------------------------------------------------

bind
  :: (Parseable s)
  => Parser s a
  -> (a -> Parser s b)
  -> Parser s b
bind p f =
  Parser $ \s -> concatMap (\(a, s') -> parse (f a) s') $ parse p s

unit
  :: (Parseable s)
  => a
  -> Parser s a
unit a =
  Parser $ \s -> [ (a, s) ]

combine
  :: (Parseable s)
  => Parser s a
  -> Parser s a
  -> Parser s a
combine p q =
  Parser $ \s -> parse p s ++ parse q s

failure
  :: (Parseable s)
  => Parser s a
failure =
  Parser $ \_ -> []

option
  :: (Parseable s)
  => Parser s a
  -> Parser s a
  -> Parser s a
option p q =
  Parser
  $ \s ->
      case parse p s of
        [ ] -> parse q s
        res -> res
        
--------------------------------------------------------------------------------

item
  :: (Parseable s)
  => Parser s Char
item =
  Parser
  $ \s ->
      case nil s of
        True  -> []
        False -> [ (hd s, tl s) ]



        
--------------------------------------------------------------------------------

-- | One or more.
some
  :: (Alternative f)
  => f a
  -> f [a]
some v = some_v
  where
    many_v = some_v <|> pure []
    some_v = (:) <$> v <*> many_v

-- | Zero or more.
many
  :: (Alternative f)
  => f a
  -> f [a]
many v = many_v
  where
    many_v = some_v <|> pure []
    some_v = (:) <$> v <*> many_v

-- | One or more.
sepBy1
  :: (Alternative f)
  => f a
  -> f b
  -> f [a]
sepBy1 p sep =
  (:) <$> p <*> (many $ sep *> p)

-- | Zero or more.
sepBy
  :: (Alternative f)
  => f a
  -> f b
  -> f [a]
sepBy p sep =
  sepBy1 p sep <|> pure []

--------------------------------------------------------------------------------

satisfy
  :: (Parseable s)
  => (Char -> Bool)
  -> Parser s Char
satisfy p =
  item `bind`
  \c ->
    if p c
    then unit c
    else Parser $ \_ -> []
    
--------------------------------------------------------------------------------

oneOf
  :: (Parseable s)
  => [Char]
  -> Parser s Char
oneOf s =
  satisfy $ flip elem s

chainl
  :: (Parseable s)
  => Parser s a
  -> Parser s (a -> a -> a)
  -> a
  -> Parser s a
chainl p op a =
  (p `chainl1` op) <|> return a

chainl1
  :: (Parseable s)
  => Parser s a
  -> Parser s (a -> a -> a)
  -> Parser s a
p `chainl1` op =
  do {a <- p; rest a}
  where
    rest a =
      (do f <- op
          b <- p
          rest (f a b)) <|> return a
      
--------------------------------------------------------------------------------

char
  :: (Parseable s)
  => Char
  -> Parser s Char
char c = satisfy (c ==)

string
  :: (Parseable s)
  => String
  -> Parser s String
string [] = return []
string (c:cs) = do { _ <- char c; _ <- string cs; return (c:cs)}

token
  :: (Parseable s)
  => Parser s a
  -> Parser s a
token p = do { a <- p; _ <- spaces ; return a}

reserved
  :: (Parseable s)
  => String
  -> Parser s String
reserved s = token (string s)

spaces
  :: (Parseable s)
  => Parser s String
spaces = many $ oneOf " \n\r"
    
--------------------------------------------------------------------------------

runParser
  :: (Parseable s)
  => Parser s a
  -> s
  -> Either String a
runParser m s =
  ps $ parse m s
  where
    ps [   ] = Left "Parser error."
    ps (x:_) = aux x
    aux x
      |       nil $ rest = Right $ fst $ x
      | not . nil $ rest = Left  $ "Parser didn't consume entire stream."
      | otherwise        = Left  $ "Parser error."
      where
        rest = snd x
{% endhighlight %}


#### Parsers/URL/Types.hs

{% highlight haskell linenos %}
module Parsers.URL.Types
  ( URI (..)
  , Scheme (..)
  , Authority (..)
  , Query (..)
  )
where

--------------------------------------------------------------------------------

-- URI = scheme:[//authority]path[?query][#fragment]
data URI
  = URI
  { scheme    ::       Scheme
  , authority ::       Authority
  , path      :: Maybe String
  , query     :: Maybe Query
  , fragment  :: Maybe String
  }
  deriving Show

data Scheme
  = HTTPS
  | HTTP
  | NotSupported
  deriving Show

-- authority = [userinfo@]host[:port]
data Authority
  = Authority
  { userinfo :: Maybe String
  , host     ::       String
  , port     :: Maybe Int
  }
  deriving Show

newtype Query
  = Query
    { keyValues :: [ (String, Maybe String) ]
    }
  deriving Show

-- Reference:
--
-- https://en.wikipedia.org/wiki/URL#Syntax
{% endhighlight %}


#### Parsers/URL/Internal.hs

{% highlight haskell linenos %}
{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Parsers.URL.Internal
  ( uri
  )
where

--------------------------------------------------------------------------------

import           Control.Applicative.Alternative
  ( Alternative
    ( (<|>)
    )
  )
import           Parsers.NanoParsec
import           Parsers.URL.Types

--------------------------------------------------------------------------------

data Optional
  = OPath     String (Maybe Optional)
  | OQuery    Query  (Maybe Optional)
  | OFragment String
  deriving Show

--------------------------------------------------------------------------------

scheme'
  :: (Parseable s)
  => Parser s Scheme
scheme' =
  do
    -- tdammers, #haskell freenode, on how to avoid backtracking:
    -- do { string "http"; optional "https" (string "s" >> pure "https") }
    s <- string "https" <|> string "http"
    _ <- reserved ":"
    return $ aux s
    where
      aux "https" = HTTPS
      aux "http"  = HTTP
      aux _______ = NotSupported

userinfo'
  :: (Parseable s)
  => Parser s (Maybe String)
userinfo' =
  do
    u <- some noseparator
    _ <- reserved "@"
    return $ Just u
    where
      noseparator =
        satisfy ('@' /=)

host'
  :: (Parseable s)
  => Parser s String
host' =
  do
    h <- some noseparators
    return $ h
    where
      noseparators =
        satisfy $ \c -> ':' /= c && '/' /= c

port'
  :: (Parseable s)
  => Parser s (Maybe Int)
port' =
  do
    _ <- reserved ":"
    p <- some noseparator
    return $ Just $ read p
    where
      noseparator = satisfy ('/' /=)

authority'
  :: (Parseable s)
  => Parser s Authority
authority' =
  do
    _ <- reserved "//"
    u <- userinfo' <|> return Nothing
    h <- host'
    p <- port'     <|> return Nothing
    return $ Authority u h p

nooseparators
  :: (Parseable s)
  => Parser s Char
nooseparators =
  satisfy
  $ \c ->
      ':' /= c &&
      '/' /= c &&
      '?' /= c &&
      '&' /= c &&
      '=' /= c &&
      '#' /= c

path'
  :: (Parseable s)
  => Parser s String
path' =
  do
    _ <- some $ reserved "/"
    p <- many nooseparators
    return $ p

opath
  :: (Parseable s)
  => Parser s (Maybe Optional)
opath =
  do
    p <- path'
    o <- optional
    return $ Just $ OPath p o

query'
  :: (Parseable s)
  => Parser s Query
query' =
  do
    _ <- reserved "?"
    p <- pair `sepBy` char '&'
    return $ Query p
    where
      might m =
        do
          a <- m
          return $ Just $ a
      pair =
        do
          key <- some nooseparators
          ___ <- reserved "="
          val <- (might $ some nooseparators) <|> return Nothing
          return (key, val)
      
oquery
  :: (Parseable s)
  => Parser s (Maybe Optional)
oquery =
  do
    q <- query'
    o <- optional
    return $ Just $ OQuery q o

fragment'
  :: (Parseable s)
  => Parser s String
fragment' =
  do
    _ <- reserved "#"
    p <- many nooseparators
    return $ p

ofragment
  :: (Parseable s)
  => Parser s (Maybe Optional)
ofragment =
  do
    q <- fragment'
    return $ Just $ OFragment q

optional
  :: (Parseable s)
  => Parser s (Maybe Optional)
optional =
  do
    o <- opath <|> oquery <|> ofragment <|> return Nothing
    return $ o

uri
  :: (Parseable s)
  => Parser s URI
uri =
  do
    s <- scheme'
    a <- authority'
    o <- optional
    return $ aux s a o
    where
      aux s a o = URI s a p q f
        where 
          (p,q,f) =
            opt (Nothing, Nothing, Nothing) o
      opt (p,q,f) (Just o) =
        case o of
          OPath     p' mo -> opt (Just p',      q ,      f ) mo
          OQuery    q' mo -> opt (p      , Just q',      f ) mo
          OFragment f'    ->     (p      ,      q , Just f')
      opt (p,q,f) (Nothing) =
        (p,q,f)
{% endhighlight %}


#### Parsers/URI.hs

{% highlight haskell linenos %}
module Parsers.URL
  ( parse
  )
where

--------------------------------------------------------------------------------

import qualified Parsers.NanoParsec   as NP
import           Parsers.URL.Internal
import qualified Parsers.URL.Types    as URL
 
--------------------------------------------------------------------------------

parse
  :: (NP.Parseable s)
  => s
  -> Either String URL.URI
parse url =
  NP.runParser uri url
{% endhighlight %}


#### Main.hs

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-11.7
   --install-ghc
   runghc
   --package bytestring
   --package monadplus
   --
   -Wall -Werror
-}

--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString       as BS
import qualified Parsers.URL           as URL

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  do
    putStrLn $ show $ URL.parse url
    putStrLn ""
    putStrLn $ show $ URL.parse url'
    where
      -- Call the parse on a regular string
      url  :: String
      url  = "https://johndoe@www.example.com:8433/foo?bar=42&baz=#qux"
      -- But also call it on a ByteString
      url' :: BS.ByteString
      url' = "https://johndoe@www.example.com:8433/foo?bar=42&baz=#qux"
{% endhighlight %}


### Output:

{% highlight text %}
user@personal:~/.../src$ ./Main.hs 
Right
  ( URI
    { scheme = HTTPS
    , authority =
      Authority
      { userinfo = Just "johndoe"
      , host = "www.example.com"
      , port = Just 8433
      }
    , path = Just "foo"
    , query =
      Just
      ( Query
        { keyValues =
          [ ("bar",Just "42")
          , ("baz",Nothing)
          ]
        }
      )
    , fragment = Just "qux"
    }

Right
  ( URI
    { scheme = HTTPS
    , authority =
      Authority
      { userinfo = Just "johndoe"
      , host = "www.example.com"
      , port = Just 8433
      }
    , path = Just "foo"
    , query =
      Just
      ( Query
        { keyValues =
          [ ("bar",Just "42")
          , ("baz",Nothing)
          ]
        }
      )
    , fragment = Just "qux"
    }
user@personal:~/.../src$ 
{% endhighlight %}

> **Note**: Output has been prettyfied to fit in this code snippet.

### References:

* Write You a Haskell (Stephen Diehl):
  - [NanoParsec][nanoparsec]
* Wikipedia:
  - [URL - Syntax][urlsyntax]
  
[nanoparsec]: http://dev.stephendiehl.com/fun/002_parsers.html#nanoparsec
[urlsyntax]:  https://en.wikipedia.org/wiki/URL#Syntax
