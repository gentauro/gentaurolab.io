--- 
layout: post
title: Haskell - Safe DAG only depending on Prelude
categories:
  - English
tags:
  - snippet
  - haskell
  - safe
  - dag
  - prelude
time: "13:28"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2019-03-24-haskell-safe-dag-only-depending-on-prelude_topological-ordering.png" />
<figcaption>
A topological ordering of a DAG, author David Eppstein at Wikipedia (CC0 1.0)
</figcaption>
</figure>
</div>


### Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Control.Arrow
  ( first
  )
import           Data.Bits
  ( xor
  , (.&.)
  )
import           Data.List
  ( sort
  )
import           Data.Maybe
  ( catMaybes
  )

--------------------------------------------------------------------------------

newtype Vertices a =
  V (Map Integer a)

newtype Edges =
  E (Map Integer [Integer])

data DAG a = G (Vertices a) Edges

--------------------------------------------------------------------------------

-- F U N C T I O N A L  P E A R L S
-- Red-Black Trees in a Functional Setting
--
-- CHRIS OKASAKI
-- School of Computer Science, Carnegie Mellon University
-- 5000 Forbes Avenue, Pittsburgh, Pennsylvania, USA 15213
-- (e-mail: cokasaki@cs.cmu.edu)

-- Red-Black Trees
data Color  = R | B
data Tree a = L | T Color (Tree a) a (Tree a)

instance Show Color where
  show R = "Red"
  show B = "Black"

instance Show a => Show (Tree a) where
  show = aux 0
    where
      aux l  L            =
        replicate l ' ' ++ "nil"
      aux l (T c lt x rt) =
        replicate l ' ' ++ show c ++ ": " ++ show x ++ "\n" ++
        replicate n ' ' ++ aux  n lt                ++ "\n" ++
        replicate n ' ' ++ aux  n rt
        where
          n = l + 1

-- Simple Map Operations
type Map k a = Tree (k, a)

empty :: Map k a
empty = L

member :: Ord k => k -> Map k a -> Maybe a
member _  L = Nothing
member k (T _ a (k', y) b)
  | k <  k' = member k a
  | k == k' = Just y
  | k >  k' = member k b
member _ (T _ _ (_,  _) _) = Nothing

insert :: Ord k => k -> a -> Map k a -> Map k a
insert k v m =
  blk . aux $ m
  where
    blk (T _ a y b) = T B a y b
    blk ___________ = error "Shouldn't be possible (insert -> blk)"
    aux  L = T R L (k,v) L
    aux (T c a y@(k', _) b)
      | k <  k' = bal c (aux a) y      b
      | k == k' = T   c      a  y      b
      | k >  k' = bal c      a  y (aux b)
    aux ___________ = error "Shouldn't be possible (insert -> aux)"
    bal B (T R (T R a x b) y c) z d = T R (T B a x b) y (T B c z d)
    bal B (T R a x (T R b y c)) z d = T R (T B a x b) y (T B c z d)
    bal B a x (T R (T R b y c) z d) = T R (T B a x b) y (T B c z d)
    bal B a x (T R b y (T R c z d)) = T R (T B a x b) y (T B c z d)
    bal c a x b                     = T c a x b

toList :: Map k a -> [(k, a)]
toList  L          = [                         ]
toList (T _ a x b) = toList a ++ [x] ++ toList b

--------------------------------------------------------------------------------

-- FNV Hash
--
-- http://isthe.com/chongo/tech/comp/fnv/#FNV-1a

fnv1a :: (Show a) => a -> Integer
fnv1a =
  foldl ( \ a x -> ((a `xor` x) * prm) .&. bin) off . aux . show
  where
    aux = map (fromIntegral . fromEnum)
    prm = 309485009821345068724781371
    off = 144066263297769815596495629667062367629
    bin = 340282366920938463463374607431768211455

--------------------------------------------------------------------------------

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys =
  (,) <$> xs <*> ys

vertices :: Ord a => DAG a -> [a]
vertices (G (V vs) _) =
  sort $ map snd $ toList vs

edges :: Ord a => DAG a -> [(a, a)]
edges (G (V vs) (E es)) =
  reverse $
  sort    $
  concat  $
  map (uncurry cartProd) $
  map (first (:[]))      $
  zip fs tss
  where
    (xs,ys) = unzip $ toList es
    fs  =     catMaybes $ map       (flip member vs) xs
    tss = map catMaybes $ map (map $ flip member vs) ys

new :: (Show a) => a -> DAG a
new v =
  G (V $ insert hsv v empty) (E empty)
  where
    hsv = fnv1a v

add :: (Eq a, Show a) => [a] -> a -> DAG a -> Either String (DAG a)
add ps v (G (V vs) (E es)) =
  case member hsv vs of
    Just  _ -> Left "Vertex, is already in the graph"
    Nothing ->
      if all (/= Nothing) $ map (flip member vs) hsps
      then
        Right $ G
        (V $ insert hsv v vs)
        (E $ foldl ( \ a ph -> insert hsv ph a) es [ hsps ])
      else
        Left "One of the parent vertices, doesn't exist"
  where
    hsps = map fnv1a ps
    hsv  =     fnv1a v

--------------------------------------------------------------------------------

main :: IO ()
main =
  putStrLn "# Example 1 (single neighbor): " >>
  (
    case dag1 of
      Left msg -> putStrLn msg
      Right  g ->
        (putStrLn $ "vertices: " ++ (show $ vertices g)) >>
        (putStrLn $ "edges:    " ++ (show $ edges    g))
  ) >>
  putStrLn "" >>
  putStrLn "# Example 2 (multiple neighbors, Wikipedia diagram): " >>
  (
    case dag2 of
      Left msg -> putStrLn msg
      Right  g ->
        (putStrLn $ "vertices: " ++ (show $ vertices g)) >>
        (putStrLn $ "edges:    " ++ (show $ edges    g))
  )
  where
    dag1 =
      foldl (\ g (p,v) -> g >>= add p v) x xs
      where
        x  = Right $ new 1
        xs = take 10 $ zip (map (:[]) nat) $ drop 1 nat
    nat = 1 : map (+ 1) nat :: [ Integer ]
    dag2 =
      -- DAG example from Wikipedia:
      -- https://en.wikipedia.org/wiki/File:Topological_Ordering.svg
      (Right $ new 'a')
      >>= add ['a']     'b' >>= add ['b'] 'c' >>= add ['a']     'd'
      >>= add ['d']     'e' >>= add ['d'] 'f' >>= add ['b','f'] 'g'
      >>= add ['e','g'] 'h'
{% endhighlight %}


### Code Output:

{% highlight text %}
# Example 1 (single neighbor): 
vertices: [1,2,3,4,5,6,7,8,9,10,11]
edges:    [(11,10),(10,9),(9,8),(8,7),(7,6),(6,5),(5,4),(4,3),(3,2),(2,1)]

# Example 2 (multiple neighbors, Wikipedia diagram): 
vertices: "abcdefgh"
edges:    [('h','g'),('h','e'),('g','f'),('g','b'),('f','d'),('e','d'),('d','a')
          ,('c','b'),('b','a')
          ]
{% endhighlight %}


### References:

* Wikipedia:
  - [Directed acyclic graph (DAG)][dag]
* Haskell - Research papers/Functional pearls:
  - [Red-black trees in a functional setting, Chris Okasaki (1999)][rsto]

[dag]:  https://en.wikipedia.org/wiki/Directed_acyclic_graph
[rsto]: https://www.usma.edu/eecs/SiteAssets/SitePages/Faculty%20Publication%20Documents/Okasaki/jfp99redblack.pdf
