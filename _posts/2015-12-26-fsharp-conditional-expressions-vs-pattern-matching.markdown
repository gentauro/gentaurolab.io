--- 
layout: post
title: F# - Conditional Expressions vs. Pattern Matching
categories:
  - English
tags:
  - snippet
  - f#
time: "09:47"
---

### Code Snippet:

{% highlight ocaml linenos %}
module Random =
  open System
  let private r = new Random()
  let coinToss = fun _ -> r.Next(0,2)

let printNumbers n =
  match n % 2 = 0 with
  | true  -> printfn "Even nr. %i" n
  | false -> printfn "Odd nr. %i" n

let printNumbers' n =
  // Missed "else" statement is found at runtime					
  if n % 2 = 0
  then printfn "Even nr. %i" n

List.init 10 (Random.coinToss)
|> List.iter printNumbers

List.init 10 (Random.coinToss)
|> List.iter printNumbers'
{% endhighlight %}

### Code output:

{% highlight text %}

>
module Random = begin
  val private r : System.Random
  val coinToss : 'a -> int
end
val printNumbers : n:int -> unit
val printNumbers' : n:int -> unit

> Even nr. 0
Odd nr. 1
Even nr. 0
Odd nr. 1
Even nr. 0
Odd nr. 1
Even nr. 0
Even nr. 0
Odd nr. 1
Odd nr. 1
val it : unit = ()

> Even nr. 0
Even nr. 0
Even nr. 0
Even nr. 0
Even nr. 0
val it : unit = ()
{% endhighlight %}

### Code Snippet:

{% highlight ocaml linenos %}
let printNumbers'' n =
  // Missed "false" match is found at compile-time
  match n % 2 = 0 with
  | true  -> printfn "Even nr. %i" n

List.init 10 (Random.coinToss)
|> List.iter printNumbers''
{% endhighlight %}

### Code output:

{% highlight text %}
>
/Users/mon/tmp/conditionalexpressionsVSpatternmatch.fsx(22,9): error FS0025: Incomplete pattern matches on this expression. For example, the value 'false' may indicate a case not covered by the pattern(s).
{% endhighlight %}

### Configuration Emacs (~/.emacs.d/init.el):

<img
src="/assets/img/posts/2015-12-26-fsharp-conditional-expressions-vs-pattern-matching_a-must-error-25-emacs.png"
alt="All">

### Configuration Visual Studio:

<img
src="/assets/img/posts/2015-12-26-fsharp-conditional-expressions-vs-pattern-matching_a-must-error-25-vs.png"
alt="All">

### References:

* MSDN:
  - [Conditional Expressions](https://msdn.microsoft.com/en-us/library/dd233231.aspx)
  - [Pattern Matching](https://msdn.microsoft.com/en-us/library/dd547125.aspx)

