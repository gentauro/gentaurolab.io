---
layout: post
title: DAXIF# - Delegate Automated Xrm Installation Framework
categories:
  - English
tags: 
  - daxif#
  - f# 
  - fsharp
time: "06:39"
---

I've been employed @ [Delegate A/S](http://www.delegate.dk/) for about a year.
In this short period I have created some tools for our CRM
developers/consultants in order to make working with [Microsoft Dynamics
CRM](http://crm.dynamics.com/) more smoothly. One of these tools is DAXIF# which
is defined as *A set of tools that in combination with other MS tools make it
easier to work with CRM/xRM on a daily basis (also for developers who are not
familiar with the platform)*

The interface is through F# script files that can be executed from a command
prompt or directly from Visual Studio (the best IDE for F# scripts):

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_01_daxifsharp.png" alt="All">

The main reason to use F# to create this set of tools is as usual the same sales
speech we use to give again and again and again: *Error free projects with
smaller code base, where there is a need to use one programming language
(no. Bat files or PowerShell, ...). Where big data, external data sources,
parallelism, concurrency, asynchronous processor are trivial to use.*:

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_02_daxifsharp.png" alt="All">

One of the things I learned from this project was that I actually could make F#
scripted and self documented Unit Test that can be executed without having to
build the final .DLL:

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_03_unittest.png" alt="All">

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_04_unittest.png" alt="All">

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_05_unittest.png" alt="All">

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_06_unittest.png" alt="All">

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_07_unittest.png" alt="All">

DAXIF# is proprietary so you will need a license to use it. We don't provide
licenses to other CRM Partner/competitors

Keep updated for the upcoming website and NuGet package.

For more information on DAXIF# and the presentations, please look into these
slides:

* Link to slides from MF#K (English): [Slides](http://goo.gl/Xc7hEv)

* Link to slides from CRM Partner Community (Danish):
  [Slides](http://goo.gl/RqoPUW)

* Geek alert: A few references to *Dota 2* might appear in the code:

<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_08_tribute_dota.png" alt="All">

or in the project structure:
<img src="/assets/img/posts/2013-12-05-daxifsharp-delegate-automated-xrm-installation-framework_09_tribute_dota.png" alt="All">
