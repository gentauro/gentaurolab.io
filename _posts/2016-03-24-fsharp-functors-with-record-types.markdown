--- 
layout: post
title: F# - Functors with Record Types
categories:
  - English
tags:
  - snippet
  - f#
time: "02:49"
---

### Code Snippet:

{% highlight ocaml linenos %}
type ('a) Set =
  private
    { empty: 'a t
      add: 'a -> 'a t -> 'a t
      exists: 'a -> 'a t -> bool }
  member x.Empty = x.empty
  member x.Add y ys = x.add y ys
  member x.Exists y ys = x.exists y ys
  static member Functor (orderType) : 'a Set =
    { empty = { t = Nil }
      add = fun x xs ->
        let rec add y = function
        | Nil         -> Cons(y,Nil)
        | Cons(hd,tl) ->
          match orderType.compare y hd with
          | Less -> Cons(x,xs.t)
          | Equal -> xs.t
          | Greater -> Cons(hd,add y tl)
        { t = add x xs.t }
      exists = fun x xs ->
        let rec exists y = function
        | Nil -> false
        | Cons(hd,tl) ->
          match orderType.compare y hd with
          | Less -> false
          | Equal -> true
          | Greater -> exists y tl
        exists x xs.t }
and ('a) t = private { t : 'a s }
and ('a) s = private Cons of 'a * 'a s | Nil
and ('a) OrderType = { compare: 'a -> 'a -> Comparison }
and Comparison = Less | Equal | Greater
{% endhighlight %}

### Code output:

{% highlight text %}
> 
type 'a Set =
  private {empty: 'a t;
           add: 'a -> 'a t -> 'a t;
           exists: 'a -> 'a t -> bool;}
  with
    member Add : y:'a -> ys:'a t -> 'a t
    member Exists : y:'a -> ys:'a t -> bool
    member Empty : 'a t
    static member Functor : orderType:'a OrderType -> 'a Set
  end
and 'a t =
  private {t: 'a s;}
and 'a s =
  private | Cons of 'a * 'a s
          | Nil
and 'a OrderType =
  {compare: 'a -> 'a -> Comparison;}
and Comparison =
  | Less
  | Equal
  | Greater
{% endhighlight %}

### Code Snippet:

{% highlight ocaml linenos %}
let floatSet =
  Set.Functor
    { compare = fun (x:float) (y:float) ->
        if x = y then
          Equal
        else if x < y then
          Less
        else
          Greater }

floatSet.Empty
|> floatSet.Add 43.
|> floatSet.Add 42.
|> floatSet.Exists 42.

let stringSet =
  Set.Functor
    { compare = fun (x:string) (y:string) ->
        if x = y then
          Equal
        else if x < y then
          Less
        else
          Greater }

stringSet.Empty
|> stringSet.Add "43"
|> stringSet.Add "42"
|> stringSet.Exists "42"
{% endhighlight %}

### Code output:

{% highlight text %}
> 
val floatSet : float Set
val it : bool = true
> 
val stringSet : string Set
val it : bool = true
{% endhighlight %}

### References:

* Part I - An introduction to OCaml:
  - [Chapter 2 The module system - Functors](http://caml.inria.fr/pub/docs/manual-ocaml/moduleexamples.html#sec20)

