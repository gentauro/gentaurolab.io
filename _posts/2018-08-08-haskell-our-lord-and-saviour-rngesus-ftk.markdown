--- 
layout: post
title: Haskell - Our Lord and Saviour RNGesus (FTK)
categories:
  - English
tags:
  - snippet
  - lord
  - saviour
  - rngesus
  - ftk
time: "10:03"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2018-08-08-haskell-our-lord-and-saviour-rngesus-ftk_rngesus-is-real.jpg" />
<figcaption>
Our Lord and Saviour with Felicia Day (for source, see References below)
</figcaption>
</figure>
</div>

### Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --package random
   --
-}

--------------------------------------------------------------------------------

module RNGesus (main) where

--------------------------------------------------------------------------------

import           System.Random
    ( newStdGen
    , randomRs
    )

--------------------------------------------------------------------------------

ppbool
  :: Bool
  -> Char

split
  :: Int
  -> [a]
  -> [[a]]

ppsplit
  :: Foldable t
  => t String
  -> IO ()

roll
  :: Int
  -> Int
  -> IO [ Bool ]

loop
  :: Int
  -> Int
  -> Int
  -> IO [String]

main
  :: IO ()

--------------------------------------------------------------------------------

ppbool True  = '■'
ppbool False = '□'

split _ [] = [              ]
split n xs = y : (split n ys)
  where
    (y,ys) = splitAt n xs

ppsplit xs =
  do
    putStr $ replicate 2 ' '
    mapM_ (putStr . (++ " ")) xs
    putStrLn ""

roll s n =
  -- `randomRs` is left-closed and right-closed
  newStdGen >>= pure . (map (< s)) . take n . randomRs (0,99)

loop =
  aux []
  where
    aux acc 0 _ _ = pure acc
    aux acc n s r =
      roll s r >>= \v -> aux (map ppbool v:acc) (n - 1) s r

main =
  do
    putStrLn "# FTK Hunter starts with 78 awareness and a Hunting Bow (3 roll)"
    putStrLn "256 rolls with 3 dices and 78 awareness:"
    putStrLn "```"
    loop 256 78 3 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "# We aim to max out awareness and use a Royal Bow (3 roll)"
    putStrLn "256 rolls with 3 dices and 95 awareness:"
    putStrLn "```"
    loop 256 95 3 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "As we can see, there are misses. What happens if we use 1 focus"
    putStrLn ""

    putStrLn "# FTK Hunter init: 78 awareness and a Hunting Bow"
    putStrLn "256 rolls with 2 dices and 78 awareness:"
    putStrLn "```"
    loop 256 78 2 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "# FTK Hunter end-game: 95 awareness and a Royal Bow"
    putStrLn "256 rolls with 2 dices and 95 awareness:"
    putStrLn "```"
    loop 256 95 2 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "As we can see, there are misses. What happens if we use 2 focus"
    putStrLn ""

    putStrLn "# FTK Hunter init: 78 awareness and a Hunting Bow"
    putStrLn "256 rolls with 1 dice and 78 awareness:"
    putStrLn "```"
    loop 256 78 1 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "# FTK Hunter end-game: 95 awareness and a Royal Bow"
    putStrLn "256 rolls with 1 dice and 95 awareness:"
    putStrLn "```"
    loop 256 95 1 >>= \ xs -> mapM_ ppsplit (split 16 xs)
    putStrLn "```"
    putStrLn ""

    putStrLn "And again we see misses. For more info, lookup `Probability` at:"
    putStrLn "- https://en.wikipedia.org/wiki/Probability"
    putStrLn ""
{% endhighlight %}

### Code Output:

{% highlight text %}
# FTK Hunter starts with 78 awareness and a Hunting Bow (3 roll)
256 rolls with 3 dices and 78 awareness:
```
  ■■■ ■■□ ■■□ ■■■ ■■■ ■■□ ■■■ ■□■ ■■■ ■□■ ■■■ ■■■ □■□ ■■■ ■■□ □□■ 
  ■■■ ■□■ □■□ ■■■ ■□□ ■■□ ■■□ ■□□ □■■ ■■■ ■■■ ■■□ ■■■ ■■■ □■■ ■■■ 
  ■■■ □■■ ■■■ ■■□ ■■□ ■■□ ■■□ ■■■ □■■ ■■■ ■■■ □■■ ■□■ □■■ ■□■ ■■■ 
  ■■■ ■□■ ■■□ ■■□ ■■■ ■■■ □■■ □■■ ■■■ □□■ □■■ ■■■ ■□■ ■□■ ■■□ ■□■ 
  □■■ ■■■ ■■■ ■□□ ■■■ ■■□ ■□□ ■■□ ■□■ □■■ ■■■ □■■ ■■■ ■□■ ■■■ ■■□ 
  □■■ □■■ ■■■ ■■■ ■■■ ■□■ □□■ □■■ ■□■ ■■□ □■■ ■■■ □■□ ■■■ ■□■ ■□■ 
  ■□□ ■■□ ■■■ ■■■ □■■ ■□□ ■□■ □■■ ■■■ □■■ ■■□ ■■□ ■■■ □□■ ■□■ ■■■ 
  ■■□ ■■■ ■□■ ■■□ ■■■ ■■■ ■□■ ■■■ ■□■ ■■■ ■■■ ■■■ □■■ □■■ ■■■ ■■■ 
  □■■ ■■■ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ □□■ ■■□ ■■■ □□■ 
  ■■■ ■■■ ■■■ ■□■ ■■■ ■■□ ■■■ ■■■ ■■■ □■■ □■■ ■■■ ■□□ ■■■ ■□□ □■■ 
  ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■□■ ■□□ □□□ ■■■ ■■■ ■■■ ■■□ 
  □□■ ■■■ ■■□ ■■□ ■■■ ■□■ ■■■ ■■■ □■■ □■□ □□□ ■■■ ■■□ ■■■ ■■■ ■■□ 
  ■□■ □■■ □■■ □■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ □□■ ■■■ ■■□ ■■■ ■□■ ■■■ 
  ■■■ □■□ ■■■ □■■ □■■ □■■ □■□ □□■ ■□□ ■■■ ■□■ ■■■ □■□ □■■ ■■■ ■■■ 
  ■■■ ■■■ □■□ ■■■ ■■■ ■■□ ■■■ ■■■ ■■□ □■■ ■□■ ■■■ ■■■ ■■■ ■■■ □■■ 
  ■□■ ■■■ ■■■ ■□■ □■■ ■■■ ■□■ ■■■ ■■■ ■■■ □■■ ■■■ □■□ □■■ ■■■ ■■■ 
```

# We aim to max out awareness and use a Royal Bow (3 roll)
256 rolls with 3 dices and 95 awareness:
```
  ■■■ ■■■ ■■■ ■■■ ■□■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■□ ■■■ ■■■ ■■■ ■■■ 
  ■■□ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■□ ■■■ □□■ ■□■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■■ ■■■ ■■■ □■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■■ ■■□ ■■■ ■■■ ■■□ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■□ ■■□ ■■■ ■■□ 
  ■■■ ■■□ ■■■ ■□■ ■■■ ■■□ ■■■ ■■■ ■■□ ■■■ ■■■ ■■■ ■■■ ■■□ ■■■ ■■■ 
  ■■■ ■■■ ■■■ ■■■ ■□■ ■■■ ■■□ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■□■ ■■□ ■■■ 
  ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■□ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ 
  ■■□ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■□■ ■■■ ■□■ 
  ■■■ ■■■ ■■■ ■■□ ■■■ □■■ ■■■ ■■■ ■□■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  □■■ ■■□ ■■■ ■■□ ■■■ ■■■ ■■■ ■□■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■□ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■□ 
  □■■ ■■■ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
  ■■■ ■■■ ■□■ ■■■ □■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ ■■■ 
```

As we can see, there are misses. What happens if we use 1 focus

# FTK Hunter init: 78 awareness and a Hunting Bow
256 rolls with 2 dices and 78 awareness:
```
  □■ ■■ □□ □■ □■ ■□ ■■ ■■ ■□ ■■ ■■ ■■ ■□ ■■ □■ □■ 
  □■ ■■ ■■ ■□ ■□ ■■ ■■ ■■ ■□ □■ ■□ ■■ ■■ ■■ ■■ □■ 
  ■■ ■■ ■■ ■□ ■□ ■■ □■ ■■ ■■ ■■ □■ □■ ■□ ■■ ■■ ■□ 
  ■■ ■■ ■■ ■■ ■■ ■■ □□ ■□ ■□ ■□ ■■ ■■ □■ ■■ ■■ ■■ 
  □□ ■■ ■□ ■■ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■■ ■■ 
  ■■ ■■ ■□ □■ ■■ ■■ ■■ ■□ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■■ 
  ■■ □■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ □■ ■■ ■■ ■□ ■■ ■■ 
  ■□ ■■ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■□ □■ □■ ■■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■□ ■■ ■□ ■■ ■■ □■ ■□ □□ ■■ ■■ ■■ ■■ 
  ■□ ■■ ■□ ■■ ■■ ■■ ■□ □□ ■■ ■■ ■■ ■□ ■□ ■■ ■■ ■□ 
  ■■ □□ □■ ■■ ■■ ■■ ■■ ■□ ■■ □■ ■■ ■□ ■■ ■■ ■■ ■■ 
  ■■ ■■ □□ ■■ □□ □■ ■□ □■ ■■ ■■ ■□ ■■ □□ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■□ ■□ ■■ ■■ ■■ □■ ■■ ■□ □■ ■□ ■■ □■ 
  ■■ □□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ ■■ ■□ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■□ ■■ ■□ ■■ ■■ ■■ ■■ □■ ■■ □■ ■■ ■■ ■■ 
  ■■ □■ ■■ □□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ □□ □■ ■□ 
```

# FTK Hunter end-game: 95 awareness and a Royal Bow
256 rolls with 2 dices and 95 awareness:
```
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ 
  ■■ ■□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ □■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■□ 
  ■□ ■■ ■■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ □□ ■□ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■□ ■□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■□ ■■ ■■ ■■ □■ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ □■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■□ ■■ ■■ ■■ 
  ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■□ ■■ ■■ ■■ ■■ 
  □■ ■■ ■■ ■■ □■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ 
  ■■ ■□ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ ■■ 
```

As we can see, there are misses. What happens if we use 2 focus

# FTK Hunter init: 78 awareness and a Hunting Bow
256 rolls with 1 dice and 78 awareness:
```
  ■ □ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ □ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ ■ ■ 
  ■ ■ □ ■ □ ■ ■ ■ □ ■ ■ ■ □ ■ ■ ■ 
  ■ ■ □ □ ■ ■ ■ ■ ■ □ ■ ■ □ ■ ■ ■ 
  ■ ■ □ □ ■ ■ ■ □ □ ■ ■ ■ ■ ■ ■ ■ 
  ■ □ □ ■ ■ ■ □ ■ ■ ■ □ □ ■ ■ ■ ■ 
  ■ □ ■ ■ □ ■ □ ■ ■ ■ ■ ■ □ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ ■ ■ ■ ■ 
  □ ■ ■ ■ ■ ■ □ ■ ■ □ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ □ ■ ■ ■ ■ 
  □ □ ■ □ ■ □ ■ ■ □ ■ □ ■ ■ ■ ■ ■ 
  □ ■ ■ □ □ □ ■ □ ■ □ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ □ □ ■ ■ ■ □ ■ ■ ■ ■ □ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ 
```

# FTK Hunter end-game: 95 awareness and a Royal Bow
256 rolls with 1 dice and 95 awareness:
```
  ■ ■ □ □ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ □ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ □ ■ ■ □ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ □ ■ 
  ■ ■ ■ ■ ■ ■ □ ■ ■ □ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ 
  □ ■ ■ □ ■ ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ 
  ■ ■ ■ ■ ■ □ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ □ ■ ■ ■ ■ ■ ■ ■ ■ 
  □ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ □ ■ ■ ■ □ □ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ □ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
  ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ 
```

And again we see misses. For more info, lookup `Probability` at:
- https://en.wikipedia.org/wiki/Probability
{% endhighlight %}


### References:

* Twitter:
  - [Felicia Day with RNGesus][twitter]
* For the King Wiki:
  - [Hunter][hunter]
  - [Hunting Bow][huntingbow]
  - [Royal Bow][royalbow]
* GitLab:
  - [Output as a Markdown snippet][gitlab]

[twitter]:    https://twitter.com/feliciaday/status/638103393232224257
[hunter]:     https://fortheking.gamepedia.com/Hunter
[huntingbow]: https://fortheking.gamepedia.com/Hunting_Bow
[royalbow]:   https://fortheking.gamepedia.com/Royal_Bow
[gitlab]:     https://gitlab.com/snippets/1741598
