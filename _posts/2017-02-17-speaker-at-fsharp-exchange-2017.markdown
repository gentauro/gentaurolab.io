--- 
layout: post
title: Speaker at F# eXchange 2017 (#fsharpX)
categories:
  - English
tags:
  -f#
  -fsharp
  -eXchange
  -fsharpX
  -speaker
time: "14:18"
---

I am pleased and honored to announce that I will be a speaker at this
years [F# eXchange 2017][fsharpx] conference.

### F# Community

It's almost [3 years ago][flyfox] since I became acquainted
with [Skills Matter][sm] and the [F# Community][fsharp]. I had spoken previously
with [Phillip Trelford][ptrelford] because I have co-founded
the [F#unctional Copenhageners Meetup Group][mfk], which is like a sisterhood
to [F#unctional Londoners Meetup Group][london]. In another case, due to work
with *Microsoft Dynamics CRM*, I had come across [Ross McKinlay][ross] (I still
owe you some beers). Both were present at the Meetup. 

When I arrived to the venue, I bumped into [Don Syme][dsyme] at the door. I was
really not expecting that. [Rob Lyndon][roblyndon] talk started and I was amazed
how this *punk rocker, with a Mohawk, looking guy* absolutely mastered coding
GPU kernels type-safely in F#. It was pretty amazing.

After the Meetup, we all went over and had a beer (or many) at a near bar. This
was great as it gave the possibility to meet many of the people who did
incredible work for the Community. I specially noticed [Tomas Petricek][tomasp],
due to the amazing tools that he has provided and which I have used have used
professionally: [F# Formatting][formatting], [F# Data][data], among others.

I think it was the night that [The Don][dsyme] agreed to give a talk at *MF#K*
as he would be in Copenhagen due to some [ICFP][icfp] related work. His talk is
still one of the most attended and I would argue that it was the cornerstone to
the foundation of our Meetup group when we are about to reach 700 members where
the next upcoming talk we are hosting, will be having [2 × F# MVPs][mvp] on
stage.

Time has passed, and *MF#K* have hosted a variety of functional programming
languages talks, most of them F# related, and I have personally given around a
dozen of F# talks and some 1-day courses. I recently held my very first
international abroad talk in Sweden (Malmö) to restart [SweNUG][swenug], the
Swedish .NET User Group, and my very last [talk][semver] might derive in a an
official [F# project][synver].

> **Note**: [Oskar][oskar], sorry for not being able to work on the project at
> the moment.

### F# eXchange 2017 Conference

That's why I write that I am honored to give a little back to F# Community
which have given me so much.

In addition to greet old acquaintances, I am really pleased and looking forward
to the strong program that has been put together:

* I had the pleasure to see this year's Conference Keynote, [Evelina][evelina],
  at NDC London last year. She is an incredibly facilitator that captures the
  attention of everyone attending the talk. Besides her unquestionable
  knowledge, she is also very hands on, key component that many forget when
  giving a talk. It also helps that she chooses topics which seems interesting
  for most of us.

* [Scott Wlaschin][scott] is without any doubt, one of my favorite people in the
  F# community. He has one of the best [learning sources][funandprofit], if not
  the best, he an incredible communicator who is able to teach very complex
  theory with analogies that most understand. Just an
  example [Railway Oriented Programming][rop].

* I'm really looking forward to meet [Michael Newton][mavnn]. I Have read time
  and time again his blog post on the subject of Type Providers and learned a
  lot from them, to the degree that it helped immensely to create one
  professionally [DAXIF][daxif]. Now, I can finally put a face to the blog.

* Last but not least, my fellow *paisano* [Alfonso Garcia-Caro][alfonsogcnunez]
  who in collaboration with *ncave* have made a fantastic job
  on [Fable][fable]. Really looking forward to his talk and maybe some
  discussion with regard of the [Motherland][spain] over a few beers.

To all the speakers I do not mention, it's because I do not know your work but I
am really looking forward to your talks !!!

The only `cons` I have noticed so far with regard of the conference is that
there are some cases where I would really like to see both talks scheduled at
the same time. But I guess that's what happens when a conference starts getting
bigger. Kudos [Skills Matter][sm].

Here is the title and abstract of my talk. I hope you will enjoy it and
hopefully, nobody will be disappointed:

#### Puritas, A journey of a thousand miles towards side-effect free code 

Puritas, from Latin, means linguistic correctness, from a lexical point of view.

In this talk, you will be exploring and walking down the bumpy road of trying to
bring side-effect free code to F#. The journey started, in Ramon's case, a few
years ago while working for an IT consultancy company where Ramon made:
Delegate.Sandbox, a library providing a Computation Expression named
SandboxBuilder, `sandbox { return 42 }`, which ensures that values returned from
the computation are I/O side-effects safe. The library is built-on top of .NETs
Partially Trusted Code Sandboxes. The path of exploration somehow stalled.

Now with new ideas under the hood and great tooling provided by Microsoft and
the amazing F# Community, the journey continues towards bringing this missed
feature to an already brilliant and loved programming language, that is used by
thousands in the magnitude of 4.

Hopefully one day, the reserved keyword: `pure` (The Holy Grail of F# keywords)
will be claimed, and we will be able to use it with ease as we do with other
keywords such as: `seq`, `async`, `query`, `lazy` and so on.

> **Link**: [F# eXchange 2017 full 2-days programme][program]

[fsharpx]: https://skillsmatter.com/conferences/8053-f-sharp-exchange-2017 "#fsharpX"
[flyfox]: https://www.meetup.com/FSharpLondon/events/156752972/ "The Flying Fox: Deep Machine Learning with F#"
[sm]: https://skillsmatter.com/
[fsharp]: http://fsharp.org/
[ptrelford]: https://twitter.com/ptrelford "aka Sean's Dad"
[mfk]: https://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/ "yes, MF#K stands for MotherF**kers!!!"
[london]: https://www.meetup.com/FSharpLondon/
[ross]: https://twitter.com/pezi_pink "aka The Squirrels Master"
[roblyndon]: https://twitter.com/roblyndon
[dsyme]: https://twitter.com/dsyme "The man himself"
[tomasp]: http://tomasp.net/ "TomASP.NET"
[formatting]: https://tpetricek.github.io/FSharp.Formatting/
[data]: http://fsharp.github.io/FSharp.Data/
[icfp]: http://icfpconference.org/ 
[mvp]: https://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/events/237296612/
[swenug]: http://www.foocafe.org/malmoe/events/1292-type-driven-development-tdd-and-idiomatic-data-structures
[semver]: https://blog.stermon.org/assets/talks/2016-11-29_Semantic_Versioning_for_dotNET_libraries_and_NuGet_packages.pdf
[synver]: https://github.com/fsprojects/SyntacticVersioning
[oskar]: https://twitter.com/ozzymcduff
[evelina]: https://twitter.com/evelgab
[scott]: https://twitter.com/scottwlaschin
[funandprofit]: http://fsharpforfunandprofit.com/
[rop]: http://fsharpforfunandprofit.com/rop/
[mavnn]: https://twitter.com/mavnn "Mr. TypeProvider"
[daxif]: https://github.com/delegateas/Delegate.Daxif/blob/master/src/Delegate.Daxif/Modules/TypeProvider.fs
[alfonsogcnunez]: https://twitter.com/alfonsogcnune
[fable]: http://fable.io/
[spain]: https://en.wikipedia.org/wiki/Spain
[program]: https://skillsmatter.com/conferences/8053-f-sharp-exchange-2017#program
