--- 
layout: post
title: Haskell - Model finity-state-machines (FSM) with types
categories:
  - English
tags:
  - snippet
  - haskell
  - fsm
  - finite
  - state
  - machine
  - type
  - system
time: "04:06"
---

### Code Snippets

#### FSM/FiniteStateMachine.hs

{% highlight haskell linenos %}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Safe                  #-}

--------------------------------------------------------------------------------

module FSM.FiniteStateMachine
  ( State
  , Transition
  , switch
  )
where

--------------------------------------------------------------------------------

class  State a                             where
class (State a, State b) => Transition a b where

--------------------------------------------------------------------------------

switch :: Transition a b => a -> b

--------------------------------------------------------------------------------

switch = undefined
{% endhighlight %}


#### Main.hs

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Safe                  #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           FSM.FiniteStateMachine
  ( State
  , Transition
  , switch
  )

--------------------------------------------------------------------------------

-- We define states and transitions into the type system

data Red
data Yellow
data Green

instance Show Red    where show _ = "Red"
instance Show Yellow where show _ = "Yellow"
instance Show Green  where show _ = "Green"

instance State Red
instance State Yellow
instance State Green

instance Transition Red    Green
instance Transition Green  Yellow
instance Transition Yellow Red

--------------------------------------------------------------------------------

-- We define our finite-state-machine (FSM)

data TrafficLight
  = Stop    Red
  | Caution Yellow
  | Go      Green
  deriving Show

--------------------------------------------------------------------------------

main :: IO ()
main =
  do
    putStrLn . ps $ show s0
    putStrLn . ps $ show s1
    putStrLn . ps $ show s2
    putStrLn . ps $ show s3
      where
        s0@(Stop    s0') = Stop    $ undefined
        s1@(Go      s1') = Go      $ switch s0'
        {- We can't implement code that goes from `Green` to `Red`:
           > si@(Stop si') = Stop $ switch s1'
           • No instance for (Transition Green Red)
               arising from a use of ‘switch’
        -}
        s2@(Caution s2') = Caution $ switch s1'
        s3               = Stop    $ switch s2'
        ps = ("The traffic light is: " ++)
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../fsm/lib$ ./Main.hs
The traffic light is: Stop Red
The traffic light is: Go Green
The traffic light is: Caution Yellow
The traffic light is: Stop Red
{% endhighlight %}


### References:

* Blog: Ramón Soto Mathiesen:
  - [Model state machines with types][fsm]
* Haskell Wiki:
  - [Smart constructors][smart]
  
[fsm]:   /articles/2016/10/06/model-state-machine-with-types.html
[smart]: https://wiki.haskell.org/Smart_constructors#Enforcing_the_constraint_statically
