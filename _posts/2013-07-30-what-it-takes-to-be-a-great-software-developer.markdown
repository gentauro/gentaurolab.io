---
layout: post
title: What it takes to be a great software developer
categories:
  - English
tags: 
  - software 
  - developer
time: "02:09"
---

I would see ideas in dreams. My mind was bursting with ideas. I would wake up in
the middle of the night. In dreams, I would have visions of code.

Once you decide on your occupation, you must immerse yourself in your work. You
have to fall in love with your work. Never complain about your job.

You must dedicate your life to mastering your skill. That's the secret of
success and is the key to being regarded honorably.

I've seen many software developers who are self-critical, but I've never seen
another software developer who is so hard on himself.

* He sets the standard for self-discipline.

* He is always looking ahead.

* He's never satisfied with his work.

* He's always trying to find ways to make the code better or to improve his
  skills. Even now, that's what he thinks about every day.

A great software developer has the following five attributes:

* First, they take their work very seriously and consistently perform on the
  highest level.

* Second, they aspire to improve their skills.

* Third is cleanliness. If the code doesn't feel clean, the program isn't going
  to perform well.

* Fourth attribute is impatience. They are not prone to collaboration. They're
  stubborn and insist on having things their own way.

What ties these attributes together is passion. That's what makes a great
software developer.

A great software developer must have all of these attributes.

A great software developer must be a perfectionist.