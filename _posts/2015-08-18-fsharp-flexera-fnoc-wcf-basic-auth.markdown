--- 
layout: post
title: F# - Flexera FNOC WCF with Basic Auth
categories:
  - English
tags:
  - snippet
  - f#
  - flexera
  - fnoc
  - wcf
  - basic
  - auth
time: "03:00"
---

### Code Snippet:

{% highlight ocaml linenos %}
#r "System.IdentityModel"
#r "System.ServiceModel"
#r "System.ServiceModel.Http"
#r "System.ServiceModel.Primitives"
#r "System.Runtime.Serialization"
#r "FSharp.Data.TypeProviders.dll"

open System
open System.IO
open System.Net
open System.Runtime.Serialization
open System.ServiceModel
open System.ServiceModel.Description
open System.ServiceModel.Dispatcher
open System.Text
open System.Web.Services.Protocols
open Microsoft.FSharp.Data.TypeProviders

type AuthorizationHeader(basicAuth) = 
  interface IClientMessageInspector with
    member x.AfterReceiveReply(reply, correlationState) = ()
    member x.BeforeSendRequest(request, channel) = 
      let prop = new Channels.HttpRequestMessageProperty()
      prop.Headers.Add(name = "Authorization", value = basicAuth)
      request.Properties.Add(Channels.HttpRequestMessageProperty.Name, prop) :> obj

type BasicAuthorization(usr : string, pwd : string) = 
  let bytes = Encoding.UTF8.GetBytes(usr + ":" + pwd)
  let auth = "Basic " + Convert.ToBase64String(bytes)
  interface IEndpointBehavior with
    member x.Validate(endpoint) = ()
    member x.AddBindingParameters(endpoint, bindingParameters) = ()
    member x.ApplyDispatchBehavior(endpoint, endpointDispatcher) = ()
    member x.ApplyClientBehavior(endpoint, clientRuntime) = 
      clientRuntime.ClientMessageInspectors.Add(new AuthorizationHeader(auth))

[<Literal>]
let baseurl = "https://DOMAIN_GOES_HERE.flexnetoperations.com/flexnet/services/"

[<Literal>]
let fnauth = baseurl + "FlexnetAuthentication?wsdl"

[<Literal>]
let userorghierarchy = baseurl + "UserOrgHierarchyService?wsdl"

type FlexnetAuthentication = WsdlService< ServiceUri=fnauth >
type UserOrgHierarchyService = WsdlService< ServiceUri=userorghierarchy >

let usr = "USERNAME_GOES_HERE"
let pwd = "PASSWORD_GOES_HERE"

/// Example 1: (require Basic Auth)
let user = new UserOrgHierarchyService.ServiceTypes.getUserPermissionsRequestType()
do user.userName <- "USERNAME_GOES_HER"
do user.domainName <- "FLEXnet"

let client1 = UserOrgHierarchyService.GetUserOrgHierarchyService()
do client1.DataContext.Endpoint.Behaviors.Add(new BasicAuthorization(usr, pwd))

let result1 = client1.getUserPermissions (user)

match result1.statusInfo.status with
| UserOrgHierarchyService.ServiceTypes.StatusType.SUCCESS -> 
  result1.responseData.permissions |> Array.iter (printfn "Permission: %s")
| _ -> printfn "%A" result1.statusInfo.status

/// Example 2: (doesn't requiere Basic Auth)
let auth = new FlexnetAuthentication.ServiceTypes.AuthenticateUserInputType()
do auth.userName <- usr
do auth.password <- pwd
do auth.domainName <- "FLEXnet"

let client2 = FlexnetAuthentication.GetFlexnetAuthentication()

let result2 = client2.authenticateUser (auth)

printfn "Access granted to %s: %b" usr result2.Success
{% endhighlight %}

### References:

* MSDN Blog: [Adding HTTP Headers to WCF
  Calls](http://blogs.msdn.com/b/mohamedg/archive/2012/12/13/adding-http-headers-to-wcf-calls.aspx)
