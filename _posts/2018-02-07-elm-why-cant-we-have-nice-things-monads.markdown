--- 
layout: post
title: Elm - Why can't we have nice things? (Monads)
categories:
  - English
tags:
  - snippet
  - elm
  - extensible
  - records
  - monads
time: "23:22"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2018-02-07-elm-why-cant-we-have-nice-things-monads_elm_meetup_mobilelife.jpg" />
<figcaption>
Elm Copenhagen, February 2018 Meetup @MobileLife
</figcaption>
</figure>
</div>

### Updated

With some input from **Basile Henry**, also attended the Meetup, I have made a
few modifications to the code in order to support `m :: * -> *` (`(a -> b) -> m
a -> m b`) as good as `Elm` allows us to do it, but without transitioning to a
signature hell as seen in `elm-data`, see **References** for more information on
that library.

#### Obviously not really monads

As a fellow [Coding Pirates][codingpirates] volunteer, **Hans Bugge Grathwohl** ,
pointed out:

{% highlight haskell linenos %}
odd :
  { fmap : (Int -> String) -> Float -> List Char
  , join : List Char -> String
  }
odd =
  { fmap = \f x -> String.toList (f (truncate x))
  , join = String.fromList
  }
			    
weird : String
weird = 4.5 |> bind odd toString
{% endhighlight %}

this can't really be considered `monads` as there is no relation between `a` and
`ma` as well as `mb` and `mmb`.

### Code Snippet

{% highlight haskell linenos %}
module Main exposing (..)

import Html exposing (br, div, text)


bind :
    { monad
        | fmap : (a -> mb) -> ma -> mmb
        , join : mmb -> mb
    }
    -> ((a -> mb) -> ma -> mb)
bind { fmap, join } f m =
    join (fmap f m)


maybe :
    { fmap : (a -> b) -> Maybe a -> Maybe b
    , join : Maybe (Maybe c) -> Maybe c
    }
maybe =
    { fmap = Maybe.map
    , join =
        \mm ->
            case mm of
                Nothing ->
                    Nothing

                Just m ->
                    m
    }


list :
    { fmap : (a -> b) -> List a -> List b
    , join : List (List c) -> List c
    }
list =
    { fmap = List.map
    , join = List.concat
    }


result :
    { fmap : (a -> b) -> Result c a -> Result c b
    , join : Result e (Result e d) -> Result e d
    }
result =
    { fmap = Result.map
    , join =
        \mm ->
            case mm of
                Result.Err e ->
                    Result.Err e

                Result.Ok m ->
                    m
    }


foo : Maybe Int
foo =
    Just 42
        |> bind maybe (\x -> Just (x + 1))
        |> bind maybe (\x -> Just (x + 1))
        |> bind maybe (\x -> Just (x + 1))


bar : List String
bar =
    [ 0, 1, 2, 4, 5 ]
        |> bind list (\x -> [ x + 10, x + 20 ])
        |> bind list (\x -> [ x + 100 ])
        |> bind list (\x -> [ toString x ])


baz : Result String Int
baz =
    Result.Ok " 42 "
        |> bind result String.toInt
        |> bind result (\x -> Result.Ok (x + 1))
        |> bind result (\x -> Result.Ok (x + 1))
        |> bind result (\x -> Result.Ok (x + 1))


qux : Result String Int
qux =
    Result.Ok " 42 "
        |> bind result (String.trim >> Result.Ok)
        |> bind result String.toInt
        |> bind result (\x -> Result.Ok (x + 1))
        |> bind result (\x -> Result.Ok (x + 1))
        |> bind result (\x -> Result.Ok (x + 1))


main =
    div []
        [ text <| "-- Maybe: " ++ toString foo
        , br [] []
        , text <| "-- Lists: " ++ toString bar
        , br [] []
        , text <| "-- Result: " ++ toString baz
        , br [] []
        , text <| "-- Result: " ++ toString qux
        ]
{% endhighlight %}

### Code output:

{% highlight text %}
-- Maybe: Just 45
-- Lists: ["110","120","111","121","112","122","114","124","115","125"]
-- Result: Err "could not convert string ' 42 ' to an Int"
-- Result: Ok 45
{% endhighlight %}


### References:

* Continuational. Lots of code, few words (Joakim Ahnfelt-Rønne blog):
  - [Monads (forget about bind)][ahnfelt]
* elm-data: Experimental implementation of generic operations for elm.
  - [README.md @ GitHub][elmdata]
* elm-lang: Documentation
  - [Extensible records][elmlang]

[codingpirates]: https://codingpirates.dk/omcodingpirates/
[ahnfelt]: http://www.ahnfelt.net/monads-forget-about-bind/
[elmdata]: https://github.com/sjorn3/elm-data/blob/master/README.md
[elmlang]: http://elm-lang.org/docs/records
