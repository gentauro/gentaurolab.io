---
layout: post
title: First impressions on CRM2013
categories:
  - English
tags: 
  - crm2013 
  - ui 
  - commandbar 
  - business-process 
  - mobile 
  - server-sync 
  - mergetables 
  - business-rules 
  - sdk 
  - autosave 
  - real-time-workflows 
  - actions 
  - jwt 
  - ripple-effect
time: "14:03"
---

I recently participated at *Microsoft's Dynamics CRM Training Blitz Day*, on the
*Technical Overview for Application consultants, Presales consultants and
Developers Track*. My first impression was ...

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013_and-they-are-gone.jpg" alt="All">

... and it's actually the way it should be. As we have it now, there a lot of
*code monkeys* that really don't understand the complexity of the systems they
are developing, and I know cos I've been that monkey on several occasions. But
it's not always the monkeys fault, what you have to understand is that what
people have been working on for several years, a developer has to learn and
master in very short period in order to implement in code, normally the
pre-analysis/design phase of a project. On other occasions it's the experts in
the subject that are not able to communicate what they want to the developers in
an understandable language, like for example plain *English*.

Back in the days while I was studying at the Computer Science Department at
Copenhagen University, myself and two fellow students, [Joakim
Ahnfelt-Rønne](http://www.linkedin.com/in/ahnfelt) and [Jørgen Thorlund
Haahr](http://www.linkedin.com/pub/j%C3%B8rgen-haahr/1a/281/62a), wrote a
bachelor thesis on the subject: [Klient/server-applikation til kvalitetskontrol
af tandbehandlinger](http://goo.gl/aDTIM). We tried to create a generic
application that would allow *subject-matter experts* to define some business
processes from an administration interface without the use of any kind of
*code*. This processes would be defined with a set of rules that would be
enforced whenever his/hers peers/colleagues would execute the defined process on
both the client side but as well on the server side. It's been about 5 years
since we made that initial prototype, a very limited piece of software but
theoretical correct and usable in real-life, to what we have nowadays in form of
CRM2013 and I can't avoid to get a little smile on my face thinking that at
least one of the big software companies are doing things the right way, or at
least they are trying.

The agenda for the Training Blitz Day looked promising with buzzwords as: *New
UI*, *Process Agility*, *Mobile Client*, *Yammer Integration*, *Exchange Sync*,
*Business Rules*, *Client Extensibility* ...

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-00_agenda.png" alt="All">

... and it didn't disappointed me, even though it was 4 hours with very short
breaks combined with my previous 8 hours at work.

They started by given a simple introduction to the new UI. The left bar, which
took about 20% of the screen is now placed on the top. This new bar is always
visible and you can access all the different sections of the CRM system at any
time.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-01_ui.png" alt="All">

Another major change in the Forms are that they now are a combination of several
entities thanks to the Business Process flows. Given the many devices that now
are able to connect to CRM, the Form visibility will adjust automatically based
on the size of the screen. Also mention that Forms are now a single page, where
the previous *iFrames* are replaced with *div html* tags that are loaded
asynchronously.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-02_ui.png" alt="All">

Finally, as Microsoft already pointed out, the very heavy loading Ribbons are
gone for good. They are replaced with the command bar, which is enabled for
touch screens, which is always visible.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-03_ui.png" alt="All">

As mentioned in the beginning of this blog post, the *Business Process Flows* are
pushed to a whole new level, where you are just not locked to a single process
but while you are working on an opportunity for example, you can choose to run
the cross-sale process if you already know the customer instead of having to go
through the standard lead-to-opportunity process.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-04_processes.png" alt="All">

The processes are easily defined with the well known interface, there are some
minor changes.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-05_processes.png" alt="All">

The best part is that these Business Processes will work for all your
interfaces: CRM web interface, Mobile applications, Outlook, Custom apps

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-06_processes.png" alt="All">

One of the awaited moments was the presentation of the Mobile Apps, only
available for Microsoft and Apple phones and tablets at the moment. The
application are build on HTML5 but with a native wrapper in order get access to
the specific hardware features. By using HTML5 it's easier to provide new
functionality without having to deploy new applications to the different App
Stores.

Note: There will be no offline client, what Microsoft tries to push is the
*always-online* and when you aren't, you will have cached previously downloaded
data. As with the Xbox One, they would might have to rethink that one again, or
at least we will need developers to do that part.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-07_mobile.png" alt="All">

The XML used to save the visual representations for the Forms and Dashboard,
will be reused on the Mobile Apps. This will make it easier to re-use already
implemented functionality. There are introduced some limits in order to provide
a fluent user experience.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-08_mobile.png" alt="All">

There is also made a separate phone app that integrates with contacts in order
to make phone-calls directly from the CRM app.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-09_mobile.png" alt="All">

Another feature of the new CRM is that the old heavy-in-memory Outlook client

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-10_outlook.png" alt="All">

will be replaced by several processes that will operate separately avoiding the OS memory limit

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-11_outlook.png" alt="All">

Another awaited feature is the Server-side synchronization. No more E-mail
routers and no more Outlook client must be running on the users PC in order to
send a couple of messages.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-12_sync.png" alt="All">

From now on, these tasks will now be done between the Exchange and CRM servers.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-13_sync.png" alt="All">

A flow of the update of an item from a phone, is done without Outlook even been
used.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-14_sync.png" alt="All">

So how difficult is it going to be to upgrade from CRM2011 to CRM2013? Well it's
going to be really easy, if your solution complies with the SDK. On the CRM
online version, Microsoft will take care of everything. On-Premises there are to
options:

* Best-practice: Just take a backup of the current tenant and then import it
  into your new CRM2013 setup.

* Alternatively: Upgrade the current CRM2011 with CRM2013 and choose to update
  all current tenants or wait to do it later from the Deploy Manager.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-15_upgrade.png" alt="All">

One of the performance improvements of the upgrade is that tables will be merged
into a single table, it was separated into two tables because of previous SQL
limitations for tables.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-16_upgrade.png" alt="All">

In order to implement *Business Process Flows* there will be almost no need to
use code made by developers, that is usually difficult to maintain across
different developers. Instead there will be used a *declarative/visualized
language/interface* 

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-17_businesslogic.png" alt="All">

Once a rule is defined, it will work everywhere.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-18_businesslogic.png" alt="All">

The *subject-matter experts* will be able to define the processes from a very
simple but powerful interface.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-19_businesslogic.png" alt="All">

The process will be deployed with the solution packages and they now also
support export/import of labels for use with several languages.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-20_businesslogic.png" alt="All">

Only headache will be once again the unsupported solutions created by some
*"Partners"*, even though Microsoft keeps saying time and time again, don't do
it.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-21_unsupported.png" alt="All">

What the final customer has to understand is that by allowing this to happen, it
gets more difficult to upgrade smoothly and even install the latest roll-updates
containing not only new functionality but also fixes to know bugs. Finally the
amount of time/money used to correct these problems, that shouldn't be there in
the first place, are the final customer going to pay for and not the
*"Partner"*. Just think about that for a moment ...

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-22_unsupported.png" alt="All">

Due to the new UI, the Client SDK is expanded to support the new functionality.

Remark: Some CRM2011 functionality will be *@deprecated* in the new
release. You can see the differences between the two SDK visualizations here:
[Xrm.Page Object Model](http://msdn.microsoft.com/en-us/library/gg328474.aspx)

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-23_clientsdk.png" alt="All">

Another *must-have* for any single business critical application out there is an
built-in autosave functionality.

Remark: Plug-ins will trigger every time auto-saved is called, so please re-think
the logic on how plug-ins are implemented. The best-practices recommended by
Microsoft are always to limit the fields an update plug-in should be triggered
on.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-24_autosave.png" alt="All">

Sitemaps and customizations to that will still be done through the XML as we
are used to.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-25_ui.png" alt="All">

Another AWESOME feature in combination with the *Business Process flows* are
that now workflows can be run synchronously as plug-ins (both pre- and
post). This will also limit the amount of code needed to implement plug-ins.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-26_realtime_workflows.png" alt="All">

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-27_realtime_workflows.png" alt="All">

In order to support real-life scenarios where a set of events needs to be
triggered and combine state between these events will now also be possible to be
done without writing code. I wished they used a bit more time on this matter as
it will be game changer compared to other CRM providers.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-28_actions.png" alt="All">

Also CRM2013 will provide OData access to Mobile/Custom Apps that uses the OData
interface through the Windows Azure Active Authentication library. 

There will also be support for phone/SMS mechanism to login for very high
secured organizations.

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-29_odata.png" alt="All">

As an CRM Architect is very easy to understand why so many, and more and more
organizations are choosing the CRM Framework (xRM), as the backbone of their
systems. Just look at the next picture. By adding a simple entity to your
solution, you will automatically get all this. Just by doing a few mouse clicks
and adding some text. Impressive right?

<img src="/assets/img/posts/2013-08-01-first-impressios-on-crm2013-30_rippleeffect.png" alt="All">

Finally I would like to say that I'm very exited and gratefully surprised with
the outcome of the new CRM2013 release. I'm really looking forward to work with
it and as I'm writing this blog post, we just received and e-mail from Microsoft
noticing us that Delegate A/S CRM Online solution will be upgraded in December
this year, without having to do anything from our side. The cloud is no longer
the future, but the present.