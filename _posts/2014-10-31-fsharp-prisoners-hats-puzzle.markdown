--- 
layout: post
title: F# - Prisoners and hats puzzle
categories:
  - English
tags:
  - snippet
  - f#
time: "18:02"
---

<img
src="/assets/img/posts/2014-10-31-fsharp-prisoners-hats-puzzle_prisoners-and-hats.png"
alt="All">

### Code Snippet:

{% highlight ocaml linenos %}
module Helpers = 
  let permutations ls = 
    let rec insertions x = function
      | []             -> [[x]]
      | (y :: ys) as l -> (x::l)::(List.map (fun x -> y::x) (insertions x ys))
    let rec permutations' = function
      | []      -> seq [ [] ]
      | x :: xs -> Seq.concat (Seq.map (insertions x) (permutations' xs))
    ls |> permutations'
    
module PrisonersHatsPuzzle =
  type Hat = Red | Blue
  type Prisoner = Hat
  type Prisoners = Prisoner * Prisoner * Prisoner * Prisoner
  type Guess = Prisoners -> Prisoner * Hat

  // Bullet-proof logic for the prisoners to always go free:
  // 
  // As the fourth prisoner is behind a screen, we don't care about him.
  // If the last prisoner sees two equal colored hats, he knows he has the
  // the opposite and hereby he can call out the color.
  // In case the last prisoner doesn't call out his color, the second from
  // behind will know that he doesn't have the same color hat as the prisoner
  // in front of him, else the last one would have called it, hereby, he
  // can safely call out the opposite color of the prisoners hat in 
  // front of him.
  // 
  let guess : Guess =
    fun prisoners ->
      let a,b,c,d = prisoners
      match a,b,c,d with
        | (_,Red,Red,_)   -> a,Blue
        | (_,Blue,Blue,_) -> a,Red
        | (_,_,Blue,_)    -> b,Red
        | (_,_,Red,_)     -> b,Blue

open Helpers
open PrisonersHatsPuzzle

let hats = [Hat.Red; Hat.Blue; Hat.Red; Hat.Blue;]

let alwaysGoFree =
  permutations hats
  |> Seq.map(fun xs ->
    match xs with
      | a::b::c::d::[] -> a,b,c,d
      | _ -> failwith "never")
  |> Seq.map(fun x -> guess x)
  |> Seq.toArray
{% endhighlight %}

### Code output:

{% highlight text %}
module Helpers = begin
  val permutations : ls:'a list -> seq<'a list>
end
module PrisonersHatsPuzzle = begin
  type Hat =
    | Red
    | Blue
  type Prisoner = Hat
  type Prisoners = Prisoner * Prisoner * Prisoner * Prisoner
  type Guess = Prisoners -> Prisoner * Hat
  val guess : Prisoner * Prisoner * Prisoner * Prisoner -> Prisoner * Hat
end

val hats : Hat list = [Red; Blue; Red; Blue]

val alwaysGoFree : (Prisoner * Hat) [] =
  [|(Blue, Blue); (Blue, Blue); (Blue, Blue); (Red, Red); (Red, Red);
    (Red, Red); (Blue, Blue); (Red, Red); (Red, Red); (Red, Red); (Blue, Blue);
    (Red, Red); (Red, Red); (Red, Red); (Blue, Blue); (Blue, Blue); (Red, Red);
    (Red, Red); (Blue, Blue); (Blue, Blue); (Blue, Blue); (Blue, Blue);
    (Blue, Blue); (Red, Red)|]
{% endhighlight %}

### References:

* Wikipedia: [Prisoners and hats puzzle](http://en.wikipedia.org/wiki/Prisoners_and_hats_puzzle)