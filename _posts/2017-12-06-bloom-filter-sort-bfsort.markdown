--- 
layout: post
title: Bloom Filter Sort (bfsort)
categories:
  - English
tags:
  -bfsort
  -bloomfilter
  -bloom
  -filter
  -sort
  -probability
time: "00:01"
---

<div align="center">
<div>Ramón Soto Mathiesen and Johan Thillemann</div>
<div>4th MirageOS hack retreat, Marrakesh</div>
<div><code>rsm λ spisemisu . com</code> and <code>joe λ celo . io</code></div>
</div>

<div align="center">
<br />
<br />
<b>Abstract</b>
<br />
<br />
</div>

Let _A_ be an array of unique numbers of size _n_, where the minimum and maximum
numbers in the array are _min_ and _max_ respectively.

In this blog post we introduce a parallel algorithm, **bfsort**, based on
**Bloom Filters**, that can sort the array _A_ in _O(log₂ n)_ time and _O(m)_
space where _m_ is the size of: _{ min, min + 1, min + 2, ... , max - 1, max }_,
while the sequential version, can sort the array _A_ in _O(m)_ time and _O(n)_
space. We argue that if merge operations of empty arrays that could be stored in
CPU-cache, given unlimited cores, then the memory overhead wouldn't have that
big of an impact on the parallel version of the algorithm.

As the algorithm is based on **Bloom Filters**, both the parallel and sequential
versions sort the array _A_ with a collision probability of _0.00001%_. The
algorithm is parameterized on the collision probability, which can be adjusted
for optimal results based on the size of the array.

### 1. Introduction

In this section, we will describe a few data structures and concepts that will
ease the understanding of the following sections:

* **Sorting** a sequence of values could be defined with the following
constraint: _{ a₀ ≤ a₁ ≤ ... ≤ aₙ }_ where a number is less or equal to it's
successor in the sequence. When working with sorting algorithms, it's important
to understand the underlying data structure that stores the values. **Arrays**
allow additions in _linear time_, as a new array _+1_ needs to be allocated on
another spot in memory, while indexed access are in _constant time_. **Lists**
allow _constant time_ additions and _linear time_ lookups.

* **Bloom Filter** is a space-efficient data structure as it saves the necessary
information in **bits**, see Figure 0, which differs from **HashTables** which
will need at least a **boolean**, normally implemented as a **byte**, where only
one bit is set and the rest are unused, to store the information. With regard to
test if an element is in the set, the **Bloom Filter** can only warranty that an
element _definitely is not in set_ while if an element is in the filter, it
still has to make a lookup on the underlying data structure. There is a
_probability_ that an element is in the filter but it isn't in the underlying
data structure, _false positives_. The _false positives_ are due to the _set of
hashes_ can set the same bit for different words. This is why it's very
important to choose the most space-efficient size of bits, _m_, in relation to
the probability of false positives, _p_, while choosing the optimal number of
hash functions, _k_, that uniformly distribute the values across the filter
[cao]:

<div align="center">
<figure>
<img
src="/assets/img/posts/2017-12-06-bloom-filter-sort-bfsort_500px-Bloom_filter.png"
/>
<figcaption>
<code>
  Figure 0: Example of a Bloom filter by [eppstein], 15 August 2007.
</code>
</figcaption>
</figure>
</div>

* **Asymptotic performance measures**:

  * **DAG**: A directed acyclic graph, _G = (V,E)_, in this blog post represents
    the computations of an algorithm. The vertices, _V_, represent the
    instructions to be computed and the edges, _E_, represent the dependencies
    between the instructions. If there is a edge, _(u,v) ∈ E_, the instructions
    in the vertex _v_ cannot be executed before the instructions in the vertex
    _u_.
	
  * **Work**: The total time of computing all instructions of the algorithm
    executed on a computer with a single processor, see Figure 1. Work can be
    mapped to the DAG as all the vertices, _V_, in the graph. The _work law_
    states that if _k_ work can be done in _Tₖ(n)_ time on a computer with _k
    processors_ and the amount of work is only _T(n)_, where _k · Tₖ(n) ≥ T(n)_,
    then the execution time of the algorithm with _k processors_, is limited to
    the amount of work divided by the amount of processors.
	
  * **Span**: The execution time of the longest path in a DAG, also know as
    _critical path_, see Figure 1. The asymptotic time complexity of the
    algorithm is dominated by the _critical path_. The _span law_ is defined as
    follows: no parallel computer with a limited amount of _k processors_ can
    execute a parallel algorithm faster than a computer with an unlimited amount
    of cores. The computer with unlimited amount of processors can always just
    execute the parallel algorithm with _k processors_: _Tₖ(n) ≥ T∞(n)_.
  
<div align="center">
<figure>
<img
src="/assets/img/posts/2017-12-06-bloom-filter-sort-bfsort_methodology_work_span.png"
width="85%" height="85%" />
<figcaption>
<code>
  Figure 1: Representation of sequential vs parallel computations as DAGs.
</code>
</figcaption>
</figure>
</div>
  
### 2. Sorting with Bloom Filters

As mentioned in the previous section when describing the **Bloom Filters**, in
order to uniformly distribute the values across the filter, we must find the
optimal relation between bits, probability of false positives and number of hash
functions.

<div align="center">
<figure>
{% highlight text %}
[|136; 165; 6; 149; 68; 30; 2; 187; 106; 1; 219; 90; 119; 14; 72; 124; 222; 25;
  179; 190; 237; 118; 177; 162; 146; 199; 195; 182; 0; 234|]
{% endhighlight %}
<figcaption>
<code>Figure 2: Random array of 31 unique bytes.</code>
</figcaption>
</figure>
</div>

For this algorithm, the primary concern is the probability of false positives
and not so much the space efficiency or the number of hash functions. For this
reason, we choose a very low probability as default value, _0.0000001_, even
though it means that the amount of bits and hash functions will be slightly
bigger in size than normal. Even though it's a very low probability number, the
chosen number ensures that the size of the underlying array in the **Bloom
Filters** doesn't becomes larger than the length of the array, which could be
translated in term of _asymptotic complexity_ as the algorithm would use _n_ extra
space, where _n_ is the length of the array.

<div align="center">
<figure>
{% highlight text %}
(31u, 1e-07, 23,
 [|2632944486u; 176026093u; 2853052920u; 534382327u; 2173353107u; 840630065u;
   4181145030u; 878918235u; 33641635u; 1504134878u; 136075251u; 3384303498u;
   2194677699u; 768245312u; 3208224496u; 2410081556u; 219847018u; 2055198962u;
   3112189164u; 4023218364u; 1356830802u; 3285656209u; 449204162u|])
{% endhighlight %}
<figcaption>
<code>Figure 3: Info parameters { number of bits: 31 * 32, probability of collision:
0.00001%, number of hashes: 23 and list of hashes: ... }.</code>
</figcaption>
</figure>
</div>

Once we have decided the parameters for the **Bloom Filters**, we can then
instantiate it and be ready to populate it with the values of the array.

<div align="center">
<figure>
{% highlight text %}
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
[|"□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"; "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□"|]
{% endhighlight %}
<figcaption>
<code>Figure 4: Empty instantiated Bloom Filter.</code>
</figcaption>
</figure>
</div>

While we populate the **Bloom Filter** with the values in _linear time_. It's
worth noticing that even though we calculate several hashes and perform several
updates on the **Bloom Filter**, those operations are still in _constant
time_. We can also find the _minimum_ and _maximum_ values in the array, also in
_linear time_, more specifically in the same traversal of the array as the one
that performs the updates in the **Bloom Filters**.

<div align="center">
<figure>
{% highlight text %}
[|"■□□■■□□□□□■□■■■□□□■■□■□□□□□□■□□□"; "■■□□□□□□□□□■■□■■□■■□□■□■□■□■■■□□"|]
[|"■□□□□□□□□□■□■□■■□□□□■■■□□■■■□□■□"; "■□■■■■□□■■□■□■■■■□□■□■■□□■■□■□■□"|]
[|"□□■■□□■■■■■■■□■□□■□■■■□■■□■□■■□■"; "□■■□■□□□■□■□□□□□■■□□■□□■■■□□□□□□"|]
[|"■□■□■□■■□□■■■□□□■□■□■□■■■□□□■■□■"; "□■□□□□□■■□■□■■□■□□■■■□■□□□■■■■■■"|]
[|"□□□■■□□□□□□■□□■□■□■□■□■■□□■■□■□□"; "□□□□□■□■□■□■□■□■□□■■■■■□■□■■■□■■"|]
[|"■□■□□□■□□■□■■□■■□□□□■■■□■■■□■□■□"; "□■■■□■□■□■□□□□□■■□□□□■■□□■■□□□□■"|]
[|"□□□□■□□□□□□□□□□□■□□□□■□■■■□□■■□■"; "■□□■□□■■■□□■■□□■□□□■■□□■□■■■■□■□"|]
[|"□■□□■■□■■■■■■□■■■■□□□■□■□■□■■□■□"; "■■■□□□■□■■□■■□■■■■□□■■■□□■□□■□□■"|]
[|"■□□■■■■□■■■□■□■■□■■■■□□■■□■□■■■□"; "■□■□■■□■□■■■□□■■□■□□■■□■■■■□□□□□"|]
[|"■■■■□□□■□■■■□□■■■■□■■■■■■■□□■■□□"; "□■□■■■■■□□■□■■■□■■□□□■■□□□□■■□■■"|]
[|"□■□■□□□□■□□□■■□■□■□□■■■□■□■□■■■□"; "■□□□□□■■□■■■□□□■□■□□■□■■□■■□■□□■"|]
[|"□□■■■□□□□■□□■□■□■□■■□□■□□□■□■□■□"; "□□□□□□■□□□□■□■□■□□■■■□■□□■□□□■□□"|]
[|"□■□■■□□□■■□■■■■□□□■□■□□□□■□□□■■□"; "■□□■□□□□■■■■□■□□■□■■□■□□□■■■□□□□"|]
[|"□■□■■□□■■■■■■■□■■■■■■■□□■■□■□□□□"; "■■■■■□■□■□■□■■■□□■□■□■□■■□□■□□■□"|]
[|"□□□■■■□■□■■□■□■■□□■□□□□□■■■■□■■■"; "□■□■■□□□■□□□□□□□■□■□■■□■□□■■■□□□"|]
[|"■□■■■□■■■■□■□□□□■■■□□□■■□□□□■■□■"; "□■□■■■□□■□□□■■■□□■■□■■□■■□■□□■■■"|]
{% endhighlight %}
<figcaption>
<code>
  Figure 5: Bloom Filter bits populated with the unique bytes from the array.
</code>
</figcaption>
</figure>
</div>

After the bits of the **Bloom Filter** are populated with the values of the
array, we can now begin to execute the part of the algorithm that will produce a
sorted array.

As we have found the _minimum_ and the _maximum_ values in the array, we can now
recursively iterate through all the numbers in the range from the _minimum_
number to the _maximum_ number. 

The technique we use is to exclude numbers which we _definitely know are not in
the set_. And because we have chose a very little probability of false
positives, we can easily argue that numbers that aren't excluded, most likely
are in the initial array.

<div align="center">
<figure>
{% highlight text %}
[|0; 1; 2; 6; 14; 25; 30; 68; 72; 90; 106; 118; 119; 124; 136; 146; 149; 162;
  165; 177; 179; 182; 187; 190; 195; 199; 219; 222; 234; 237|]
{% endhighlight %}
<figcaption>
<code>Figure 6: Array is sorted after algorithm execution.</code>
</figcaption>
</figure>
</div>

Therefore we can state that the **work** is in _Θ(m)_, where _m_ is the length
of the interval from the _minimum_ value to the _maximum_ value. As this is a
sequential algorithm, the **span** will also match the **work** and therefore be
in _O(m)_.

It's worth noticing that we only need _n_ extra space, where _n_ is the length
of the array to be sorted.

For an example of the algorithm applied to a list of **bytes**, please see
Figures 2 - 6.

### 3. Parallelizing the algorithm

Readers familiar with **Bloom Filters** will recognize that there are certain
operations in the algorithm described in the previous section, that can be
parallelized.

<div align="center">
<figure>
{% highlight ocaml linenos %}
let rec aux f lower high = parallel {
  if lower < high then
    let    mid  = lower + high >>> 1
    let!   fork = spawn <| aux f lower mid
    let!   main = aux f (mid+1) high
    let!   join = fork
    return Util.merge join main 
  else
    return if f lower then [lower] else []
}

{% endhighlight %}
<figcaption>
<code>Figure 7: Pseudocode of a fork/join implementation in F#, an ML-family programming language.</code>
</figcaption>
</figure>
</div>

In this section we will write them down and show how we can lower the **span**,
critical path, if an a computer with an unlimited amount of cores is provided
and a **fork/join** approach is used, see pseudocode from Figure 7, which will reduce the
global asymptotic time complexity of the algorithm:

#### 3.1 Initialization

In the sequential algorithm, this step is done in _linear time_ by iterating
over each of the elements of the array.

* **Allocate underlying Bloom Filter array**: Allocating the array in memory can
    be done in _constant time_ while populating it with initial _zero values_
    can be done in _logarithmic time_, _O(h)_, where _h = log₂ n_, when using a
    similar parallel algorithm as described in the pseudocode from Figure 7 in
    this section.
	
* **Seeds and Hashes**: As the size of the bits of the underlying **Bloom
  Filter** array _much greater-than_ the size values of the seeds and the
  hashes, therefore none of these operations will have an impact on the time
  complexity. It's worth noticing that the random generation of seeds as well as
  the instantiation of the hashes based on the seeds can be done in parallel as
  well.

The new **span** for this step is reduced from _O(n)_ to _O(h)_ where _h = log₂
n_ and _n_ is the length of the array.

#### 3.2 Populating

In the sequential algorithm, this step is done in _linear time_ by iterating
over each of the elements of the array.

* **Updating the Bloom Filter with the hash values**: As described in the
sequential algorithm, even though we calculate several hash functions for each
value, these operations don't change the asymptotic time complexity of this step
as the operations are in _constant time_. Therefore, as we are able to
parallelize the update of the **Bloom Filter** with an approach as described in
pseudocode from Figure 7 in this section. The new **span** for this step is
reduced from _O(n)_ to _O(h)_ where _h = log₂ n_ and _n_ is the length of the
array. It's worth noticing that **Bloom Filter** are built on top of the **or**
operation, which is the most suitable operation to be parallelized. As we don't
work with **bytes** we need to ensure that in order to ensure no **race
conditions**, an architecture with support for concurrent atomic CPU operations
[atomic], must be chosen.
 
* **Minimum and maximum**: As above, we can also easily calculate these values
by storing locally the value of the comparison between two numbers and bobble
them all the way to the root of the recursive tree. The new **span** for this
step is reduced from _O(n)_ to _O(h)_ where _h = log₂ n_ is the length of the
array. If the architecture supports concurrent atomic CPU operations, then the
extra space needed can be reduced to _O(1)_.

As both steps are reduced from _O(n)_ to _O(h)_ where _h = log₂ n_ is the length
of the array, we can conclude that the new **span** for this step is also
reduced from _O(n)_ to _O(h)_.

#### 3.3 Sorting

In the sequential algorithm, the **span**, _O(m)_, is bound to the **work**
which is in _Θ(m)_, where _m_ is the length of the interval from the _minimum_
value to the _maximum_ value.

* **Querying the Bloom Filter with the hash values** We can easily parallelize
  the queries of the values contained in interval from the _minimum_ value to
  the _maximum_ value with a **fork/join** approach, reducing the **span** from
  _O(n)_ to _O(h')_ where _h' = log₂ m_. In order to make this value more
  understandable, we will showcase the following example with **32-bit unsigned
  integers**. Lets assume that our list contains some **32-bit uints** where the
  minimum value equals _0_ and the maximum value equal _4.294.967.295_ (_2³² -
  1_). Then our **span** would be _O(32)_ as _log₂ 4.294.967.295 = 32_.

* **Merging** Once we know if a value is _definitely not in set_ then we can
  return an empty array for that case while for the other case we will return an
  array of size one containing that element. The merge process will consist of
  combining two arrays into one and copying the elements, in parallel into the
  newly created array, with the same approach of **fork/join** as mentioned in
  this section. The initialization and merge of the top array will be bounded to
  _n_, where _n_ is the size of the array to sort, therefore reducing the
  **span** of this process _O(lg n)_ as we can create a new array in _constant
  time_ and populate it in _O(h)_ where _h = log₂ n_.
  
As the second step dominates the first, we can argue that we have reduced from
_O(m)_ to _O(h)_ where _h = log₂ n_ is the length of the array, we can conclude
that the new **span** for this step is also reduced from _O(n)_ to _O(h)_.

Sadly, we will have an overhead in memory usage as we are storing _empty arrays_,
for at most _m_ values. But, since the merge of _empty arrays_, which are
minimalistic in size, could be stored on the unlimited cores CPU-cache, then the
memory overhead wouldn't have that big of an impact on the algorithm.

#### 3.4 Summary

By combining the time complexities from the different steps:

_Tₖ(n) = O(h) + O(h) + O(h) = 3 · O(h) = O(h)_

where _h = log₂ n_ and _n_ is the length of the array. Therefore, we can
conclude that the **work** is _Θ(m)_ where _m_ is the length of the interval
from the _minimum_ value to the _maximum_ value in the array to be sorted and
the **span** is in _logarithmic time_.

As mentioned in the previous section, there will be an overhead in memory usage
as we are storing _empty arrays_ for at most _m_ values. If the merge operations
of _empty arrays_, which are minimalistic in size, could be stored on each of
the unlimited cores CPU-cache, then the memory overhead wouldn't have that big
of an impact on the algorithm.

### 4. Implementation

The algorithms are implemented in **F#**, which could easily be described as the
**OCaml** implementation for **.NET**, as we already had a working native
version of the **Bloom Filter** data structure. Here are a few implementation
details that are worth noticing:

The hash algorithm chosen is **MurmurHash3** [smhasher] as it has support for a
seed which can be used to generate different hashes for the same key by re-using
the same algorithm. In order to get a better seeds and hereby ensuring a more
uniform distribution of values across the filter, see Figure 8 for a graphical
representation, the **RNGCryptoServiceProvider** is used for random number
generation instead of **System.Random**. See **Hash** and **Random** modules for
more information.

<div align="center">
<figure>
<img
src="/assets/img/posts/2017-12-06-bloom-filter-sort-bfsort_bloomfilter_populated.png"
/>
<figcaption>
<code>
  Figure 8: Graphical representation of values uniformly distributed across the filter.
</code>
</figcaption>
</figure>
</div>

Usage of **Interlocked.CompareExchange** [interlocked] which allows concurrent
_atomic CPU operations_ to perform updates on data values with minimal overhead,
see Figure 9. In order to use these operations with the space-efficient data
structure, we will need to instantiate our underlying array with **ref 'a**
values. In case the reader is not convinced that the minimum and maximum numbers
can be found in parallel with these operations, it can easily be refactored to a
binary tree of height _h = log₂ n_ by using a (**fork/join**) approach, where
the value of the comparisons are stored locally. It's easier to see that the
span, critical path, is also _O(lg n)_ time but with a bit more space overhead
_O(n)_ as described in the previous section. Please look into the **Atomic** and
**BloomFilter** modules for more information.

<div align="center">
<figure>
<img
src="/assets/img/posts/2017-12-06-bloom-filter-sort-bfsort_methodology_pram.png"
width="55%" height="55%" />
<figcaption>
<code>
  Figure 9: Several processors reading from the same shared memory but only one can
write atomically in a clock cycle.
</code>
</figcaption>
</figure>
</div>

In the parallel versions of the algorithm, by using true parallelism
**Array.Parallel** (system threads) compared to using concurrency with
**Async.StartChild** (lightweight threads) in a **fork/join** approach, shows
how there is an extra memory overhead in the first as the operations require to
allocate all the values in the range from minimum to maximum in an array in
order to filter out values that are definitely not in set, while in the second
approach there is less memory overhead as we can use the same approach as
described in the previous paragraph when finding in parallel the minimum and
maximum numbers in an array to merge lists by using **lazy list** that allows
concatenation in constant time _O(1)_. See the **BloomFilter** and **List.Lazy**
modules for more information.

A full implementation can be seen in **Appendix A: Source Code**.

### 5. Concluding remarks

We have presented both a sequential as well as a parallel algorithm that uses
the **Bloom Filter** data structure as an underlying component in order to sort
an array with a very small collision probability of _0.00001%_, which can be
incremented and decremented for optimal results based on the size of the array.

We argue that the sequential algorithms _asymptotic complexity_ is in _Θ(m)_
time and space as the **work** is in _Θ(m)_, where _m_ is the length of the
interval from the _minimum_ value to the _maximum_ value. As this is a
sequential algorithm, the **span** will also match the **work**. Also it's worth
noticing that we only need _n_ extra space, where _n_ is the length of the array
to be sorted.

And we argue that the parallel algorithms _asymptotic complexity_ is in
_logarithmic time_ time as the dominating operation is bounded to the **span**
_O(lg n)_ where _h = log₂ n_ is the length of the array. We also argue that the
**work** is is _Θ(m)_ where _m_ is the length of the interval from the _minimum_
value to the _maximum_ value in the array and therefore we will need _m_ extra
space.

#### 5.1 Notes:

* In order to ease the understanding of the **span** for the parallel algorithm,
  the following example was used: Lets assume that our list contains some
  **32-bit uints** where the minimum value equals _0_ and the maximum value
  equal _4.294.967.295_ (_2³² - 1_). Then our **span** would be _O(32)_ as _log₂
  4.294.967.295 = 32_.

* In order to limit the amount of **work**, it would be ideal to use this
  sorting algorithm with data that is to not scattered, as the amount of
  **work** is defined by the interval from minimum and maximum values of the
  array.

* As this is a probabilistic sorting algorithm, you will have a small chance
  that the given array is not sorted after executing the algorithm. Therefore
  this algorithm shouldn't be used _critical operations_.
  
* It can easily be shown if an array is sorted in _O(lg n)_, where _n_ is the
  size of the array, by using the **fork/join** parallel approach mentioned in
  section 3. Parallelizing the algorithm, where you just need to ensure that the
  head of the merged leaf array is lower or equal to the head of the right array
  and bubble that boolean value up to the root of the tree. In case the array is
  not sorted, the sorting algorithm could be executed again but by providing an
  even lower probability which would result in a bit more of memory overhead and
  **work**.

### References

<table border = "0" width = "100%" style = "border-collapse:separate; border-spacing:1em;">
<tr>
  <td width = "10%" valign = "top"><code>[bloom]</code></td>
  <td align = "left" >
    Bloom, Burton H. (1970), “Space/Time Trade-offs in Hash Coding with Allowable Errors”, Communications of the ACM, 13 (7): 422–426, doi:10.1145/362686.362692
  </td> 
</tr>
<tr>
  <td width = "10%" valign = "top"><code>[eppstein]</code></td>
  <td align = "left" >
    David_Eppstein/Gallery<br />
    <a href="https://commons.wikimedia.org/wiki/User:David_Eppstein/Gallery">https://commons.wikimedia.org/wiki/User:David_Eppstein/Gallery</a><br />
    Accessed: 2017-12-03
  </td> 
</tr>
<tr>
  <td width = "10%" valign = "top"><code>[cao]</code></td>
  <td align = "left" >
    Bloom Filters - the math<br />
    <a href="http://pages.cs.wisc.edu/~cao/papers/summary-cache/node8.html">http://pages.cs.wisc.edu/~cao/papers/summary-cache/node8.html</a><br />
    Accessed: 2017-12-03
  </td> 
</tr>
<tr>
  <td width = "10%" valign = "top"><code>[smhasher]</code></td>
  <td align = "left" >
    SMHasher is a test suite designed to test the distribution, collision, and performance properties of non-cryptographic hash functions.<br />
    <a href="https://github.com/aappleby/smhasher">https://github.com/aappleby/smhasher</a><br />
    Accessed: 2017-12-03
</td> 
</tr>
<tr>
  <td width = "10%" valign = "top"><code>[atomic]</code></td>
  <td align = "left" >
     Atomic Operations.<br />
    <a href="https://software.intel.com/en-us/node/506090">https://software.intel.com/en-us/node/506090</a><br />
    Accessed: 2017-12-03
  </td> 
</tr>
<tr>
  <td width = "10%" valign = "top"><code>[interlocked]</code></td>
  <td align = "left" >
    Interlocked Operations.<br />
    <a href="https://docs.microsoft.com/en-us/dotnet/standard/threading/interlocked-operations">https://docs.microsoft.com/en-us/dotnet/standard/threading/interlocked-operations</a><br />
    Accessed: 2017-12-03
  </td> 
</tr>
</table>

### Bibtex

If you want to cite this blog post, you can use the following `BibTeX`
information:

{% highlight text %}
@online{bfsort,
  author = {Ramón Soto Mathiesen and Johan Thillemann},
  title = {Bloom Filter Sort (bfsort)},
  year = 2017,
  url = {https://blog.stermon.org/articles/2017/12/06/bloom-filter-sort-bfsort},
  urldate = {2017-12-06}
}
{% endhighlight %}

### Appendix A: Source Code

{% highlight ocaml linenos %}
#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

(*
   bfsort: Bloom Filter Sort
*)

let inline (>>=) m f = Option.bind f m

[<RequireQualifiedAccess>]
module Assert =
  
  let sorted : int array -> bool =
    fun xs ->
      let rec aux acc = function
        | [] | [  _  ] ->  acc
        | x :: [  y  ] -> (x <= y && acc)
        | x ::  y :: xs -> aux (x <= y && acc) (y :: xs)
      xs |> Array.toList |> aux true

[<RequireQualifiedAccess>]
module Atomic =
  
  open System.Threading
  
  (*
     Interlocked Class:
  
     https://msdn.microsoft.com/en-us/library/
             system.threading.interlocked(v=vs.110).aspx
  *)
  let rec private aux : (int -> int -> int) -> int ref -> int -> int =
    fun f a b ->
      let x = !a
      let y = f x b
      match x = Interlocked.CompareExchange(a,y,x) with
        | true  -> y
        | false -> aux f a b
  
  let or' : int ref -> int -> int =
    fun x y -> aux ( ||| ) x y
  
  let min : int ref -> int -> int =
    fun x y -> aux min x y
  
  let max : int ref -> int -> int =
    fun x y -> aux max x y

[<RequireQualifiedAccess>]
module Binary =
  
  open System
  
  let mask = function | true -> '■' | false -> '□'
  
  let fromInt : int -> int -> (bool -> char) option -> string option =
    fun bits n mask ->
      let con = function | '1'  -> true | _____ -> false
      let def = function | true -> '1'  | false -> '0'
      let bin =
        Convert.ToString(n,2).PadLeft(bits,'0')
        |> Seq.map (con >> (defaultArg mask def) >> string)
        |> Seq.fold ( + ) ""
        
      match bin |> String.length = bits with
        | true  -> Some bin
        | false -> None

[<RequireQualifiedAccess>]
module Bytes =
  
  [<RequireQualifiedAccess>]
  module BigEndian =
    
    open System
    
    let private littleEndian = BitConverter.IsLittleEndian
    
    let toUint32 : byte array -> uint32 =
      fun bs ->
        let bs' = if littleEndian then bs |> Array.rev else bs
        (bs',0) |> BitConverter.ToUInt32

[<RequireQualifiedAccess>]
module Random =
  
  open System
  
  let private r = Random ()
  
  let byte (   ) = r.Next(0 , 1 <<< 8)
  let next lb ub = r.Next(lb, ub     )
  
  [<RequireQualifiedAccess>]
  module Crypto =
    
    open System.Security.Cryptography
    
    let next () =
      
      use rng = new RNGCryptoServiceProvider ()
      let bs  = Array.Parallel.init 32 (fun _ -> 0uy)
      
      rng.GetNonZeroBytes      bs
      Bytes.BigEndian.toUint32 bs

[<RequireQualifiedAccess>]
module Hash =
  
  let private string2bytes : string -> byte array =
    System.Text.Encoding.UTF8.GetBytes
    
  (*
     FVN Hash:
  
     http://isthe.com/chongo/tech/comp/fnv/
  *)
  let fnv1aHash : string -> uint32 =
    fun key ->
      (* Example: ./fnv1a32 -s foo => 0xa9f37ed7 *)
      let fnvp  = (1 <<< 24) + (1 <<< 8) + 0x93 |> uint32
      let fnvob = 2166136261u
  
      let b = key |> string2bytes
  
      let rec aux h = function
        | i when i < (b.Length) ->
          let h'  =
            h ^^^ (b.[i] |> uint32)
            |> fun x -> x * fnvp
          aux h' (i+1)
        | _ -> h
      aux fnvob 0
  
  (*
     Murmur Hash:
  
     https://sites.google.com/site/murmurhash/
  *)
  let murmur3Hash : uint32 -> string -> uint32 =
    fun seed key ->
      (* Example: ./murmur3 foo => x86_32: b12f489e (seed 42) *)
      let rotl x r = (x <<< r) ||| (x >>> (32 - r))
      let fmix h =
        h
        |> fun x -> x ^^^ (x >>> 16)
        |> fun x -> x * 0x85ebca6bu
        |> fun x -> x ^^^ (x >>> 13)
        |> fun x -> x * 0xc2b2ae35u
        |> fun x -> x ^^^ (x >>> 16)
      let getblock b i =
        System.BitConverter.ToUInt32(value = b, startIndex = i)
  
      let data    = key  |> string2bytes
      let len     = data |> Array.length
      let nblocks = len >>> 2 (* equivalent to len / 4 but faster *)
      let h1      = seed
      let c1      = 0xcc9e2d51u
      let c2      = 0x1b873593u
  
      (* body *)
      let rec body h = function
        | i when i < (nblocks) ->
          let k1 =
            getblock data (i * 4)
            |> fun x -> x * c1
            |> fun x -> rotl x 15
            |> fun x -> x * c2
          let h' =
            h ^^^ k1
            |> fun x -> rotl x 13
            |> fun x -> x * 5u + 0xe6546b64u
          body h' (i+1)
        | _ -> h
      let h1' = body h1 0
  
      (* tail *)
      let tail = nblocks * 4
      let rec tail' (k,h) = function
        | 0 -> h
        | 1 ->
          let k' =
            k ^^^ (uint32 data.[tail])
            |> fun x -> x * c1
            |> fun x -> rotl x 15
            |> fun x -> x * c2
          let h' = h ^^^ k'
          tail' (k',h') (0)
        | i ->
          let k' =
            (uint32 data.[tail + (i - 1)]) <<< (1 <<< (i + 1))
            |> fun x -> k ^^^ x
          tail' (k',h) (i-1)
      let h1'' = tail' (0u,h1') (len &&& 3)
  
      (* finalization *)
      h1'' ^^^ (uint32 len)
      |> fun x -> x |> fmix

[<RequireQualifiedAccess>]
module Logarithm =
  
  (*
     ISO 31-11:
  
     https://en.wikipedia.org/wiki/
             ISO_31-11#Exponential_and_logarithmic_functions
  *)
  let ln : double -> double = log
  let lb : double -> double = fun x -> System.Math.Log   (x,2.)
  let lg : double -> double = fun x -> System.Math.Log10 (x   )

[<RequireQualifiedAccess>]
module List =
  
  [<RequireQualifiedAccess>]
  module Lazy =
    
    type 'a lazylist = Nil | Cons of 'a * (unit -> 'a lazylist)
    
    let rec merge : 'a lazylist * 'a lazylist -> 'a lazylist = function
      | xs, Nil    | Nil, xs     -> xs
      | Cons (x,xs), Cons (y,ys) ->
        let x' = min x y
        let y' = max x y
        Cons (x', fun () -> merge (xs (), Cons (y', fun () -> ys())))
    
    let rec toList : 'a lazylist -> 'a list = function
      | Nil          -> []
      | Cons (x, xs) -> x :: (toList (xs()))
    
    let toArray : 'a lazylist -> 'a array = fun xs -> toList xs |> List.toArray

[<RequireQualifiedAccess>]
module BloomFilter =
  
  type bloomFilter       = private Config of config
  and private config     =
    { parameters : parameters
      bits       : int ref            array
      seeds      : uint32             array
      hashes     : (string -> uint32) array
    }
  and private parameters =
    { n : uint32
      p : double
      k : int
      m : uint32
    }
  
  let init : uint32 -> double -> uint32 array option -> bloomFilter option =
    fun n p seeds ->
      let m =
        (* ceil up to nearest 2^n in order to use &&& (n-1) instead of % n *)
        -(float n * Logarithm.ln p) / (Logarithm.ln 2. ** 2.)
        (* round to nearest 32 bits *)
        |> uint32 >>> 5 <<< 5
      let k =
        (*
           Optimal number of hashes:
        
           http://pages.cs.wisc.edu/~cao/papers/summary-cache/node8.html
        *)
        (float m / float n) * Logarithm.ln 2.
        |> (ceil >> int)
      let bs = Array.Parallel.init (m >>> 5 |> int) (fun _ -> ref 0)
      
      match seeds with
        | Some ss ->
          if ss |> Array.length = k then
            Config
              { parameters =
                  { n = n
                    p = p
                    k = k
                    m = m
                  }
                bits   = bs
                seeds  = ss
                hashes = ss |> Array.Parallel.map Hash.murmur3Hash
              }
            |> Some
          else
            None
        | None ->
          let ss = Array.Parallel.init k (fun _ -> Random.Crypto.next ())
          Config
            { parameters =
                { n = n
                  p = p
                  k = k
                  m = m
                }
              bits   = bs
              seeds  = ss
              hashes = ss |> Array.Parallel.map Hash.murmur3Hash
            }
          |> Some
  
  let info : bloomFilter -> (uint32 * double * int * uint32 array) option =
    fun (Config { parameters = { n = n; p = p; k = k }; seeds = ss; })->
      (n,p,k,ss) |> Some
  
  let private bytebit hash =
    let byte = hash >>> 5
    let bit = hash - (byte <<< 5)
    byte,bit
  
  let private sbit : int -> int        = fun n   -> 1 <<< (31-n)
  let private gbit : int -> int -> int =
    fun i32 n ->
      System.Convert.ToString(i32,2).PadLeft(32,'0').[n] |> string |> int
  
  let add : string -> bloomFilter -> unit option =
    fun key (Config { parameters = { m = m; }; hashes = hs; bits = bs; }) ->
      hs
      |> Array.Parallel.map (fun f     -> f key % m |> (int >> bytebit))
      |> Array.Parallel.iter(fun (x,y) -> Atomic.or' bs.[x] (sbit y) |> ignore)
      |> Some
  
  let query : string -> bloomFilter -> bool option =
    fun key (Config { parameters = { m = m; }; hashes = hs; bits = bs; }) ->
      hs
      |> Array.Parallel.map (fun f -> f key % m |> (int >> bytebit))
      |> Array.fold(fun a (x,y) -> (gbit !bs.[x] y = 1) && a) true
      |> Some
  
  let print : bloomFilter -> string array option =
    fun (Config { bits = bs; }) ->
      let masked = fun n -> n |> Binary.fromInt 32 <| Some Binary.mask
      bs
      |> Array.Parallel.map (fun n -> masked !n)
      |> Array.Parallel.choose id
      |> Some
  
  let length : bloomFilter -> int option =
    fun (Config { bits = bs; }) ->
      bs
      |> Array.length
      |> Some
  
  let sort : int array -> double option -> int array =
    fun xs probability ->
      let size    = xs |> Array.length
      let minimum = System.Int32.MaxValue |> ref
      let maximum = System.Int32.MinValue |> ref
      
      (*
         Probability of collisions:
   
         0.1       -> 10%
         0.01      -> 1%
         0.001     -> 0.1%
         0.0001    -> 0.01%
         0.00001   -> 0.001%
         0.000001  -> 0.0001%
         0.0000001 -> 0.00001% (still with 2*n space)
      *)
      let collision = defaultArg probability 1.e-7
      
      (*
         Create & Update Bloom Filter, Min & Max values in parallel
      *)
      let bf = init (uint32 size) collision None
      xs
      |> Array.Parallel.iter (
        fun k ->
          Atomic.min minimum k  |> ignore
          Atomic.max maximum k  |> ignore
          bf >>= add (string k) |> ignore
      )
      
      (*
         Create a new array with sorted values in sequential.
         
         Note: Extra space is bound to size of array
      *)
      let rec aux : int list -> int -> int array =
        fun acc n ->
          let acc' =
            if bf >>= query (string n) = Some true then
              n :: acc
            else
              acc
          match n with
            | i when i = !minimum -> List.toArray acc'
            | i                   -> aux          acc' (i-1)
      aux [] !maximum
  
  [<RequireQualifiedAccess>]
  module Parallel =
    
    let sort : int array -> double option -> int array =
      fun xs probability ->
        let size    = xs                    |> Array.length
        let minimum = System.Int32.MaxValue |> ref
        let maximum = System.Int32.MinValue |> ref
        
        (*
           Probability of collisions:
     
           0.1       -> 10%
           0.01      -> 1%
           0.001     -> 0.1%
           0.0001    -> 0.01%
           0.00001   -> 0.001%
           0.000001  -> 0.0001%
           0.0000001 -> 0.00001% (still with 2*n space)
        *)
        let collision = defaultArg probability 1.e-7
      
        (*
           Create & Update Bloom Filter, Min & Max values in parallel
        *)
        let bf = init (uint32 size) collision None
        xs
        |> Array.Parallel.iter (
          fun k ->
            Atomic.min minimum k  |> ignore
            Atomic.max maximum k  |> ignore
            bf >>= add (string k) |> ignore
        )
        
        (*
           Create a new array with sorted values in parallel.
           
           Note: Parallel version would require more than O(N) of extra space
        *)
        Array.Parallel.init (!maximum - !minimum + 1) (
          fun i ->
            let n = !minimum + i
            if bf >>= query (string n) = Some true then
              Some n
            else
              None
        )
        |> Array.Parallel.choose id
  
  [<RequireQualifiedAccess>]
  module Async =
    
    [<RequireQualifiedAccess>]
    module Parallel =
      
      let merge : 'a array -> 'a array -> 'a array =
        fun xs ys ->
          let n,m = Array.length xs, Array.length ys
          Array.Parallel.init (n+m) (
            fun i -> if i < n then xs.[i] else ys.[i-n]
          )
      
      let sort : int array -> double option -> int array =
        fun xs probability ->
          let size    = xs                    |> Array.length
          let minimum = System.Int32.MaxValue |> ref
          let maximum = System.Int32.MinValue |> ref
          
          (*
             Probability of collisions:
       
             0.1       -> 10%
             0.01      -> 1%
             0.001     -> 0.1%
             0.0001    -> 0.01%
             0.00001   -> 0.001%
             0.000001  -> 0.0001%
             0.0000001 -> 0.00001% (still with 2*n space)
          *)
          let collision = defaultArg probability 1.e-7
          
          (*
             Create & Update Bloom Filter, Min & Max values in parallel
          *)
          let bf = init (uint32 size) collision None
          xs
          |> Array.Parallel.iter (
            fun k ->
              Atomic.min minimum k  |> ignore
              Atomic.max maximum k  |> ignore
              bf >>= add (string k) |> ignore
          )
          
          (* This implementation is slower than when using lazy lists
          
          let rec aux low high =
            async {
              if low < high then
                let    mid  =  low + high >>> 1
                let!   fork =  Async.StartChild (aux low mid)
                let!   main =  aux (mid+1) high
                let!   join =  fork
                return merge join main
              else
                let condition = 
                  if bf >>= query (string low) = Some true then
                    [| low |]
                  else
                    [|     |]
                return condition
            }
          
          aux !minimum !maximum
          |> Async.RunSynchronously
          
          *)
          
          let rec aux low high =
            async {
              if low < high then
                let    mid  =  low + high >>> 1
                let!   fork =  Async.StartChild (aux low mid)
                let!   main =  aux (mid+1) high
                let!   join =  fork
                return List.Lazy.merge (join, main)
              else
                let condition = 
                  if bf >>= query (string low) = Some true then
                    List.Lazy.Cons (low, fun () -> List.Lazy.Nil)
                  else
                    List.Lazy.Nil
                return condition
            }
          
          aux !minimum !maximum
          |> Async.RunSynchronously
          |> List.Lazy.toArray

(* Explanation with the finite byte Set: {0} ∪ ℕ<0xFF *)
let explanation () =
  
  let pprint : 'a array -> unit =
    fun xs ->
      xs |> Array.chunkBySize 2 |> Array.iter (printfn "%A")
  
  (* Generate unique random values in a specific range *)
  let upper = System.Byte.MaxValue |> int
  let lower = System.Byte.MinValue |> int
  let size  = (upper - lower) >>> 3
  let randoms =
    Array.Parallel.init size (fun _ -> Random.next lower upper)
    |> Array.distinct
  
  printfn "Random array of unique bytes:"
  randoms |> printfn "%A"
  
  (* Init with with constraints *)
  let bf = BloomFilter.init (uint32 size) 0.0000001 <| None
  
  (* Info *)
  bf >>= BloomFilter.info  >>= (printfn "Info:\n%A" >> Some) |> ignore
  
  (* Before *)
  printfn "Before:"
  bf >>= BloomFilter.print >>= (pprint >> Some) |> ignore
  
  (* Populate BF and narrow the Set by setting minimum and maximum values *)
  let min'     = System.Byte.MaxValue |> int |> ref
  let max'     = System.Byte.MinValue |> int |> ref
  let populate =
    randoms
    |> Array.Parallel.iter(
      fun b ->
        Atomic.min min' b                 |> ignore
        Atomic.max max' b                 |> ignore
        bf >>= BloomFilter.add (string b) |> ignore
    )
  
  (* After *)
  printfn "After:"
  bf >>= BloomFilter.print >>= (pprint >> Some) |> ignore
  
  (* Sort with a collision probability of 0.0000001 *)
  let xs' =
    let rec aux lower high =
      async {
        if lower < high then
          let    mid  = lower + high >>> 1
          let!   fork = Async.StartChild (aux lower mid)
          let!   main = aux (mid+1) high
          let!   join = fork
          return List.Lazy.merge (join, main)
        else
          let condition = 
            if bf >>= BloomFilter.query (string lower) = Some true then
              List.Lazy.Cons (lower, fun () -> List.Lazy.Nil)
            else
              List.Lazy.Nil
          return condition
      }
    
    aux !min' !max'
    |> Async.RunSynchronously
    |> List.Lazy.toArray
  
  printfn "Array sorted: %b" <| Assert.sorted xs'
  xs' |> printfn "%A"

(* Example of usage of the sorting algorithm *)
let examples () =
  (* Generate unique random values in a specific range *)
  let upper   = 355_000
  let lower   = 115_050
  let size    = (upper - lower) >>> 1
  let randoms =
    Array.Parallel.init size (fun _ -> Random.next lower upper)
    |> Array.distinct
  
  let xs = randoms |> BloomFilter               .sort <| None
  let ys = randoms |> BloomFilter      .Parallel.sort <| Some 1.e-7
  let zs = randoms |> BloomFilter.Async.Parallel.sort <| Some 1.e-7
  
  printfn "Assert arrays are sorted: %b"
    (Assert.sorted xs && Assert.sorted ys && Assert.sorted zs)

explanation ()
{% endhighlight %}
