---
layout: post
title: Outlook Interop and F#
categories:
  - English
tags: 
  - outlook 
  - interop 
  - f#
time: "14:03"
---

How to print your inbox, including subfolders and mail items

{% highlight ocaml %}
open System.Runtime.InteropServices
open Microsoft.Office.Interop.Outlook
 
let printTreeStruture l =
    l |> List.map(fun n -> 
        printf "%O" (String.replicate n " ")
        printf "%O" "|") |> ignore
    printf "%O" "- "
    ()
 
let printItems l (mi:MailItem) =
    printTreeStruture l
    printfn "%O" (mi.Subject)
 
let printFolder l (mf:MAPIFolder) =
    printTreeStruture l
    printfn "%O" (mf.Name.ToUpper())
 
let items l (f:Items) =
    for i in f do
        match i with
        | :? MailItem as mi -> printItems l mi
        | _ -> ()
 
let rec folders l (f:Folders) =
    for mf in f do
        printFolder l mf
        items (l @ [1]) mf.Items
        match mf.Folders with
        | :? MAPIFolder -> ()
        | _ -> folders (l @ [1]) mf.Folders
 
[<EntryPoint>]
let main argv = 
    let o = new Microsoft.Office.Interop.Outlook.ApplicationClass()
    let mapi = o.GetNamespace("MAPI")
    let mv = System.Reflection.Missing.Value
    let f = mapi.GetDefaultFolder(OlDefaultFolders.olFolderInbox)
    let exp = f.GetExplorer(false) 
 
    mapi.Logon(mv,mv,mv,mv)
 
    printFolder [] f
 
    folders [0] f.Folders
 
    try
        Marshal.ReleaseComObject(o) |> ignore 
    with
        | exn ->
            let innerMessage =
                match (exn.InnerException) with
                | null -> ""
                | innerExn -> innerExn.Message
            printfn "An exception occurred:\n %s\n %s" exn.Message innerMessage
    0
{% endhighlight %}

The only dependency is the Microsoft.Office.Interop.Outlook assembly

<img src="/assets/img/posts/2013-03-12-outlook-interop-and-fsharp1.png" alt="Assembly">

The output will be like:

{% highlight bash %}
-INBOX
|-FOLDER1_NAME
| |-SUBFOLDER1_NAME
| | |-Email1_Subject
| | |-SUBSUBFOLDER1_NAME
| | | |-Email1_Subject
| | | |-Email2_Subject
| | | |-…
{% endhighlight %}

If you have the issue that not all messages are retrieved, follow this kb
article: [Read more at Only a subset of your Exchange mailbox items are
synchronized in Outlook 2013](http://support.microsoft.com/kb/2733062)

But the trick is to set the *Mails to keep offline* slider to *All*:
<img src="/assets/img/posts/2013-03-12-outlook-interop-and-fsharp2.png" alt="All">

And remember to release the [COM](http://msdn.microsoft.com/en-us/library/system.runtime.interopservices.marshal.releasecomobject.aspx) object.