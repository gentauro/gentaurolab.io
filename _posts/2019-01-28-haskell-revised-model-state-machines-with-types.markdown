--- 
layout: post
title: Haskell - (revised) Model state machines with types
categories:
  - English
tags:
  - snippet
  - haskell
  - model
  - state
  - machine
  - type
time: "00:30"
---

### ASCII art

{% highlight text %}
                           +: State
                           #: Transition
                          
                           +--------------------------+
                           |            On            |
                           +--------------------------+
                                 ʌ             |
                                 |             v
                           #-----------#  #-----------#
                           | Off -> On |  | On -> Off |
                           #-----------#  #-----------#
                                 ʌ             |
                                 |             v
                           +--------------------------+
                           |            Off           |
                           +--------------------------+
{% endhighlight %}


### Code Snippets

#### FSM/Light.hs

{% highlight haskell linenos %}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Safe                  #-}

--------------------------------------------------------------------------------

module FSM.Light
  ( Off
  , On
  , Transition
  , off
  , on
  , switch
  )
where

--------------------------------------------------------------------------------

data On
data Off

class  State a                             where
class (State a, State b) => Transition a b where

instance State On  where
instance State Off where

instance Transition On  Off where
instance Transition Off On  where

--------------------------------------------------------------------------------

off     :: Off
on      :: On
switch  :: Transition a b => a -> b

--------------------------------------------------------------------------------

off     = undefined
on      = undefined
switch  = undefined
{% endhighlight %}


#### Main.hs

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           FSM.Light
  ( Off
  , On
  , off
  , switch
  )

--------------------------------------------------------------------------------

blinking :: Either Off On -> Either Off On
blinking (Left  x) = Right $ switch x
blinking (Right x) = Left  $ switch x

{- We can't implement `blinking` incorrectly:

> blinking (Left x) = Left $ switch x

• No instance for (FSM.Light.Transition Off Off)
    arising from a use of ‘switch’
-}

--------------------------------------------------------------------------------

main :: IO ()
main =
  do
    putStr "> Type the amount of times to turn on/off the ligth switch: "
    times <- getLine
    putStrLn . (++ " We turn on/off " ++ times ++ " time(s)") $ oi $ read times
      where
        si n = foldl (\a _ -> blinking a) (Left off) ([ 1 .. n ] :: [ Integer ])
        oi n =
          case si n of
            Left  _ -> "> OFF |"
            Right _ -> "> ON  |"
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../fsm$ ./Main.hs
> Type the amount of times to turn on/off the ligth switch: 0
> OFF | We turn on/off 0 time(s)
user@personal:~/../fsm$ ./Main.hs
> Type the amount of times to turn on/off the ligth switch: 10
> OFF | We turn on/off 10 time(s)
user@personal:~/../fsm$ ./Main.hs
> Type the amount of times to turn on/off the ligth switch: 11
> ON  | We turn on/off 11 time(s)
user@personal:~/../fsm$ 
{% endhighlight %}


### References:

* Blog: Ramón Soto Mathiesen:
  - [Model state machines with types][fsm]
* Haskell Wiki:
  - [Smart constructors][smart]
  
[fsm]:   /articles/2016/10/06/model-state-machine-with-types.html
[smart]: https://wiki.haskell.org/Smart_constructors#Enforcing_the_constraint_statically
