---
layout: post
title: ƒ(x)RM as Open Source
categories:
  -English
tags:
  -f#
  -fsharp
  -frm
  -opensource
time: "19:05"
---

### The MIT Open Source license with a twist, *Copyleft (ↄ)*

As of today and after my biannual 1:1 meeting with our CEO and Head of
Department, I can now announce that our *ƒ(x)RM (Functional Relationship
Management)* will be released under the following Open Source License:

{% highlight text %}
The MIT License (MIT)

Copyleft (ↄ) 2014 Ramón Soto Mathiesen, CTO of CRM @ Delegate A/S

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyleft notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYLEFT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  
{% endhighlight %}

This is really good news, stay tuned for more information on ƒ(x)RM :-)

<img src="/assets/img/posts/2014-03-27-frm-as-open-source_open_source.png"
alt="All">
