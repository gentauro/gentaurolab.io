--- 
layout: post
title: Elm - Basic Counter example with State from QueryString
categories:
  - English
tags:
  - snippet
  - elm
time: "04:22"
---
### Code Snippet: index.html

{% highlight html linenos %}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Main</title>
    <style>
      html,head,body { padding:0; margin:0; }
      body { font-family: calibri, helvetica, arial, sans-serif; }
    </style>
    <script src="elm.min.js" type="text/javascript">
    </script>
  </head>
  <body>
    <script type="text/javascript">
      Elm.fullscreen(
        Elm.Main,
        { queryString: window.location.search });
    </script>
  </body>
</html>
{% endhighlight %}

### Code Snippet: Main.elm

{% highlight haskell linenos %}
module Main (..) where

import Html exposing (text)
import ParseModel exposing (Model,parse,update, view)
import Html exposing (Html)
import StartApp.Simple exposing (start)

main : Signal Html.Html
main =
 start
 { model = parse queryString -- 0
 , update = update
 , view = view
 }

port queryString : String
{% endhighlight %}

### Code Snippet: src/ParseModel.elm

{% highlight haskell linenos %}
module ParseModel where

import List exposing (filter,head)
import Maybe exposing (withDefault)
import String exposing (dropLeft,split,startsWith,toInt)

import Html exposing (..)
import Html.Attributes exposing (href,style)
import Html.Events exposing (onClick)
import Http exposing (url)

-- MODEL
type alias Model = Int

-- UPDATE
type Action = Increment | Decrement
update : Action -> Model -> Model
update action model =
  case action of
    Increment ->
      model + 1
    Decrement ->
      model - 1

-- VIEW
view : Signal.Address Action -> Model -> Html
view address model =
  div []
    [ button [ onClick address Decrement ] [ text "-" ]
    , state model
    , button [ onClick address Increment ] [ text "+" ]
    ]

-- INITIAL STATE
query2model : String -> String
query2model s =
  withDefault "?foo=" (Just s)
    |> dropLeft 1 -- drop '?'
    |> split "&"
    |> filter (\term -> startsWith "foo=" term)
    |> head
    |> withDefault "foo=0"
    |> dropLeft 4 -- drop 'foo='
       
parse : String -> Int
parse s =
  case query2model s |> toInt of
    Err msg -> Debug.crash msg
    Ok  val -> val
  
-- SAVE STATE
linkToApp model =
  a [ "/assets/apps/elm/counterState/index.html?foo=" ++ (toString model) |> href ]
    [ toString model |> text ]

content link =
  div [ ]
    [ text ""
    , link
    ]

state model =
  div [countStyle] [ linkToApp model |> content ]

-- CSS
countStyle : Attribute
countStyle =
  style
    [ ("font-size", "20px")
    , ("font-family", "monospace")
    , ("display", "inline-block")
    , ("width", "50px")
    , ("text-align", "center")
    ]
{% endhighlight %}

### Build Snippet: build.bash
{% highlight bash %}
#!/bin/bash

# ensure latest packages are downloaded
elm-package install --yes

# only create the .js as we will rely on a custom html file (port)
elm-make Main.elm --output elm.js --yes

# make elm js as small as possible
./.misc/jsmin/jsmin < elm.js > elm.min.js
{% endhighlight %}

### Code output:

* Link: [Counter + State](/assets/apps/elm/counterState/index.html)

### References:

* The Elm Architecture:
  - [Example 1: A Counter](https://github.com/evancz/elm-architecture-tutorial/tree/master/examples/1)

