--- 
layout: post
title: Draft - Example
categories:
  - English
tags:
  - draft
  - example
time: "00:00"
---

### Template

This is just an example to see if it's possible to write blog posts as drafts,
which can't be accessed from any `menu`, so it will be possible to request
review on the post before they are published.
