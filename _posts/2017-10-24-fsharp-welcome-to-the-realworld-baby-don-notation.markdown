--- 
layout: post
title: F# - Welcome to the RealWorld, baby (+Don notation)
categories:
  - English
tags:
  - snippet
  - fsharp
  - monads
  - monadic
  - haskell
  - haskellish
  - don
  - notation
time: "19:22"
---

### Real World Code Snippet

{% highlight ocaml linenos %}
module RealWorld =
  
  type    World = private | World
  and  'a Pure  = private | Pure  of  'a
  and  'a IO    =           World -> ('a Pure * World)
  
  let bind  : 'a IO -> 'b IO -> 'b IO =
    fun a1 a2 world0 ->
      let (a,world1) = a1 world0 in
      let (b,world2) = a2 world1 in
      (b,world2)
  
  let lift : 'a IO -> ('a Pure -> 'b IO) -> 'b IO =
    fun a1 a2 world0 ->
      let (a,world1) = a1   world0 in
      let (b,world2) = a2 a world1 in
      (b,world2)
  
  let ( >> )  : 'a IO ->             'b IO  -> 'b IO = bind
  let ( >>= ) : 'a IO -> ('a Pure -> 'b IO) -> 'b IO = lift
  
  let unit : unit Pure = Pure ()
  
  let effect : ('a -> 'b) -> 'a Pure -> 'b IO =
    fun f (Pure a) ->
      fun world -> Pure (f a), world
  
  let eval : unit IO -> unit Pure * World =
    fun main -> main World
  
  [<AutoOpen>]
  module Don =
    
    [<Sealed>]
    type DonBuilder () =
      member __.Yield (()) : unit IO = fun world -> unit,world
      [<CustomOperation("bind")>]
      member __.Bind' (a1, a2) = bind a1 a2
      [<CustomOperation("lift")>]
      member __.LiftM (a1, a2) = lift a1 a2
    
    let don = new DonBuilder ()
{% endhighlight %}

### Real World Code output:

{% highlight text %}
module RealWorld = begin
  type World = private | World
  and 'a Pure = private | Pure of 'a
  and 'a IO = World -> 'a Pure * World
  val bind : a1:'a IO -> a2:'b IO -> world0:World -> 'b Pure * World
  val lift :
    a1:'a IO -> a2:('a Pure -> 'b IO) -> world0:World -> 'b Pure * World
  val ( >> ) : ('a IO -> 'b IO -> 'b IO)
  val ( >>= ) : ('a IO -> ('a Pure -> 'b IO) -> 'b IO)
  val unit : unit Pure = Pure ()
  val effect : f:('a -> 'b) -> 'a Pure -> 'b IO
  val eval : main:unit IO -> unit Pure * World
  module Don = begin
    type DonBuilder =
      class
        new : unit -> DonBuilder
        member Bind' : a1:'c IO * a2:'d IO -> 'd IO
        member LiftM : a1:'a IO * a2:('a Pure -> 'b IO) -> 'b IO
        member Yield : unit -> unit IO
      end
    val don : DonBuilder
  end
end
{% endhighlight %}

### Utils Code Snippet

{% highlight ocaml linenos %}
module Util =
  
  open RealWorld
  
  module Random =
    let private r  = new System.Random ()
    let next   ( ) =          r.Next   ()
  
  module Read =
    let readInt  () = System.Console.Read     ()
    let readKey  () = System.Console.ReadKey  ()
    let readLine () = System.Console.ReadLine ()
  
  let (!) : 'a -> 'a IO =
    fun a -> effect (fun _ -> a) unit
  
  let getRand : int IO =
    effect Random.next unit
  
  let putInt : int Pure -> unit IO =
    effect <| printf "%i"
  
  let readLn : string IO =
    effect Read.readLine unit
  
  let putStr : string Pure -> unit IO =
    effect <| printf "%s"
  
  let putStrLn : string Pure -> unit IO =
    effect <| printfn "%s"
  
  let sample1 : unit IO =
    ! "What is your name?"
    >>= putStrLn
    >>  readLn
    >>= fun a ->
      ! "How old are you?"
      >>= putStrLn
      >>  readLn
      >>= fun b ->
        putStr a
        >>  ! ","
        >>= putStr
        >>  putStrLn b
  
  let sample2 : unit IO = don {
    bind ! "What is your name?"
    lift putStrLn
    bind readLn
    
    lift (fun a -> don {
    bind ! "How old are you?"
    lift putStrLn
    bind readLn
    
    lift (fun b -> don {
    bind (putStr a)
    bind ! ","
    lift putStr
    bind (putStrLn b)
    
    })
    })
  }
{% endhighlight %}

### Utils Code output:

{% highlight text %}
module Util = begin
  module Random = begin
    val private r : System.Random
    val next : unit -> int
  end
  module Read = begin
    val readInt : unit -> int
    val readKey : unit -> System.ConsoleKeyInfo
    val readLine : unit -> string
  end
  val ( ! ) : a:'a -> 'a RealWorld.IO
  val getRand : int RealWorld.IO
  val putInt : (int RealWorld.Pure -> unit RealWorld.IO)
  val readLn : string RealWorld.IO
  val putStr : (string RealWorld.Pure -> unit RealWorld.IO)
  val putStrLn : (string RealWorld.Pure -> unit RealWorld.IO)
  val sample1 : unit RealWorld.IO
  val sample2 : unit RealWorld.IO
end
{% endhighlight %}

### Execution Code Snippet

{% highlight ocaml %}
let _ = Util.sample1 |> RealWorld.eval, Util.sample2 |> RealWorld.eval
{% endhighlight %}

{% highlight text %}
mon@razerRamon:~/tmp/realWorld$ ./RealWorld.fsx
{% endhighlight %}

### Execution Code output:

{% highlight text %}
What is your name?
John Doe
How old are you?
42
John Doe,42
What is your name?
John Doe
How old are you?
42
John Doe,42
{% endhighlight %}


### References:

* The Haskell Programming Language, **IO inside**:
  - [Welcome to the RealWorld, baby][realworld]
* Ramón Soto Mathiesen blog:
  * [OCaml - Welcome to the RealWorld, baby][ramonblog]


[realworld]: https://wiki.haskell.org/IO_inside#Welcome_to_the_RealWorld.2C_baby
[ramonblog]: /articles/2017/10/24/ocaml-welcome-to-the-realworld-baby
