--- 
layout: post
title: Idris - Evoting, vote count matching votes
categories:
  - English
tags:
  - snippet
  - idris
  - evoting
time: "14:51"
---


### Idris Code Snippet

{% highlight haskell linenos %}
module Evoting

import Data.Vect
import Data.List

%default total

Name : Type
Name = String

Surname : Type
Surname = String

data Candidate = Person Name Surname
data Vote      = Blank | For Candidate
data Validity  = Valid Vote | Invalid

Eq Candidate where 
  (Person name1 surname1) == (Person name2 surname2) =
    name1 == name2 && surname1 == surname2
  (Person name1 surname1) /= (Person name2 surname2) =
    name1 /= name2 || surname1 /= surname2

validity : List Candidate -> Vote -> Validity
validity (Nil    ) __________ = Invalid
validity _________ (Blank   ) = Valid Blank
validity (x :: xs) (For vote) = 
  if x == vote then Valid (For vote) else validity xs (For vote)

invalidate : Vect n Vote -> Vect n Validity
invalidate (Nil    ) = Nil
invalidate (_ :: xs) = Invalid :: invalidate xs

election : List Candidate -> Vect n Vote -> Vect n Validity
election __________ (Nil          ) = Nil
election Nil        (votes        ) = invalidate votes
election candidates (vote :: votes) =
  validity candidates vote :: election candidates votes
                          
candidates : List Candidate
candidates =
  Person "John" "Doe" ::
  Person "Jane" "Doe" :: 
  []

{- Version 1: Replicate real life behaviour -}
votes : Vect 3 Vote {- We know the number of citizens -}
votes = 
  For (Person "Jane" "Doe") :: 
  For (Person "John" "Hoe") :: {- Invalid candidate -}
  Blank ::
  [] 
{% endhighlight %}

### Idris Code output:

{% highlight text %}
Welcome to the Idris REPL!
Idris 1.0

Type checking ./evoting.idr
λΠ> election candidates votes
[Valid (For (Person "Jane" "Doe")), Invalid, Valid Blank] : Vect 3 Validity
λΠ>
{% endhighlight %}

### References:

* Idris | A Language with Dependent Types:
  * [Web](https://www.idris-lang.org/)
  * [Tutorial](http://docs.idris-lang.org/en/latest/tutorial/index.html#tutorial-index)
