--- 
layout: post
title: Haskell - Usage of malloc/free is SAFE
categories:
  - English
tags:
  - snippet
  - haskell
  - malloc
  - free
  - safe
time: "11:36"
---

### Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.Word
  ( Word8
  )
import           Foreign.Marshal.Alloc
  ( free
  , mallocBytes
  )
import           Foreign.Ptr
  ( Ptr
  )
import           Foreign.Storable
  ( peekByteOff
  , pokeByteOff
  )

--------------------------------------------------------------------------------

readBytes :: Ptr Word8 -> Int -> IO ()
readBytes p i =
  mapM_ (\n -> word8 n >>= putStrLn . show) [ 0 .. (i - 1) ]
  where
    word8 = peekByteOff p :: Int -> IO Word8

saveBytes :: Ptr Word8 -> Int -> Word8 -> IO ()
saveBytes p i b =
  mapM_ (\n -> pokeByteOff p n b) [ 0 .. (i - 1) ]

main :: IO ()
main =
  do
    putStrLn "# We allocate bytes: "
    putStrLn $ show n
    p <- mallocBytes n
    putStrLn "# Pointer to bytes: "
    putStrLn $ show p
    putStrLn "# Initial bytes: "
    readBytes p n
    putStrLn "# Updated bytes: "
    saveBytes p n b
    readBytes p n
    putStrLn "# We free bytes (pointer): "
    putStrLn $ show p
    free p
    where
      n = 016
      b = 127 :: Word8
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../malloc/lib$ ./Main.hs
# We allocate bytes: 
16
# Pointer to bytes: 
0x00007c5c74000b30
# Initial bytes: 
0
11
0
116
92
124
0
0
0
0
0
0
0
0
0
0
# Updated bytes: 
127
127
127
127
127
127
127
127
127
127
127
127
127
127
127
127
# We free bytes (pointer): 
0x00007c5c74000b30
{% endhighlight %}


### References:

* Haskell Hackage (base):
  - [Foreign.Marshal.Alloc][alloc]
  - [Foreign.Ptr][ptr]
  - [Foreign.Storable][storable]
  
[alloc]:    https://hackage.haskell.org/package/base/docs/Foreign-Marshal-Alloc.html
[ptr]:      https://hackage.haskell.org/package/base/docs/Foreign-Ptr.html
[storable]: https://hackage.haskell.org/package/base/docs/Foreign-Storable.html
