---
layout: post
title: Restricted File Executer, a usefull alternative to RunAs.exe
categories:
  - English
tags:
  - rfe
  - opensource
  - runas.exe
  - windows
time: "21:42"
---

### What the application does

First of all, what is Restricted File Executer?

Restricted File Executer is an C# application that tries to fill out a small
problem in Microsoft Windows NT based Operation Systems. That small problem is
that you cannot make an administrative executable files (.exe, .msi or .bat)
that a user, without administrator rights, can run, unless you also give him the
password to the installation account (RunAs.exe).

In a domain based network you have the GPO, where you can place .msi files (and
only this type of file). Or you can install through a .vbs (visual basic
script). Sometimes this is just not enough. For example, not all the people need
to update to the newest Real Player, but our Press Department does, by the way,
Real Player doesn't release .msi installation packages. Or you can't just say to
a user, I placed a new script in our script folder, please restart your pc so
the the script can run at computer level.

Like it's not enough when you have remote users that doesn't have connection to
your domain, and even if the had, a 50 Mb online installation would take a
while. Whith RFE (as I will refer to this application in future) you can make a
set of packages, burn it on a CD and send it to the remote user.

Another advantage is that because of the encrypted data container (.rdc) you can
easily put all your licensed software on your public server, without worrying
about other people having access to them (hackers, competitors, ...). Unless
they have the same AES encryption key as you and an identical user account with
the same username and password, the file will be useless to them.

### Who will it be usefull for

Besides from the company where I work right now? Well I supouse to all other
System Administrators / IT Departments that run into the same problem as I did.

<img src="/assets/img/posts/2006-08-18-restricted-file-executer_rfe_overview.png" alt="Restricted File Executer (RFE)">

### System Requirements 

* Microsoft Windows 2000 / XP / 2003 Operating System: I supouse this would be
obvious.

* Microsoft .NET Framework Version 2.0: The application is developed in C#,
therefore you need to install the Microsoft .NET Framework.

### Download

[Link](/assets/download/2006-08-18-restricted-file-executer_setup_rfe.7z)
