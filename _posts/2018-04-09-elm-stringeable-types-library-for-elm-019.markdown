--- 
layout: post
title: Elm - Stringeable types library for Elm 0.19
categories:
  - English
tags:
  - snippet
  - elm
  - stringeable
  - showable
  - typeclass
time: "18:30"
---

### Code Snippet

{% highlight haskell linenos %}
import Html exposing (br, div, text)


type alias Stringable a =
    { stringable : a -> String }



-- Basics.toString going to Debug.toString in Elm 0.19 right?


toString : Stringable a -> a -> String
toString { stringable } x =
    stringable x



-- Primitive Types (The basic types built-into Elm: Int, Char, Float, ...):


int : Stringable Int
int =
    {- Obviously this will be changed with Int.toString in 0.19 -}
    { stringable = \x -> Basics.toString x }


char : Stringable Char
char =
    {- Obviously this will be changed with Char.toString in 0.19 -}
    { stringable = \x -> Basics.toString x }


float : Stringable Float
float =
    {- Obviously this will be changed with Float.toString in 0.19 -}
    { stringable = \x -> Basics.toString x }



-- Composite Types: (List, Array, ... but also custom made like Tree)


list : Stringable a -> Stringable (List a)
list nestedStringable =
    { stringable =
        \xs ->
            let
                str =
                    xs
                        |> List.map (toString nestedStringable)
                        |> List.reverse
                        |> List.foldl (\a b -> a ++ ", " ++ b) ""
                        |> String.dropRight 2
            in
            "[ " ++ str ++ " ]"
    }



-- Custom types built with primitive (core Elm) types


type alias FooBar =
    { foo : Int
    , bar : Char
    }


type BazQux
    = BazQux Int


foobar : Stringable FooBar
foobar =
    { stringable =
        \{ foo, bar } ->
            "FooBar { "
                ++ "foo: "
                ++ toString int foo
                ++ ", bar: "
                ++ toString char bar
                ++ " }"
    }


bazqux : Stringable BazQux
bazqux =
    { stringable = \(BazQux x) -> "BazQux " ++ toString int x }



-- Usage:


example00 =
    let
        str =
            [ 42.0, 43.0, 44.0 ]
                |> toString (list float)
    in
    "-- List of Floats: " ++ str


example01 =
    let
        str =
            toString foobar (FooBar 42 'c')
    in
    "-- Custom (product) type: " ++ str


example02 =
    let
        str =
            toString bazqux (BazQux 42)
    in
    "-- Custom (sum) type: " ++ str


main =
    div []
        [ text example00
        , br [] []
        , text example01
        , br [] []
        , text example02
        , br [] []
        ]
{% endhighlight %}

### Code output:

{% highlight text %}
-- List of Floats: [ 42, 43, 44 ]
-- Custom (product) type: FooBar { foo: 42, bar: 'c' }
-- Custom (sum) type: BazQux 42
{% endhighlight %}


### References:

* `Basic.toString` (Elm 0.18):
  - [`toString : a -> String`][elmcore]
* _"In 0.19; `toString` is moved to `Debug.` and only available in dev-mode"_:
  - [zwilias @ Twitter][twitter]

[elmcore]: http://package.elm-lang.org/packages/elm-lang/core/5.1.1/Basics#toString
[twitter]: https://twitter.com/zwilias/status/917373319879458816
