--- 
layout: post
title: Haskell - Granulating Effects
categories:
  - English
tags:
  - snippet
  - haskell
  - granulating
  - effects
time: "21:27"
---


### Files

{% highlight text %}
mon@razerRamon:~/tmp/haskell/granulate-effects$ ll
total 12K
-rwxr-xr-x 1 mon mon 3.2K Nov 24 20:57 GranulateEffects.hs*
mon@razerRamon:~/tmp/haskell/granulate-effects$
{% endhighlight %}

### Haskell Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-8.21
   --install-ghc
   runghc
   --package http-client
   --package http-client-tls
   --
   -Wall -Werror
-}

module GranulateEffects (main) where

import qualified Data.ByteString.Lazy.Char8 as L8

{- https://haskell-lang.org/library/http-client -}
import Network.HTTP.Client
{- https://hackage.haskell.org/package/http-client-tls -}
import Network.HTTP.Client.TLS (tlsManagerSettings)

{- As Kris Jenkins (@krisajenkins) says: "IO is the SUDO of side-effects" -}
getHtml :: String -> IO String
getHtml url = do
  {- To get secure HTML we will need: TLS manager, request and response -}
  manager  <- newManager   tlsManagerSettings
  request  <- parseRequest url
  response <- httpLbs request manager
  let html =  L8.unpack $ responseBody response

  {- Because we aren't limiting effects, we can also log to a file -}
  writeFile "./logging.txt" html
  
  return html

{- Lets granulate our effects to what we really want -}
class Monad m => 
  MmanagerTLS m where
  newManager' :: ManagerSettings -> m Manager

instance MmanagerTLS IO where
  newManager' = newManager

class Monad m => 
  Mrequest m where
  parseRequest' :: String -> m Request

instance Mrequest IO where
  parseRequest' = parseRequest

class Monad m => 
  Mresponse m where
  httpLbs' :: Request -> Manager -> m (Response L8.ByteString)

instance Mresponse IO where
  httpLbs' = httpLbs

{- We update the signature and add our granulated prime functions -}
getHtml' :: (MmanagerTLS m, Mrequest m, Mresponse m) => String -> m String
getHtml' url = do
  {- To get secure HTML we will need: TLS manager, request and response -}
  manager  <- newManager'   tlsManagerSettings
  request  <- parseRequest' url
  response <- httpLbs' request manager
  let html =  L8.unpack $ responseBody response

  {- And we are no longer allowed to log to a file, only retrieve HTML -}
  -- writeFile "./logging.txt" html
  {- error:
    • Couldn't match type ‘m’ with ‘IO’
      ‘m’ is a rigid type variable bound by
        the type signature for:
          getHtml' :: forall (m :: * -> *).
                      (MmanagerTLS m, Mrequest m, Mresponse m) =>
                      String -> m String
        at GranulateEffects.hs:70:13
      Expected type: m ()
        Actual type: IO ()
    • In a stmt of a 'do' block: writeFile "./logging.txt" html
      In the expression:
        do { manager <- newManager' tlsManagerSettings;
             request <- parseRequest' url;
             response <- httpLbs' request manager;
             let html = L8.unpack $ responseBody response;
             .... }
      In an equation for ‘getHtml'’:
          getHtml' url
            = do { manager <- newManager' tlsManagerSettings;
                   request <- parseRequest' url;
                   response <- httpLbs' request manager;
                   .... }
    • Relevant bindings include
        getHtml' :: String -> m String (bound at GranulateEffects.hs:71:1)
  -}
  
  return html
  
{- And ofc it's easier to just put everything inside the same monad -}
class Monad m =>
  HtmlHttpsM m where
  newManager''   :: ManagerSettings -> m Manager
  parseRequest'' :: String -> m Request
  httpLbs''      :: Request -> Manager -> m (Response L8.ByteString)

instance HtmlHttpsM IO where
  newManager''   = newManager
  parseRequest'' = parseRequest
  httpLbs''      = httpLbs

{- We update the signature and add our granulated prime prime functions -}
getHtml'' :: (HtmlHttpsM m) => String -> m String
getHtml'' url = do
  {- To get secure HTML we will need: TLS manager, request and response -}
  manager  <- newManager''   tlsManagerSettings
  request  <- parseRequest'' url
  response <- httpLbs'' request manager
  let html =  L8.unpack $ responseBody response

  {- And we are still not allowed to log to a file, only retrieve HTML -}
  -- writeFile "./logging.txt" html
  {- error:
    • Couldn't match type ‘m’ with ‘IO’
      ‘m’ is a rigid type variable bound by
        the type signature for:
          getHtml'' :: forall (m :: * -> *).
                       HtmlHttpsM m =>
                       String -> m String
        at GranulateEffects.hs:111:14
      Expected type: m ()
        Actual type: IO ()
    • In a stmt of a 'do' block: writeFile "./logging.txt" html
      In the expression:
        do { manager <- newManager'' tlsManagerSettings;
             request <- parseRequest'' url;
             response <- httpLbs'' request manager;
             let html = L8.unpack $ responseBody response;
             .... }
      In an equation for ‘getHtml''’:
          getHtml'' url
            = do { manager <- newManager'' tlsManagerSettings;
                   request <- parseRequest'' url;
                   response <- httpLbs'' request manager;
                   .... }
    • Relevant bindings include
        getHtml'' :: String -> m String
          (bound at GranulateEffects.hs:112:1)
  -}
  
  return html

main :: IO ()
main = do
  html0 <- getHtml   "https://www.google.dk/"
  html1 <- getHtml'  "https://www.google.dk/"
  html2 <- getHtml'' "https://www.google.dk/"
  print $ html0
  print $ html1
  print $ html2
{% endhighlight %}

### Haskell Code output:

{% highlight text %}
mon@razerRamon:~/tmp/haskell/granulate-effects$ ./GranulateEffects.hs
<!doctype html> ... </script></div></body></html>
<!doctype html> ... </script></div></body></html>
<!doctype html> ... </script></div></body></html>
mon@razerRamon:~/tmp/haskell/granulate-effects$
{% endhighlight %}

### Files

{% highlight text %}
mon@razerRamon:~/tmp/haskell/granulate-effects$ ll
total 68K
-rwxr-xr-x 1 mon mon 3.2K Nov 24 20:57 GranulateEffects.hs*
-rw-r--r-- 1 mon mon  46K Nov 24 21:29 logging.txt
mon@razerRamon:~/tmp/haskell/granulate-effects$ 
{% endhighlight %}


### References:

* Erlang Solutions YouTube channel:
  - [Kris Jenkins - Types As Design Tools][youtube]
  
[youtube]: https://www.youtube.com/watch?v=6mUAvd6i4OU
