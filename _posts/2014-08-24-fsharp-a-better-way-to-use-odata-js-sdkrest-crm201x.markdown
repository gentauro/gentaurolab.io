--- 
layout: post
title: F# - A better way to use OData with JavaScript and SDK.REST.js on CRM2011 and CRM2013
categories:
  - English
tags:
  - snippet
  - f#
  - odata
  - javascript
  - sdk
  - rest
  - crm2011
  - crm2013
time: "12:32"
---

### Code Snippet:

{% highlight ocaml linenos %}
#if INTERACTIVE
#r "System.Data.Services.Client"
#r "FSharp.Data.TypeProviders"
#endif

open System
open System.Net
open System.Data.Services.Client
open Microsoft.FSharp.Data

[<Literal>]
let url = @"https://demo.crm4.dynamics.com/XRMServices/2011/OrganizationData.svc/"

[<Literal>] // "%TMP%/odata/OrganizationData.csdl"
let csdl = __SOURCE_DIRECTORY__  + @"/odata/OrganizationData.csdl" 

type Xrm = 
    TypeProviders.ODataService<
        ServiceUri = url,
        LocalSchemaFile = csdl,
        ForceUpdate = false>

let ctx = Xrm.GetDataContext()

// To be used when writing JavaScript with OData
ctx.DataContext.SendingRequest.Add (
    fun eventArgs -> printfn "-Url: %A" eventArgs.Request.RequestUri)
ctx.DataContext.SendingRequest.Add (
    fun eventArgs -> printfn "-Query: %s" eventArgs.Request.RequestUri.Query)

// Remember to "pipe" to a Sequence, in order to evaluate the Linq Query:
query { for a in ctx.AccountSet do
        where (a.AccountNumber = "42")
        select (a.AccountNumber, a.AccountId)
        skip 5
        take 1} 
|> Seq.map id
{% endhighlight %}

### Code output:

{% highlight text %}
val url : string =
  "https:demo.crm4.dynamics.com/XRMServices/2011/OrganizationData.svc/"
val csdl : string =
  "C:\Users\rsm\AppData\Local\Temp\odata\OrganizationData.csdl"
type Xrm =
  class
    static member GetDataContext : unit -> Xrm.ServiceTypes.SimpleDataContextTypes.demoContext
     + 1 overload
    nested type ServiceTypes
  end
val ctx : Xrm.ServiceTypes.SimpleDataContextTypes.demoContext
val it : unit = ()
-Uri: https:demo.crm4.dynamics.com/XRMServices/2011/OrganizationData.svc/AccountSet()?$filter=AccountNumber eq '42'&$skip=5&$top=1&$select=AccountNumber,AccountId
-Query: $filter=AccountNumber eq '42'&$skip=5&$top=1&$select=AccountNumber,AccountId
val it : seq<string * Guid> = seq []
{% endhighlight %}

### Remark:

The reason <code>LocalSchemaFile = csdl</code> and <code>ForceUpdate =
false</code> are set to static values are because Microsoft still doesn't allow
us to use OData from a server side context (we still need to login to CRM Online
with a browser that supports JavaScript). If anybody have a **hint** on how to
access the CSDL service from a .NET application, please write a comment below.

The point is that even though there is no data returned in the .NET application
from CRM Online, it doesn't matter as we just want to use the queries (nicely
written in Linq with intellisense and type-safety) outputted from <code>fun
eventArgs -> printfn "-Query: %s" eventArgs.Request.RequestUri.Query</code> for
our JavaScript code in combination with the official SDK.REST.js library:

{% highlight javascript linenos %}
SDK.REST.retrieveMultipleRecords(
  "Account",
  "$filter=AccountNumber eq '42'&$skip=5&$top=1&$select=AccountNumber,AccountId", // query
  function (results) {
    // Do stuff
  },
  errorHandler,
  onCompleteHandler
);
{% endhighlight %}

The current way it's done (and built on @deprecated technologies) is just not
very **handy** (and has never been) as it requires to expand your current CRM
tenant with a 3rd party managed solution:

[CRM 2011 OData Query Designer](https://crm2011odatatool.codeplex.com/)

### References:

* MSDN: [Walkthrough: Accessing an OData Service by Using Type Providers
  (F#)](http://msdn.microsoft.com/en-us/library/hh156504.aspx)

* MSDN: [Sample: Create, retrieve, update, and delete using the OData endpoint
  with JavaScript](http://msdn.microsoft.com/en-us/library/gg334427.aspx)