---
layout: post
title: Software Equality (updated with SVG version)
categories:
  -English
tags:
  -fun
  -software
  -equality
  -logo
time: "14:40"
---

### The Software Equality logo

Last year the [Human Rights Campaign](http://www.hrc.org/) created the [Marriage
Equality
logo](http://en.wikipedia.org/wiki/Human_Rights_Campaign#Marriage_equality_logo):

<img
src="/assets/img/posts/2014-03-29-software-equality-marriage-equality-logo.svg"
width="50%" height="50%" alt="All" />

I thought to myself, why not make a logo for the ƒuntional people who want to
create ƒunctional applications but aren't allowed to do it because of the old
fashioned and conservative software industry. So I made this logo:

<img
src="/assets/img/posts/2014-03-29-software-equality-software-equality-logo-red.svg"
width="50%" height="50%" alt="All" />

Well I made the logo but forgot to write a blog post about it. But now and
coinciding with the 1 year anniversary of the **Marriage Equality logo**,
2013-03-25, why not bring up the issue with this logo so people who want to make
ƒuntional software can do it without being worried or persecuted by the masses.

If you have a little ƒuntional programmer in you, now it's the time to come out
of the closet :-)

### Bret Victor The Future of Programming

<iframe width="720" height="540" src="//www.youtube.com/embed/8pTEmbeENF4"
frameborder="0"> </iframe>
