--- 
layout: post
title: F# - Fibonacci with CSP and F# Agents
categories:
  - English
tags:
  - snippet
  - f#
time: "17:52"
---

<img
src="/assets/img/posts/2015-07-11-fsharp-fibonacci-with-csp-and-fsharp-agents_fib.png"
alt="All">

### Code Snippet:

{% highlight ocaml linenos %}
#nowarn "40"

open System

/// Types (alias types)
type Agent<'a> = MailboxProcessor<'a>

/// Domain agents
let print fn =
  Agent.Start(fun inbox ->
    let rec loop = async {
      let! msg = inbox.Receive()
      fn msg
      return! loop }
    loop)

let duplicate a1 a2 fn =
  Agent.Start(fun inbox ->
    let rec loop = async {
      let! msg = inbox.Receive()
      fn a1 a2 msg
      return! loop }
    loop)

let delay a fn =
  Agent.Start(fun inbox ->
    let rec loop = async {
      let! msg = inbox.Receive()
      fn a msg
      return! loop }
    loop)

let add a fn =
  Agent.Start(fun inbox ->
    let rec loop state = async {
      let! msg = inbox.Receive()
      state |> function | None -> () | Some v -> fn a (0I+v+msg);
      return! loop (Some (msg))}
    loop (None))

/// Domain functions
let out = lazy print (fun msg -> printfn "%A" msg)
let rec delta2int =
  lazy
    duplicate out (pairsInt:Lazy<Agent<bigint>>)
      (fun a1 a2 msg -> a1.Value.Post msg; a2.Value.Post msg)
and prefixInt0 = lazy delay delta2int (fun a msg -> a.Value.Post msg)
and prefixInt1 = lazy delay prefixInt0 (fun a msg -> a.Value.Post msg)
and pairsInt = lazy add prefixInt1 (fun a msg -> a.Value.Post msg)

prefixInt0.Value.Post 0I
prefixInt1.Value.Post 1I

// Main (recursive loop)
let rec main () : unit = main ()
main ()
{% endhighlight %}

### Code output:

{% highlight text %}
type Agent<'a> = MailboxProcessor<'a>
val print : fn:('a -> unit) -> MailboxProcessor<'a>
val duplicate :
  a1:'a -> a2:'b -> fn:('a -> 'b -> 'c -> unit) -> MailboxProcessor<'c>
val delay : a:'a -> fn:('a -> 'b -> unit) -> MailboxProcessor<'b>
val add :
  a:'a ->
    fn:('a -> Numerics.BigInteger -> unit) ->
      MailboxProcessor<Numerics.BigInteger>
val out : Lazy<MailboxProcessor<bigint>> = Value is not created.
val delta2int : Lazy<MailboxProcessor<bigint>> = Value is not created.
val prefixInt0 : Lazy<MailboxProcessor<bigint>> = Value is not created.
val prefixInt1 : Lazy<MailboxProcessor<bigint>> = Value is not created.
val pairsInt : Lazy<Agent<bigint>> = Value is not created.
{% endhighlight %}

{% highlight text %}
0
1
1
2
3
5
8
13
21
34
55
89
144
233
377
610
987
1597
2584
4181
6765
10946
17711
28657
46368
75025
121393
196418
317811
514229
832040
1346269
2178309
3524578
5702887
9227465
14930352
24157817
39088169
63245986
102334155
165580141
267914296
433494437
701408733
1134903170
1836311903
2971215073
4807526976
7778742049
12586269025
20365011074
32951280099
53316291173
86267571272
139583862445
225851433717
365435296162
591286729879
956722026041
1548008755920
2504730781961
4052739537881
6557470319842
10610209857723
17167680177565
27777890035288
44945570212853
72723460248141
117669030460994
190392490709135
308061521170129
498454011879264
806515533049393
1304969544928657
...
{% endhighlight %}

### Error output (only on Mono):

{% highlight text %}
Unhandled Exception:
System.NullReferenceException: Object reference not set to an instance of an object
  at Microsoft.FSharp.Control.AsyncBuilderImpl+bindA@783[Microsoft.FSharp.Core.Unit,Microsoft.FSharp.Core.Unit].Invoke (Microsoft.FSharp.Control.AsyncParams`1 args) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.AsyncBuilderImpl+callA@805[Microsoft.FSharp.Core.Unit,Microsoft.FSharp.Core.Unit].Invoke (Microsoft.FSharp.Control.AsyncParams`1 args) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.AsyncBuilderImpl+tryWithA@839[Microsoft.FSharp.Core.Unit].Invoke (Microsoft.FSharp.Control.AsyncParams`1 args) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.AsyncBuilderImpl+callA@805[Microsoft.FSharp.Core.Unit,Microsoft.FSharp.Core.Unit].Invoke (Microsoft.FSharp.Control.AsyncParams`1 args) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.AsyncBuilderImpl+queueAsync@727[Microsoft.FSharp.Core.Unit].Invoke (Microsoft.FSharp.Core.Unit unitVar0) [0x00000] in <filename unknown>:0
  at <StartupCode$FSharp-Core>.$Control.loop@426-40 (Microsoft.FSharp.Control.Trampoline this, Microsoft.FSharp.Core.FSharpFunc`2 action) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.Trampoline.ExecuteAction (Microsoft.FSharp.Core.FSharpFunc`2 firstAction) [0x00000] in <filename unknown>:0
  at Microsoft.FSharp.Control.TrampolineHolder.Protect (Microsoft.FSharp.Core.FSharpFunc`2 firstAction) [0x00000] in <filename unknown>:0
  at <StartupCode$FSharp-Core>.$Control+-ctor@512-1.Invoke (System.Object state) [0x00000] in <filename unknown>:0 
{% endhighlight %}

{% highlight text %}
Mono JIT compiler version 4.0.0 ((detached/d136b79 Mon Apr 13 14:40:59 EDT 2015)
Copyright (C) 2002-2014 Novell, Inc, Xamarin Inc and Contributors. www.mono-project.com
        TLS:           normal
        SIGSEGV:       altstack
        Notification:  kqueue
        Architecture:  x86
        Disabled:      none
        Misc:          softdebug
        LLVM:          yes(3.6.0svn-mono-(detached/a173357)
        GC:            sgen
{% endhighlight %}

### References:

* Wikipedia: [Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number)
* Communicating Sequential Processes (CSP): [CSP](http://www.usingcsp.com)