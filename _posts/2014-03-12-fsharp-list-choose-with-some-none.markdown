---
layout: post
title: F# - List choose with Some v/None
categories:
  - English
tags:
  - snippet
  - f#
time: "21:32"
---

### Code Snippet:

{% highlight ocaml linenos %}
type FooBar = { foo : string; bar : int option }

[1 .. 10]
|> List.map(
  fun x -> x % 2 = 0 |> function
  | true ->  { foo = "some"; bar = Some x }
  | false -> { foo = "none"; bar = None   } )
|> List.choose(fun x -> id x.bar)
{% endhighlight %}

### Code output:

{% highlight text %}
> type FooBar = { foo: string; bar: int option; }
> val it : int list = [2; 4; 6; 8; 10]
{% endhighlight %}

### References:

* MSDN: [List.choose](http://msdn.microsoft.com/en-us/library/ee353456.aspx)