--- 
layout: post
title: F# - MS CRM Online Browser Performance Test
categories:
  - English
tags:
  - snippet
  - f#
time: "23:50"
---

### NuGet Snippet:

{% highlight text %}
C:\_tmp\performanceTest>nuget install -ExcludeVersion canopy
Attempting to resolve dependency 'Selenium.WebDriver (= 2.42.0)'.
Attempting to resolve dependency 'Selenium.Support (= 2.42.0)'.
Attempting to resolve dependency 'SizSelCsZzz (= 0.3.36.0)'.
Attempting to resolve dependency 'Newtonsoft.Json (= 6.0)'.
Installing 'Selenium.WebDriver 2.42.0'.
Successfully installed 'Selenium.WebDriver 2.42.0'.
Installing 'Selenium.Support 2.42.0'.
Successfully installed 'Selenium.Support 2.42.0'.
Installing 'Newtonsoft.Json 6.0.1'.
Successfully installed 'Newtonsoft.Json 6.0.1'.
Installing 'SizSelCsZzz 0.3.36.0'.
Successfully installed 'SizSelCsZzz 0.3.36.0'.
Installing 'canopy 0.9.11'.
Successfully installed 'canopy 0.9.11'.
 
C:\_tmp\performanceTest>
{% endhighlight %}

### Script Snippet (DG.StressTest.Browser.cmd):

{% highlight bat %}
@echo off

:: Add the paths for the F# SDK 3.x (from higher version to lower)
set FSHARPSDK=^
C:\Program Files (x86)\Microsoft SDKs\F#\3.1\Framework\v4.0\;^
C:\Program Files (x86)\Microsoft SDKs\F#\3.0\Framework\v4.0\

cls

:: Execute the script "only" with the first "fsianycpu.exe" found
for %%i in (fsianycpu.exe) do "%%~$FSHARPSDK:i" DG.StressTest.Browser.fsx %*

pause
{% endhighlight %}

### Code Snippet (DG.StressTest.Browser.fsx):

{% highlight ocaml linenos %}
(* nuget install -ExcludeVersion canopy *)
#r @"Selenium.Support\lib\net40\WebDriver.Support.dll"
#r @"Selenium.WebDriver\lib\net40\WebDriver.dll"
#r @"canopy\lib\canopy.dll"
 
#load @"DG.Auth.fsx" (* Just contains let usr = "usr" and let pwd = "pwd" *)
 
open System
open System.IO
 
open canopy
open runner
open configuration
 
(* Config canopy *)
compareTimeout <- 30.0
 
(* Utils *)
let timestamp  () = DateTime.Now.ToString("o").Replace(":","")
let timestamp' () = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
 
let string2float = function
  | (n,s) -> match Double.TryParse s with | true, value -> value * n | _ -> 0.
 
let parse (s:string) = 
  match s with
    | ms when s.Contains("ms")     ->    1., ms.Replace(" ms","")
    | KB when s.Contains("KB/sec") ->    1., KB.Replace(" KB/sec","")
    | MB when s.Contains("MB/sec") -> 1000., MB.Replace(" MB/sec","")
    | _ -> failwith "Not recognized unit"
  |> string2float |> int
 
(* Local files / folders *)
let output = @"./output.csv"
let source = @".source/"
 
(* Connection info: *)
let uriMain = Uri(@"https://org.crm4.dynamics.com");
let uriDiag = Uri(uriMain.AbsoluteUri + @"/tools/diagnostics/diag.aspx")
 
(* Save to .source folder *)
let save2source data =
  File.WriteAllText(source + timestamp() + ".log", data)
 
(* Browser Performance Test MS CRM Online *)
let rec performanceTestCrm date path = 
  match (date > DateTime.Now) with
  | true -> 
    click "#runBtn_all"
 
    waitFor (fun () -> (read "#td_status_all") = "complete")
 
    save2source (read "#resultConsole")
 
    let latency, speed, jsArray, jsMorph, jsBase64, jsDOM =
      parse (read "#td_result_latency"),
      parse (read "#td_result_bandwidth"),
      parse (read "#td_result_jsArrayBenchmark"),
      parse (read "#td_result_jsMorphBenchmark"),
      parse (read "#td_result_jsBase64Benchmark"),
      parse (read "#td_result_jsDomBenchmark")
 
    let sw = File.AppendText(path)
    sw.WriteLine(
      sprintf "%s;%i;%i;%i;%i;%i;%i;"
        (timestamp'()) latency speed jsArray jsMorph jsBase64 jsDOM)
    sw.Dispose()
 
    reload()
 
    performanceTestCrm date path
  | false -> ()
 
(* Start Browser Response Test: *)
start chrome
 
"MS CRM Online Browser Performance Test" &&& fun _ ->
  (* Clear output.csv and .source folder *)
  File.Exists(output) |> function 
    | true -> File.Delete(output)
    | false -> ()
  Directory.EnumerateFiles(@".source","*.log",SearchOption.AllDirectories)
  |> Seq.iter(fun x -> File.Delete(x))
 
  (* Go to MS CRM Online *)
  url uriMain.AbsoluteUri
 
  (* Login *)
  "#cred_userid_inputtext"   << DG.Auth.usr
  "#cred_password_inputtext" << DG.Auth.pwd
  click "#cred_sign_in_button"
  press enter
 
  (* Go to diag url *)
  url uriDiag.AbsoluteUri
 
  (* Start and Stop DateTimes *)
  let startDate = DateTime.Now
  let stopDate  = startDate.AddMinutes(60.)
 
  let sw = File.CreateText(output)
  sw.WriteLine(
    "Timestamp (ISO 8601);Latency (ms);Speed (KB/sec);" +
    "JS Array (ms);JS Morph (ms);JS Base64 (ms);JS DOM (ms);"
  )
  sw.Dispose()
 
  performanceTestCrm stopDate output
 
run()
 
quit()
{% endhighlight %}

### Code result:

{% highlight text %}
Starting ChromeDriver (v2.10.267521) on port 64015
Only local connections are allowed.
Test: Browser Performance Test MS CRM Online
Passed
 
60 minutes 19 seconds to execute
1 passed
0 failed
Press any key to continue . . .
{% endhighlight %}

### Code result (output.csv):

{% highlight text %}
Timestamp (ISO 8601);Latency (ms);Speed (KB/sec);JS Array (ms);JS Morph (ms);JS Base64 (ms);JS DOM (ms);
2014-09-18 22:59:59;47;302;229;33;5;13;
2014-09-18 23:00:03;47;302;197;46;6;17;
2014-09-18 23:00:07;48;322;192;45;4;18;
2014-09-18 23:00:10;48;315;186;36;4;12;
2014-09-18 23:00:14;46;302;186;32;3;13;
2014-09-18 23:00:17;47;322;186;33;3;13;
...
2014-09-18 23:59:59;48;329;194;47;4;17;
{% endhighlight %}

### Code result (.source\2014-09-18T225959.2540079+0200.log):

{% highlight text %}
=== Latency Test Info ===
Number of times run: 20
Run 1 time: 46 ms
Run 2 time: 51 ms
Run 3 time: 48 ms
Run 4 time: 47 ms
Run 5 time: 48 ms
Run 6 time: 45 ms
Run 7 time: 46 ms
Run 8 time: 46 ms
Run 9 time: 46 ms
Run 10 time: 52 ms
Run 11 time: 50 ms
Run 12 time: 47 ms
Run 13 time: 47 ms
Run 14 time: 45 ms
Run 15 time: 44 ms
Run 16 time: 47 ms
Run 17 time: 48 ms
Run 18 time: 48 ms
Run 19 time: 45 ms
Run 20 time: 50 ms
Average latency: 47 ms
Client Time: Thu, 18 Sep 2014 20:59:57 GMT
 
=== Bandwidth Test Info ===
Run 1
  Time: 56 ms
  Blob Size: 15180 bytes
  Speed: 264 KB/sec
Run 2
  Time: 49 ms
  Blob Size: 15180 bytes
  Speed: 302 KB/sec
Run 3
  Time: 50 ms
  Blob Size: 15180 bytes
  Speed: 296 KB/sec
Run 4
  Time: 49 ms
  Blob Size: 15180 bytes
  Speed: 302 KB/sec
Run 5
  Time: 51 ms
  Blob Size: 15180 bytes
  Speed: 290 KB/sec
Run 6
  Time: 51 ms
  Blob Size: 15180 bytes
  Speed: 290 KB/sec
Run 7
  Time: 52 ms
  Blob Size: 15180 bytes
  Speed: 285 KB/sec
Run 8
  Time: 52 ms
  Blob Size: 15180 bytes
  Speed: 285 KB/sec
Run 9
  Time: 50 ms
  Blob Size: 15180 bytes
  Speed: 296 KB/sec
Run 10
  Time: 53 ms
  Blob Size: 15180 bytes
  Speed: 279 KB/sec
Max Download speed: 302 KB/sec
Client Time: Thu, 18 Sep 2014 20:59:58 GMT
 
=== Browser Info ===
Browser CodeName: Mozilla
Browser Name: Netscape
Browser Version: 5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36
Cookies Enabled: true
Platform: Win32
User-agent header: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36
Client Time: Thu, 18 Sep 2014 20:59:58 GMT
 
=== Machine Info ===
Client IP Address: XXX.XXX.XXX.XXX
Client Time: Thu, 18 Sep 2014 20:59:58 GMT
 
=== Array Manipultaion Benchmark ===
Time: 229 ms
Client Time: Thu, 18 Sep 2014 20:59:58 GMT
 
=== Morph Benchmark ===
Time: 33 ms
Client Time: Thu, 18 Sep 2014 20:59:58 GMT
 
=== Base 64 Benchmark ===
Time: 5 ms
Client Time: Thu, 18 Sep 2014 20:59:59 GMT
 
=== DOM Benchmark ===
Total Time: 13 ms
Breakdown:
  Append:  3ms
  Prepend: 5ms
  Index:   0ms
  Insert:  4ms
  Remove:  1ms
Client Time: Thu, 18 Sep 2014 20:59:59 GMT
 
=== Organization Info ===
Organization name: orgSomeIdNumber
Is Live: True
Server time: 9/18/2014 8:59:55 PM UTC
Url: https://org.crm4.dynamics.com//tools/diagnostics/diag.aspx
Client Time: Thu, 18 Sep 2014 20:59:59 GMT
{% endhighlight %}

### Chart diagrams:

<img src="/assets/img/posts/2014-09-18-fsharp-ms-crm-online-browser-performance-test-latency.jpg"
alt="All">

<img src="/assets/img/posts/2014-09-18-fsharp-ms-crm-online-browser-performance-test-speed.jpg"
alt="All">

<img src="/assets/img/posts/2014-09-18-fsharp-ms-crm-online-browser-performance-test-javascript.jpg"
alt="All">

### Architecture (Lenovo ThinkPad W540):

<img src="/assets/img/posts/2014-09-18-fsharp-ms-crm-online-browser-performance-test-architecture.jpg"
alt="All">
