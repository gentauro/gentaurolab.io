--- 
layout: post
title: Haskell - EU MEPs overview on Article 13 (“meme ban”)
categories:
  - English
tags:
  - snippet
  - haskell
  - eu
  - mep
  - article
  - thirteen
  - meme
  - ban
time: "01:05"
---


### Code Snippet

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --package bytestring
   --package containers
   --package http-client
   --package http-client-tls
   --package tagsoup
   --
-}

--------------------------------------------------------------------------------
--
-- (c) 2019 SPISE MISU ApS, opensource.org/licenses/LGPL-3.0
--
--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Control.Arrow
  ( second
  )
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.List
  ( sortOn
  )
import qualified Data.Map                   as Dict
import           Data.Maybe
  ( catMaybes
  )
import           Data.String
  ( IsString
  )
import           Network.HTTP.Client
  ( Manager
  , httpLbs
  , newManager
  , parseRequest
  , requestHeaders
  , responseBody
  )
import           Network.HTTP.Client.TLS
  ( tlsManagerSettings
  )
import           Text.HTML.TagSoup
  ( Tag (TagOpen)
  , parseTags
  , (~/=)
  )

--------------------------------------------------------------------------------

-- HTTP

headers
  :: ( IsString a, IsString b )
  => [ (a, b) ]
headers =
  [ ( "Host"
    , "saveyourinternet.eu"
    )
  , ( "User-Agent"
    , "Links (2.14; Linux x86_64; text)"
    )
  ]

get :: Manager -> String -> IO L8.ByteString
get manager url =
  do
    req <- parseRequest url
    res <- httpLbs req { requestHeaders = headers } manager
    return $ responseBody res

--------------------------------------------------------------------------------

-- PARSE

data MEP
  = Pledged
  | Against
  | Neutral
  | For
  deriving (Eq, Ord, Show)

parse :: L8.ByteString -> [ MEP ]
parse bs =
  concat $ catMaybes $ map f $ rows
  where
    tags = parseTags bs
    rows =
      takeWhile (~/= ("</tbody>" :: String)) $
      drop 1 $
      dropWhile (~/= ("<tbody>"  :: String)) tags
    f ( TagOpen "tr" xs) = Just $ catMaybes $ map (position . snd) xs
    f __________________ = Nothing
    position x =
      case x of
        "tshowcase-mepspledged" -> Just Pledged
        "tshowcase-mepsagainst" -> Just Against
        "tshowcase-mepsneutral" -> Just Neutral
        "tshowcase-mepsfor"     -> Just For
        _______________________ -> Nothing

--------------------------------------------------------------------------------

-- MISC

data EU
  = Austria
  | Belgium
  | Bulgaria
  | Croatia
  | Cyprus
  | CzechRepublic
  | Denmark
  | Estonia
  | Finland
  | France
  | Germany
  | Greece
  | Hungary
  | Ireland
  | Italy
  | Latvia
  | Lithuania
  | Luxembourg
  | Malta
  | Netherlands
  | Slovenia
  | Spain
  | Poland
  | Portugal
  | Romania
  | Slovakia
  | Sweden
  | UnitedKingdom
  | Total
  deriving (Eq, Ord, Show)

ccTLD :: EU -> String
ccTLD Austria       = "at"
ccTLD Belgium       = "be"
ccTLD Bulgaria      = "bg"
ccTLD Croatia       = "hr"
ccTLD Cyprus        = "cy"
ccTLD CzechRepublic = "cz"
ccTLD Denmark       = "dk"
ccTLD Estonia       = "ee"
ccTLD Finland       = "fi"
ccTLD France        = "fr"
ccTLD Germany       = "de"
ccTLD Greece        = "gr"
ccTLD Hungary       = "hu"
ccTLD Ireland       = "ie"
ccTLD Italy         = "it"
ccTLD Latvia        = "lv"
ccTLD Lithuania     = "lt"
ccTLD Luxembourg    = "lu"
ccTLD Malta         = "mt"
ccTLD Netherlands   = "nl"
ccTLD Poland        = "pl"
ccTLD Portugal      = "pt"
ccTLD Romania       = "ro"
ccTLD Slovakia      = "sk"
ccTLD Slovenia      = "si"
ccTLD Spain         = "es"
ccTLD Sweden        = "se"
ccTLD UnitedKingdom = "uk"
ccTLD Total         = "--"

base :: String
base =
  "https://saveyourinternet.eu/"

urls :: [ EU ] -> [ String ]
urls =
  map (base ++) . map ccTLD

groupBy
  :: Ord k
  => (a -> k)
  -> [a]
  -> [(k,[a])]
groupBy f =
  -- https://hoogle.haskell.org/?hoogle=Ord k => (a -> k) -> [a] -> [(k, [a])]
  Dict.toAscList . Dict.fromListWith (++) . map (\a -> (f a, [a]))

--------------------------------------------------------------------------------

main :: IO ()
main =
  do
    manager   <- newManager tlsManagerSettings
    responses <- mapM (get manager) $ urls $ cts
    let meps = map parse responses
    putStrLn $ "# EU MEPs with regard of Article 13 (aka `meme ban`):"
    putStrLn $ ""
    putStrLn $ replicate len '-'
    putStrLn $ "Country       | ccTLD "
      ++ " | Pledged   | Against   | Neutral   | For       | Total    "
      ++ " | Pledged % | Against % | Neutral % | For %     | Total %   |"
    putStrLn $ replicate len '-'
    mapM_ (putStrLn . ppd)
      $ sortOn snd
      $ zip cts
      $ aux
      $ meps
    putStrLn $ replicate len '-'
    mapM_ (putStrLn . ppd)
      $ [ (Total, concat $ aux $ meps) ]
    putStrLn $ replicate len '-'
    putStrLn $ ""
    putStrLn $ "Source: " ++ base ++ " and https://pledge2019.eu/"
    where
      len = 144
      ppd (c,ps) =
        cn ++ (rep $ (13 -) $ length cn) ++ " | " ++
        cc ++ (rep    04               ) ++ " | " ++
        (pad $ (09 -) $ length mp) ++ mp ++ " | " ++
        (pad $ (09 -) $ length ma) ++ ma ++ " | " ++
        (pad $ (09 -) $ length mn) ++ mn ++ " | " ++
        (pad $ (09 -) $ length mf) ++ mf ++ " | " ++
        (pad $ (09 -) $ length mt) ++ mt ++ " | " ++
        (pad $ (09 -) $ length pp) ++ pp ++ " | " ++
        (pad $ (09 -) $ length pa) ++ pa ++ " | " ++
        (pad $ (09 -) $ length pn) ++ pn ++ " | " ++
        (pad $ (09 -) $ length pf) ++ pf ++ " | " ++
        (pad $ (09 -) $ length pt) ++ pt ++ " | "
        where
          cn = show  c
          cc = ccTLD c
          p  = sum $ snd <$> filter ((== Pledged) . fst) ps
          a  = sum $ snd <$> filter ((== Against) . fst) ps
          n  = sum $ snd <$> filter ((== Neutral) . fst) ps
          f  = sum $ snd <$> filter ((== For)     . fst) ps
          t  = p + a + n + f
          mp = show  p
          ma = show  a
          mn = show  n
          mf = show  f
          mt = show  t
          t' =                      fromIntegral t
          p' = (/ t') $ (* 100.0) $ fromIntegral p
          a' = (/ t') $ (* 100.0) $ fromIntegral a
          n' = (/ t') $ (* 100.0) $ fromIntegral n
          f' = (/ t') $ (* 100.0) $ fromIntegral f
          pp = show $ dec p' 1
          pa = show $ dec a' 1
          pn = show $ dec n' 1
          pf = show $ dec f' 1
          pt = "100.0"
      dec :: Double -> Int -> Double
      dec x n = y / z
        where
          y = fromIntegral $ ((round $ x * z) :: Integer)
          z = 10 ^ n
      rep = (flip replicate) ' '
      pad = (flip replicate) ' '
      aux = map (map (second length)) . map (groupBy id)
      cts =
        [ Austria
        , Belgium
        , Bulgaria
        , Croatia
        , Cyprus
        , CzechRepublic
        , Denmark
        , Estonia
        , Finland
        , France
        , Germany
        , Greece
        , Hungary
        , Ireland
        , Italy
        , Latvia
        , Lithuania
        , Luxembourg
        , Malta
        , Netherlands
        , Slovenia
        , Spain
        , Poland
        , Portugal
        , Romania
        , Slovakia
        , Sweden
        , UnitedKingdom
        ]
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../saveyourinternet$ ./Main.hs 
{% endhighlight %}

<div align="center">
<figure>
<img
src="/assets/img/posts/2019-02-21-haskell-eu-meps-overview-on-article-13-meme-ban_console-output.png" />
<figcaption>
Code Output saved as a picture for better visibility
</figcaption>
</figure>
</div>


### References:

* The fight against Article 13:
  - [SaveYourInternet][saveyourinternet]
  - [Pledge 2019: Against Article 13][pledgeeu]

[saveyourinternet]: https://saveyourinternet.eu/
[pledgeeu]:         https://pledge2019.eu/en
