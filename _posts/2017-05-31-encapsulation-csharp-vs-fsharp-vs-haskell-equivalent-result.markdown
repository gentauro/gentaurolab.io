--- 
layout: post
title: Encapsulation - C# vs F# vs Haskell, equivalent result
categories:
  - English
tags:
  - snippet
  - encapsulation
  - f#
  - c#
  - haskell
time: "23:18"
---

### Files

{% highlight text %}
mon@razerRamon:~/tmp/encapsulation$ ll -R
.:
total 12K
drwxrwxr-x 2 mon mon 4.0K May 31 23:08 cs/
drwxrwxr-x 2 mon mon 4.0K May 31 23:10 fs/
drwxrwxr-x 2 mon mon 4.0K May 31 23:03 hs/

./cs:
total 36K
-rwxrwxr-x 1 mon mon   57 May 31 21:36 build.bash*
-rw-rw-r-- 1 mon mon  662 May 31 23:07 Movie.cs
-rwxrwxr-x 1 mon mon 3.5K May 31 23:08 Movie.exe*

./fs:
total 12K
-rwxrwxr-x 1 mon mon 694 May 31 23:10 Movie.fsx*

./hs:
total 24K
-rw-rw-r-- 1 mon mon 470 May 31 23:03 Movie.hs
-rwxrwxr-x 1 mon mon 466 May 31 23:02 Program.hs*
mon@razerRamon:~/tmp/encapsulation$ 
{% endhighlight %}

### C# Code Snippet

{% highlight csharp linenos %}
using System;

class Program
{
    public class Movie
    {
	private uint rating = 0;

	public uint Rating /* Values between 0 (default) and 5 */
	{
	    get
	    {
		return rating;
	    }
	    set
	    {
		if (value > 5)
		{
		    rating = 5; /* Normalize high values to 0 - 5 scale */
		}
		else
		{
		    rating = value;
		}
	    }
	}
    }

    static void Main()
    {
	var movie = new Movie();
	/* Computer says no:
	   
	   Movie.cs(33,8): error CS0122: `Program.Movie.rating' is inaccessible 
	   due to its protection level
	   
	movie.rating = 1024; */
	movie.Rating = 1024;
	Console.WriteLine("Movie rating equals 5: {0}", movie.Rating == 5);
    }
}
{% endhighlight %}

### C# Code output:

{% highlight text %}
mon@razerRamon:~/tmp/encapsulation/cs$ ./build.bash && ./Movie.exe 
Movie rating equals 5: True
{% endhighlight %}

### F# Code Snippet

{% highlight ocaml linenos %}
#!/usr/bin/env fsharpi

module Movie =
  type movie = { rating : rating }
  and rating = private Rating of uint32 (* Values between 0 (default) and 5 *)
  
  let init () = { rating = Rating 0u }
  
  let get { rating = Rating value } = value
  let set value movie =
    let value' =
      match value > 5u with
        | true  -> 5u
        | false -> value
    { movie with rating = Rating value' }

let movie  = Movie.init()
(* Computer says no:

   Movie.fsx(23,36): error FS0039: The value or constructor 'Rating' is not
   defined
   
let movie' = { movie with rating = Rating 1024u } *)
let movie' = movie |> Movie.set 1024u
printfn "Movie rating equals 5: %b" (movie' |> Movie.get = 5u)
{% endhighlight %}

### F# Code output:

{% highlight text %}
mon@razerRamon:~/tmp/encapsulation/fs$ ./Movie.fsx 
Movie rating equals 5: true
{% endhighlight %}

### Haskell Code Snippet

{% highlight haskell linenos %}
module Movie (Movie, create, get, set) where

newtype Rating = Rating Integer

data Movie = Movie { rating :: Rating } {- Values between 0 (default) and 5 -}

create () = Movie (Rating 0)

get (Movie (Rating ( value ))) = value
set movie value =
  let value' =
        case value > 5 of
          True -> 5
          False -> value
      value'' =
          case value' < 0 of
            True -> 0
            False -> value'
  in
    movie { rating = Rating value'' }
{% endhighlight %}

{% highlight haskell linenos %}
#!/usr/bin/env runhaskell

module Main (main) where

import Movie(Movie, create, get, set)

main =
  let movie  = create ()
      {- Computer says no:
      
         Program.hs:9:24: ‘rating’ is not a (visible) constructor field name
         Program.hs:9:33: Not in scope: data constructor ‘Rating’
      
      movie' = movie { rating = Rating 1024 } -}
      movie' = set movie 1024
  in
    print ("Movie rating equals 5: " ++ show ((get movie') == 5))
{% endhighlight %}

### Haskell Code output:

{% highlight text %}
mon@razerRamon:~/tmp/encapsulation/hs$ ./Program.hs 
"Movie rating equals 5: True"
{% endhighlight %}

### References:

* Wikipedia:
  - [Encapsulation](https://en.wikipedia.org/wiki/Encapsulation_(computer_programming))
