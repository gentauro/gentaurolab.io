--- 
layout: post
title: OCaml - Welcome to the RealWorld, baby
categories:
  - English
tags:
  - snippet
  - ocaml
  - monads
  - monadic
  - haskell
  - haskellish
time: "07:07"
---

### Real World Code Snippet

{% highlight ocaml linenos %}
module RealWorld : sig                              
                                                    
  type   world = private World                      
  type α pure  = private Pure of α                  
  type α io    = world → (α pure * world)           
                                                    
  val bind : α io → β io → β io                     
  val lift : α io → (α pure → β io) → β io          
                                                    
  val ( >> )  : α io →           β io  → β io       
  val ( >>= ) : α io → (α pure → β io) → β io       
                                                    
  val unit : unit pure                              
                                                    
  val effect : (α → β) → α pure → β io              
  val eval   : unit io → (unit pure * world)        
                                                    
end                                                 
                                                    
  = struct                                          
                                                    
  type   world = World                              
  type α pure  = Pure of α                          
  type α io    = world → (α pure * world)           
                                                    
  let bind : α io → β io → β io =                   
    λ action1 action2 world0 →                      
      let (a,world1) = action1 world0 in            
      let (b,world2) = action2 world1 in            
      (b,world2)                                    
                                                    
  let lift : α io → (α pure → β io) → β io =        
    λ action1 action2 world0 →                      
      let (a,world1) = action1   world0 in          
      let (b,world2) = action2 a world1 in          
      (b,world2)                                    
                                                    
  let ( >> )  : α io →           β io  → β io = bind
  let ( >>= ) : α io → (α pure → β io) → β io = lift
                                                    
  let unit : unit pure = Pure ()                    
                                                    
  let effect : (α → β) → α pure → β io =            
    λ f (Pure a) →                                  
      λ world → Pure (f a), world                   
                                                    
  let eval : unit io → (unit pure * world) =        
    λ main → main World                             
                                                    
end 
{% endhighlight %}

### Real World Code output:

{% highlight text %}
module RealWorld :
  sig
    type world = private World
    type 'a pure = private Pure of 'a
    type 'a io = world -> 'a pure * world
    val bind    : 'a io -> 'b io -> 'b io
    val lift    : 'a io -> ('a pure -> 'b io) -> 'b io
    val ( >> )  : 'a io -> 'b io -> 'b io
    val ( >>= ) : 'a io -> ('a pure -> 'b io) -> 'b io
    val unit    : unit pure
    val effect  : ('a -> 'b) -> 'a pure -> 'b io
    val eval    : unit io -> unit pure * world
  end
{% endhighlight %}

### Utils Code Snippet

{% highlight ocaml linenos %}
module Util = struct                                
                                                    
  open RealWorld                                    
                                                    
  let (!) : α → α io =                              
    λ a → effect (λ _ → a) unit                     
                                                    
  let readLn : string io =                          
    effect read_line unit                           
                                                    
  let putStr : string pure → unit io =              
    effect print_string                             
                                                    
  let putStrLn : string pure → unit io =            
    effect print_endline              	              
                                                    
  let sample : unit io =                            
    ! "What is your name?"                          
    >>= putStrLn                                    
    >>  readLn                                      
    >>= λ a →                                       
      ! "How old are you?"                          
      >>= putStrLn                                  
      >>  readLn                                    
      >>= λ b →                                     
        putStr a                                    
        >>  ! ": "                                  
        >>= putStr                                  
        >>  putStrLn b                              
                                                    
end                                                 
{% endhighlight %}

### Utils Code output:

{% highlight text %}
module Util :
  sig
    val ( ! )    : 'a -> 'a RealWorld.io
    val readLn   : string RealWorld.io
    val putStr   : string RealWorld.pure -> unit RealWorld.io
    val putStrLn : string RealWorld.pure -> unit RealWorld.io
    val sample   : unit RealWorld.io
  end
{% endhighlight %}

### Execution Code Snippet

{% highlight ocaml %}
let _ = Util.sample |> RealWorld.eval 
{% endhighlight %}

{% highlight text %}
mon@razerRamon:~/tmp/ocaml$ ocaml real_world.ml
{% endhighlight %}

### Execution Code output:

{% highlight text %}
What is your name?
John Doe
How old are you?
42
John Doe: 42
{% endhighlight %}


### References:

* The Haskell Programming Language, **IO inside**:
  - [Welcome to the RealWorld, baby][realworld]

[realworld]: https://wiki.haskell.org/IO_inside#Welcome_to_the_RealWorld.2C_baby
