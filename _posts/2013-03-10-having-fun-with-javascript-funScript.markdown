---
layout: post
title: Having F#un with JavaScript (FunScript)
categories:
  - English
tags: 
  - f# 
  - funscript 
  - javasript
time: "15:30"
---

JavaScript is the most used programing language on GitHub (21% of all the
projects, [https://github.com/languages/](https://github.com/languages/)). All
of us that have worked with this programing language, know that it's very
difficult to find errors while writing the code, as it is with all other
dynamics typed languages. This is where FunScript enters the picture, by giving
the possiblity to write in F#, a strongly typed programing language, and then by
compiling it to JavaScript. But haven't we heard this before? Write in a
strongly-typed language and then compile to JavaScript (CoffeScript, Haxe,
Cappuccino, Dart, TypeScript, ...).  Yes, but the main difference between
FunScript and other frameworks, is that it doesn't rely on a FFI (Foreign
Function Interface) but on TypeProviders which makes the Framework very easy to
extend.

FunScript supports REST + TypeScript, among others as TypeProviders. This way
you can rely on other to write them (d3.d.ts, google.maps.d.ts, jquery.d.ts,
jquerymobile.d.ts, jqueryui.d.ts, node.d.ts, ...) and if you need a special
library you can create your own TypeScript declaration file and contribute to
expand the FunScript community.

Link to slides: [Slides](http://goo.gl/pSfcY)

And for more information (and video soon): [Open Source Days
2013](http://opensourcedays.org/2013/content/having-fun-javascript-funscript)