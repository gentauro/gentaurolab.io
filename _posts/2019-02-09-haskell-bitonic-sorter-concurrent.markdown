--- 
layout: post
title: Haskell - Bitonic sorter (concurrent)
categories:
  - English
tags:
  - snippet
  - haskell
  - bitonic
  - sorter
  - concurrent
time: "00:38"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2019-02-08-haskell-bitonic-sorter-sequential_bitonic-sort.png" />
<figcaption>
Author Bitonic at English Wikipedia (CC0 1.0)
</figcaption>
</figure>
</div>

### Code Snippets

#### Control/Asynchronous.hs

{% highlight haskell linenos %}
{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Control.Asynchronous
  ( ForeignThreadInterface
  , Fork
  , fork
  , join
  , sync
  )
where

--------------------------------------------------------------------------------

import           Control.Concurrent
  ( ThreadId
  , forkFinally
  )
import           Control.Concurrent.MVar
  ( MVar
  , newEmptyMVar
  , putMVar
  , readMVar
  )
import           GHC.Exception
  ( SomeException
  , throw
  )

--------------------------------------------------------------------------------

data Fork a = Fork !ThreadId (IO (Either SomeException a))

--------------------------------------------------------------------------------

-- To prevent the users from adding instances of `ForeignThreadInterface`, we
-- provide a middle-layer (`ASYNC`) between the `Monad` instance and the `Proxy`
-- (`ForeignThreadInterface`) instance.

class Monad m => ASYNC                  m where
  new :: m (MVar a)
  put ::    MVar a -> a -> m ( )
  get ::    MVar a ->      m  a
class ASYNC m => ForeignThreadInterface m where
  fork ::   m    a   -> m (Fork a)
  join ::   Fork a   -> m       a
  sync :: [ Fork a ] -> m      ( )

--------------------------------------------------------------------------------

instance ASYNC                  IO where
  -- newEmptyMVar: Create an MVar which is initially empty.
  new = newEmptyMVar
  -- putMVar: Put a value into an MVar.
  put = putMVar
  -- readMVar: Atomically read the contents of an MVar. If the MVar is currently
  -- empty, readMVar will wait until it is full. readMVar is guaranteed to
  -- receive the next putMVar.
  get = readMVar
instance ForeignThreadInterface IO where
  fork = \ compute ->
    new                               >>= \ var ->
    forkFinally compute (finally var) >>= \ tid ->
    pure $ Fork tid $ get var
    where
      finally v = \ r -> put v r
  join (Fork _ mvar) =
    mvar >>= \ var ->
    case var of
      Right v -> pure  v
      Left  e -> throw e
  sync =
    mapM_ join
{% endhighlight %}


#### Data/Bitonic.hs

{% highlight haskell linenos %}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE Safe       #-}

--------------------------------------------------------------------------------

module Data.Bitonic
  ( Sortable
  , SortableTrust
  , sort
  )
where

--------------------------------------------------------------------------------

import           Data.Bits
  ( countLeadingZeros
  , finiteBitSize
  , shiftL
  , shiftR
  , (.&.)
  )
import qualified Foreign.Marshal.Alloc as FFI
import           Foreign.Ptr
  ( Ptr
  , plusPtr
  )
import           Foreign.Storable
  ( Storable
  )
import qualified Foreign.Storable      as FFI

import           Control.Asynchronous
  ( ForeignThreadInterface
  , fork
  , sync
  )

--------------------------------------------------------------------------------

-- To prevent the users from adding instances of `ForeignMemoryInterface`, we
-- provide a middle-layer (`FMEMI`) between the `Monad` instance and the `Proxy`
-- (`ForeignMemoryInterface`) instance.
--
-- Note: This is redundant as by using `RankNTypes` to create a type alias and
-- expose that type from the module instead.

class Monad m => FMEMI                  m
class FMEMI m => ForeignMemoryInterface m where
  malloc :: Storable a => Int   ->             m (Ptr a)
  free   :: Storable a => Ptr a ->             m (     )
  peek   :: Storable a => Ptr a -> Int ->      m      a
  poke   :: Storable a => Ptr a -> Int -> a -> m (     )

--------------------------------------------------------------------------------

type Sortable a io =
  ( Storable a
  , Ord a
  , ForeignMemoryInterface io
  , ForeignThreadInterface io
  ) => [a] -> io [a]

type SortableTrust a =
  ( Storable a
  , Ord a
  ) => [a] -> [a]

type OffSet = Int
type Length = Int

--------------------------------------------------------------------------------

instance FMEMI                  IO
instance ForeignMemoryInterface IO where
  malloc = FFI.mallocBytes
  free   = FFI.free
  peek   = FFI.peekElemOff
  poke   = FFI.pokeElemOff

--------------------------------------------------------------------------------

sort :: Sortable a io
sort [       ] = pure []
sort xs@(hd:_) =
  malloc     m  >>= \ p  ->
  store  p 0 ys >>= \ _  -> -- initiate memory values to the list max value
  store  p 0 xs >>= \ _  ->
  sorter p o n  >>= \ _  ->
  query  p n    >>= \ zs ->
  free   p      >>= \ _  ->
  pure $ take l zs
  where
    l  = length xs
    n  = pow2 l -- Ensure that allocated memory is 2^i
    o  = FFI.sizeOf hd
    m  = n * o
    ys = take n $ cycle [ foldl1 max xs ]

--------------------------------------------------------------------------------

query
  ::
    ( Storable a
    , ForeignMemoryInterface io
    )
  => Ptr a
  -> Length
  -> io [a]
query p n =
  aux 0
  where
    aux i
      | i   <   n =
        aux (i+1) >>= \ tl ->
        peek p i  >>= \ hd ->
        pure $ hd:tl
      | otherwise = pure []

store
  ::
    ( Storable a
    , ForeignMemoryInterface io
    )
  => Ptr a
  -> OffSet
  -> [a]
  -> io ()
store _ _ [    ] = pure ()
store p i (x:xs) = poke p i x >> store p (i+1) xs

--------------------------------------------------------------------------------

pow2 :: Int -> Int
pow2 x =
  if x .&. (x - 1) == 0
  then x
  else 1 `shiftL` (b - z)
  where
    b = finiteBitSize     x
    z = countLeadingZeros x

next :: ( Storable a ) => Ptr a -> OffSet -> Ptr a
next p o =
  p `plusPtr` ( 1 * o) `asTypeOf` p

prev :: ( Storable a ) => Ptr a -> OffSet -> Ptr a
prev p o =
  p `plusPtr` (-1 * o) `asTypeOf` p

--------------------------------------------------------------------------------

sorter
  ::
    ( Storable a
    , Ord a
    , ForeignMemoryInterface io
    , ForeignThreadInterface io
    )
  => Ptr a
  -> OffSet
  -> Length
  -> io ()
sorter p o n =
  (
    if 2 < n
    then
      fork (sorter p o m) >>= \ f ->
      fork (sorter q o m) >>= \ s ->
      sync [ f, s ]
    else
      pure ()
  ) >>= \ _ ->
  merger p o n
  where
    m = n `shiftR`   1
    q = p `plusPtr` (m * o) `asTypeOf` p

merger
  ::
    ( Storable a
    , Ord a
    , ForeignMemoryInterface io
    , ForeignThreadInterface io
    )
  => Ptr a
  -> OffSet
  -> Length
  -> io ()
merger p o n =
  w p (prev l o) >>= \ _ ->
  if 2 < n
  then
    fork (bitonic p o m) >>= \ f ->
    fork (bitonic q o m) >>= \ s ->
    sync [ f, s ]
  else
    pure ()
  where
    m = n `shiftR`   1
    q = p `plusPtr` (m * o) `asTypeOf` p
    l = p `plusPtr` (n * o) `asTypeOf` p
    w i j
      | i   <   q =
          fork (comparator i j)          >>= \ f ->
          fork (w (next i o) (prev j o)) >>= \ s ->
          sync [ f, s ]
      | otherwise = pure ()

comparator
  ::
    ( Storable a
    , Ord a
    , ForeignMemoryInterface io
    )
  => Ptr a
  -> Ptr a
  -> io ()
comparator p q =
  peek p 0           >>= \ i ->
  peek q 0           >>= \ j ->
  poke p 0 (min i j) >>= \ _ ->
  poke q 0 (max i j) >>= \ _ ->
  pure ()

bitonic
  ::
    ( Storable a
    , Ord a
    , ForeignMemoryInterface io
    , ForeignThreadInterface io
    )
  => Ptr a
  -> OffSet
  -> Length
  -> io ()
bitonic p o n =
  cleaner p o n >>= \ _ ->
  if 2 < n
  then
    fork (bitonic p o m) >>= \ f ->
    fork (bitonic q o m) >>= \ s ->
    sync [ f, s ]
  else
    pure ()
  where
    m = n `shiftR`   1
    q = p `plusPtr` (m * o) `asTypeOf` p

cleaner
  ::
    ( Storable a
    , Ord a
    , ForeignMemoryInterface io
    , ForeignThreadInterface io
    )
  => Ptr a
  -> OffSet
  -> Length
  -> io ()
cleaner p o n =
  w p q
  where
    m = n `shiftR`   1
    q = p `plusPtr` (m * o) `asTypeOf` p
    w i j
      | i   <   q =
          fork (comparator i j)          >>= \ f ->
          fork (w (next i o) (next j o)) >>= \ s ->
          sync [ f, s ]
      | otherwise = pure ()
{% endhighlight %}


#### Main.hs

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

{-# LANGUAGE Trustworthy #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           System.IO.Unsafe
  ( unsafePerformIO
  )

import           Data.Bitonic
  ( SortableTrust
  , sort
  )

--------------------------------------------------------------------------------

-- In case you need the sorting algorithm to produce no IO effects, you will
-- have to create your own local version by implementing the `SortableTrust`
-- alias type with `unsafePerformIO`.

trust :: SortableTrust a
trust = unsafePerformIO . sort

--------------------------------------------------------------------------------

main :: IO ()
main =
  do
    putStrLn $ "# Bitonic sort"
    putStrLn $ "> xs (initial): " ++ (show  xs)
    sort xs >>= putStrLn . ("> ys (effects): " ++) . show
    putStrLn $ "> zs (trusted): " ++ (show $ trust xs)
      where
        xs = reverse [ 0 .. 15 ] :: [ Word ]
{% endhighlight %}


### Code Output:

{% highlight text %}
user@personal:~/../bitonic$ ./Main.hs
# Bitonic sort
> xs (initial): [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]
> ys (effects): [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
> zs (trusted): [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
user@personal:~/../bitonic$ 
{% endhighlight %}


### References:

* Wikipedia:
  - [Bitonic sorter][wiki]
* Blog: Ramón Soto Mathiesen:
  - [Bitonicsort in F#][blog]
* Haskell Hackage (base):
  - [Foreign.Marshal.Alloc][alloc]
  - [Foreign.Ptr][ptr]
  - [Foreign.Storable][storable]
  - [Data.Bits][bits]
  - [Control.Concurrent][concurrent]
  - [Control.Concurrent.MVar][mvar]
  - [GHC.Exception][exception]


[wiki]:       https://en.wikipedia.org/wiki/Bitonic_sorter
[blog]:       /articles/2013/09/17/bitonicsort-in-fsharp.html
[alloc]:      https://hackage.haskell.org/package/base/docs/Foreign-Marshal-Alloc.html
[ptr]:        https://hackage.haskell.org/package/base/docs/Foreign-Ptr.html
[storable]:   https://hackage.haskell.org/package/base/docs/Foreign-Storable.html
[bits]:       https://hackage.haskell.org/package/base/docs/Data-Bits.html
[concurrent]: https://hackage.haskell.org/package/base/docs/Control-Concurrent.html
[mvar]:       https://hackage.haskell.org/package/base/docs/Control-Concurrent-MVar.html
[exception]:  https://hackage.haskell.org/package/base/docs/Control-Exception.html
