--- 
layout: post
title: Bitonicsort in F# (revised)
categories:
  - English
tags:
  - snippet
  - bitonic
  - sort
  - bitonicsort 
  - f# 
  - fsharp
  - sequential
  - parallel
  - classic
  - arbitrary
  - nix
  - overlays
  - lts
  - dotnet
time: "11:13"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2020-04-20-bitonicsort-in-fsharp-revised_bitonic-sort.png" />
<figcaption>
Author Bitonic at English Wikipedia (CC0 1.0)
</figcaption>
</figure>
</div>

### Code Snippets

#### default.nix

{% highlight text %}
# Version by Robert (rycee) to point to a specific LTS version
{ pkgs ? import <nixpkgs> {
  overlays = [(
    self: super: {
      dotnet-sdk-lts = super.dotnet-sdk.overrideAttrs (
        old: rec {
          version = "3.1.102";
          src = super.fetchurl {
            url = "https://dotnetcli.azureedge.net/dotnet/Sdk/${version}/dotnet-sdk-${version}-linux-x64.tar.gz";
            sha256 = "e03ceeb5beaf7c228bd8dcbf7712cf12f5ccbfcd6d426afff78bfbd4524ff558";
          };
        }
      );
    }
  )];
}}:

with pkgs;

mkShell {
  buildInputs = [
    ncurses # for `clear`
    emacs
    dotnet-sdk-lts
  ];
}
{% endhighlight %}

{% highlight bash %}
[…@nixosT480:~/code/dotnet/bitonic/src]$ nix-shell --pure
[nix-shell:~/code/dotnet/bitonic/src]$
{% endhighlight %}


#### bitonic.fsproj

{% highlight xml %}
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.1</TargetFramework>
  </PropertyGroup>

  <PropertyGroup>
    <ServerGarbageCollection>true</ServerGarbageCollection>
  </PropertyGroup>
  
  <ItemGroup>
    <Compile Include="bitonic.fs" />
    <None Include="bitonic.sh">
      <CopyToOutputDirectory>Always</CopyToOutputDirectory>
    </None>
  </ItemGroup>

</Project>
{% endhighlight %}

#### runtimeconfig.template.json

{% highlight json %}
{
  "configProperties": {
    "System.Globalization.Invariant": true
  }
}
{% endhighlight %}

#### bitonic.fs

{% highlight ocaml linenos %}
(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

[<RequireQualifiedAccess>]
module Assert =
  
  let inline ensure condition msg f =
    if   condition
    then f ()
    else failwith msg

[<RequireQualifiedAccess>]
module Performance =
  
  open System.Diagnostics
  
  let private w = Stopwatch ()
  let start ( ) = w.Restart ()
  let stop  ( ) = w.Stop    (); w.Elapsed.TotalSeconds

[<RequireQualifiedAccess>]
module QualityAssurance =
  
  let inline issorted (a : 'a [] when 'a : comparison) : bool =
    let rec aux = function
      | 0 -> a.[0] <= a.[  1]
      | i -> a.[i] <= a.[i+1] && aux (i-1)
    match a with
      | [| |] -> true
      | [|_|] -> true
      | _____ -> aux (Array.length a - 2)

[<RequireQualifiedAccess>]
module BadImplementation =
  
  let inline isPow2 x =
    match x with
    | 0 -> false
    | _ -> x &&& (x - 1) = 0
  
  let comparator x y =
    match x with
    | _ when x < y -> (x,y)
    | _ -> (y,x)
  
  let halfCleaner bs =
    let n = bs |> Array.length
    let m = n/2
      
    match isPow2(n) with
    | true -> ()
    | false -> failwithf "Input array %A, must be n=2^k" bs
  
    Array.mapi(fun i x -> 
               match i with
               | _ when i < m -> fst (comparator x bs.[m+i])
               | _ -> snd (comparator x bs.[i-m])) bs
  
  let rec bitonicSorter bs =
    let n = bs |> Array.length
    let m = n/2
  
    match isPow2(n) with
    | true -> ()
    | false -> failwithf "Input array %A, must be n=2^k" bs
  
    let bs' = halfCleaner bs
    let bs1 = bs'.[0 .. (m - 1)]
    let bs2 = bs'.[m .. (n - 1)]
  
    match n with
    | _ when 2 < n ->
      Array.append (bitonicSorter bs1) (bitonicSorter bs2)
    | _ -> bs'
  
  let merger ss1 ss2 =
    let m1 = ss1 |> Array.length
    let m2 = ss2 |> Array.length
    let n = m1 + m2
    let m = n/2
  
    match (m1 = m2) with
    | true -> ()
    | false -> failwithf "Input arrays (%A,%A), must have the same length" ss1 ss2
      
    match isPow2(n) with
    | true -> ()
    | false -> failwithf "Comibnation of (%A,%A) arrays, must be n=2^k" ss1 ss2
  
    let ss2' = ss2 |> Array.rev
  
    let ss1'' = Array.map2(fun x y -> fst (comparator x y)) ss1 ss2'
    let ss2'' = Array.map2(fun x y -> snd (comparator x y)) ss1 ss2'
  
    match n with
    | _ when 2 < n -> Array.append (bitonicSorter ss1'') (bitonicSorter ss2'')
    | _ -> Array.append ss1'' ss2''
  
  let rec sorter array =
    let n = array |> Array.length
    let m = n/2
      
    match isPow2(n) with
    | true -> ()
    | false -> failwithf "Input array %A, must be n=2^k" array
  
    let as1 = array.[0 .. (m - 1)]
    let as2 = array.[m .. (n - 1)]
  
    match n with
    | _ when 2 < n -> merger (sorter as1) (sorter as2)
    | _ -> merger as1 as2  

[<RequireQualifiedAccess>]
module Array =
  
  [<RequireQualifiedAccess>]
  module Bitonic =
    
    open System.Threading.Tasks
    
    let inline subi index offset f (a : 'a []) : unit =
      let        n   = Array.length a
      let        i   = index
      let        j   = index + offset
      let        g   = OptimizedClosures.FSharpFunc<_,_,_>.Adapt(f)
      let inline h _ =
        Parallel.For (i, j, fun k -> g.Invoke(k, a.[k])) |> ignore
      let        m   =
        sprintf
          "Index %i: or index+offset: %i are out of bounds (n = %i)" i j n
      Assert.ensure (0 <= i && j <= n) m h

    let inline private sync xs = Array.Parallel.iter (fun f -> f ()) xs
    
    [<RequireQualifiedAccess>]
    module Classical =
      
      let inline ispow2 x =
        if   0 = x
        then false
        else x &&& (x-1) = 0
    
      let inline private comp (a : 'a [] when 'a : comparison) i j : unit =
        let x = a.[i]
        let y = a.[j]
        a.[i] <- min x y
        a.[j] <- max x y
      
      [<RequireQualifiedAccess>]
      module Sequential =
        
        let inline private halfcleaner a n i : unit =
          let m = n >>> 1
          for j in i .. 1 .. (i+m-1) do comp a j (m+j)
        
        let inline private sorter a n i : unit =
          let rec aux a n i =
            let m = n >>> 1
            halfcleaner a n i
            if  2 < n
            then
              aux a m  i    
              aux a m (i+m)
          aux a n i
        
        let inline private merger a n i : unit = 
          let m = n >>> 1
          let k = n  -  1 + 2 * i
          for j in i .. 1 .. (i+m-1) do comp a (j) (k-j)
          if  2 < n
          then
            sorter a m  i
            sorter a m (i+m)
        
        let inline sort inplace a : 'a [] when 'a : comparison =
          let rec aux a n i =
            let m = n >>> 1
            if  2 < n
            then
              aux  a m  i
              aux  a m (i+m)
            merger a n  i
          let b = if inplace then a else Array.map id a
          let n =                        Array.length b
          aux b n 0
          b
      
      [<RequireQualifiedAccess>]
      module Parallel =
        
        let inline private halfcleaner threadshold a n i : unit =
          let m = n >>> 1
          if  threadshold < m
          then
            subi     i           (m-1) (fun j _ -> comp a j (m+j)) a
          else
            for j in i .. 1 .. (i+m-1)  do         comp a j (m+j)
        
        let inline private sorter threadshold a n i : unit =
          let rec aux a n i =
            let m = n >>> 1
            halfcleaner threadshold a n i
            if  2 < n
            then
              if threadshold < n
              then
                sync
                  [| fun () -> aux a m  i
                  ;  fun () -> aux a m (i+m)
                  |] 
              else
                aux a m  i
                aux a m (i+m)
          aux a n i
        
        let inline private merger threadshold a n i : unit = 
          let m = n >>> 1
          let k = n  -  1 + 2 * i
          if  threadshold < m
          then
            subi     i            m   (fun j _ -> comp a j (k-j)) a
          else
            for j in i .. 1 .. (i+m-1) do         comp a j (k-j)
          if  2 < n
          then
            if threadshold < n
            then
              sync
                [| fun () -> sorter threadshold a m  i
                ;  fun () -> sorter threadshold a m (i+m)
                |]
            else
              sorter threadshold a m  i
              sorter threadshold a m (i+m)
        
        let inline sort threadshold inplace a : 'a [] when 'a : comparison =
          let rec aux a n i =
            let m = n >>> 1
            if  2 < n
            then
              if threadshold < n
              then
                sync
                  [| fun () -> aux a m  i
                  ;  fun () -> aux a m (i+m)
                  |]
              else
                aux            a m  i
                aux            a m (i+m)
            merger threadshold a n  i
          let b = if inplace then a else Array.Parallel.map id a
          let n =                        Array.length          b
          aux b n 0
          b
      
    (* Bitonic sorting network for n not a power of 2:
       https://www.inf.hs-flensburg.de/lang/algorithmen/sortieren/bitonic/oddn.htm
    *)
    [<RequireQualifiedAccess>]
    module Arbitrary =
      
      let inline log02b b n =
        let rec aux acc = function
          | 0 ->      acc
          | x -> aux (acc + 1) (x >>> b)
        aux 0 (n >>> b)
      
      let inline log2 n =
        log02b 1 n
    
      let inline private comp (a : 'a [] when 'a : comparison) i j d : unit =
        let x = a.[i]
        let y = a.[j]
        if  d = (x > y)
        then
          a.[i] <- y
          a.[j] <- x
      
      [<RequireQualifiedAccess>]
      module Sequential =
        
        let inline private merger a =
          let rec aux a n i d = 
            let m = (1 <<< (log2 n)) >>> 1
            let k = n - 1
            for j in i .. 1 .. (i+k-m) do comp a j (j+m) d
            if  2 < n
            then
              aux a    m   i    d
              aux a (n-m) (i+m) d
          aux a
        
        let inline sort inplace a : 'a [] when 'a : comparison =
          let rec aux a n i d =
            let m = n >>> 1
            if  2 < n
            then
              aux  a    m   i    (not d)
              aux  a (n-m) (i+m)      d
            merger a  n     i         d
          let b = if inplace then a else Array.map id a
          let n =                        Array.length b
          aux b n 0 true
          b
      
      [<RequireQualifiedAccess>]
      module Parallel =
        
        let inline private merger threadshold =
          let rec aux a n i d = 
            let m = (1 <<< (log2 n)) >>> 1
            let k = n - 1
            if  threadshold > 0 && threadshold < m
            then
              subi     i           (k-m) (fun j _ -> comp a j (j+m) d) a
            else
              for j in i .. 1 .. (i+k-m)  do         comp a j (j+m) d
            if  2 < n
            then
              if  threadshold < n
              then
                sync
                  [| fun () -> aux a    m   i    d
                  ;  fun () -> aux a (n-m) (i+m) d
                  |]
              else
                aux a    m   i    d
                aux a (n-m) (i+m) d
          aux
        
        let inline sort threadshold inplace a : 'a [] when 'a : comparison =
          let rec aux a n i d =
            let m = n >>> 1
            if  2 < n
            then
              if threadshold < n
              then
                sync
                  [| fun () -> aux  a    m   i    (not d)
                  ;  fun () -> aux  a (n-m) (i+m)      d 
                  |]
              else
                aux            a    m   i   (not d)
                aux            a (n-m) (i+m)     d 
            merger threadshold a  n     i        d  
          let b = if inplace then a else Array.Parallel.map id a
          let n =                        Array.length          b
          aux b n 0 true
          b

[<EntryPoint>]
let main argv =
  
  printfn "# Bitonicsort in F# (revised)"
  printfn ""
  let k = if 0 = Array.length argv then 20 else int argv.[0]
  let n = 1 <<< k // n = 2^k
  
  printfn "## CPU (cores, threads per core and threadshold size of array)"
  let cs = System.Environment.ProcessorCount
  (* Ex: lscpu | grep "Thread(s) per core:" -> Thread(s) per core: 2 *)
  let ts = 2
  let th = n / (cs * ts)
  printfn "* Cores: %i, threads/core: %i and threadshold/size: %i." cs ts th
  printfn ""
  
  printfn "## Init array (length = 2^%i): [| 0; 1; 0; 1; … |]." k
  Performance.start ()
  let a = Array.init n (fun i -> i % 2)
  let r = Performance.stop ()
  printfn "* Length: %i." (Array.length a)
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted a
  printfn ""
  
  printfn "## Init (parallel) array (length = 2^%i): [| 0; 1; 0; 1; … |]." k
  Performance.start ()
  let b = Array.Parallel.init n (fun i -> i % 2)
  let r = Performance.stop ()
  printfn "* Length: %i." (Array.length b)
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted b
  printfn ""
  
  printfn "## Array.map id array."
  Performance.start ()
  let c = Array.map id a
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted c
  printfn ""
  
  printfn "## Array.Parallel.map id array."
  Performance.start ()
  let d = Array.Parallel.map id a
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted d
  printfn ""
  
  if Array.Bitonic.Classical.ispow2 n (* Devs responsibility to test *)
  then
    printfn "## Classical Bitonic sort array with bad (buggy) version."
    Performance.start()
    let e = BadImplementation.sorter a
    let r = Performance.stop ()
    printfn "* Result: %f seconds." r
    printfn "* Sorted: %b." <| QualityAssurance.issorted e
    printfn ""
    
    printfn "## Classical Bitonic sort array with sequential version."
    Performance.start()
    let f = Array.Bitonic.Classical.Sequential.sort false a
    let r = Performance.stop ()
    printfn "* Result: %f seconds." r
    printfn "* Sorted: %b." <| QualityAssurance.issorted f
    printfn ""
    
    printfn "## Classical Bitonic sort array with sequential in-place version."
    let g = Array.init n (fun i -> i % 2)
    printfn "* Sorted: %b." <| QualityAssurance.issorted g
    Performance.start()
    let h = Array.Bitonic.Classical.Sequential.sort true  g
    let r = Performance.stop ()
    printfn "* Result: %f seconds." r
    printfn "* Sorted: %b." <| QualityAssurance.issorted h
    printfn ""
    
    printfn "## Classical Bitonic sort array with parallel version."
    Performance.start()
    let i = Array.Bitonic.Classical.Parallel.sort th false a
    let r = Performance.stop ()
    printfn "* Result: %f seconds." r
    printfn "* Sorted: %b." <| QualityAssurance.issorted i
    printfn ""
    
    printfn "## Classical Bitonic sort array with parallel in-place version."
    let j = Array.init n (fun i -> i % 2)
    printfn "* Sorted: %b." <| QualityAssurance.issorted j
    Performance.start()
    let l = Array.Bitonic.Classical.Parallel.sort th true  j
    let r = Performance.stop ()
    printfn "* Result: %f seconds." r
    printfn "* Sorted: %b." <| QualityAssurance.issorted l
    printfn ""
  else
    printfn "Input array %i, must be n=2^k" n
    System.Environment.Exit(1)
  
  printfn "## Arbitrary Bitonic sort array with sequential version."
  Performance.start()
  let f = Array.Bitonic.Arbitrary.Sequential.sort false a
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted f
  printfn ""
  
  printfn "## Arbitrary Bitonic sort array with sequential in-place version."
  let g = Array.init (n+1) (fun i -> i % 2)
  printfn "* Length: %i." (Array.length g)
  printfn "* Sorted: %b." <| QualityAssurance.issorted g
  Performance.start()
  let h = Array.Bitonic.Arbitrary.Sequential.sort true  g
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted h
  printfn "> Remark: Array length is odd (2^%i + 1)" k
  printfn ""
  
  printfn "## Arbitrary Bitonic sort array with parallel version."
  Performance.start()
  let i = Array.Bitonic.Arbitrary.Parallel.sort th false a
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted i
  printfn ""
  
  printfn "## Arbitrary Bitonic sort array with parallel in-place version."
  let j = Array.init (n+1) (fun i -> i % 2)
  printfn "* Length: %i." (Array.length j)
  printfn "* Sorted: %b." <| QualityAssurance.issorted j
  Performance.start()
  let l = Array.Bitonic.Arbitrary.Parallel.sort th true  j
  let r = Performance.stop ()
  printfn "* Result: %f seconds." r
  printfn "* Sorted: %b." <| QualityAssurance.issorted l
  printfn "> Remark: Array length is odd (2^%i + 1)" k
  printfn ""
    
  System.Environment.Exit(0)
  
  0
{% endhighlight %}

#### build.sh

{% highlight bash %}
#!/bin/sh

# Clean screen
clear

# Clean previous build (if any)
dotnet clean --configuration Release

# Build in Release mode
dotnet build --configuration Release

# References
# ==========
# 
# - `dotnet clean`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-clean
# - `dotnet publish` > Options > `--configuration`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish#options
{% endhighlight %}

#### stand.sh

{% highlight bash %}
#!/bin/sh

net=netcoreapp3.1
run=linux-x64
pub="$(pwd)/pub/"

# Clean screen
clear

# Clean previous build (if any)
dotnet clean --configuration Release

# Remove previous publish folder (if any)
if [ -d $pub ]; then rm -rf $pub; fi

# Publish in Release mode (self-contained)
dotnet publish \
       --configuration Release \
       --force \
       --self-contained \
       --framework $net \
       --runtime   $run \
       --output    $pub

# References
# ==========
# 
# - `dotnet clean`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-clean
# - `dotnet publish` > Options > `--configuration`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish#options
# - `dotnet publish` > Options > `--force` `--self-contained` / `--framework` / `--runtime`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish#options
# - .NET Core RID Catalog > Linux RIDs
# https://docs.microsoft.com/en-us/dotnet/core/rid-catalog#linux-rids
# - Target frameworks > Supported target framework versions
# https://docs.microsoft.com/en-us/dotnet/standard/frameworks
{% endhighlight %}

#### bitonic.sh

{% highlight bash %}
#!/bin/sh

log=$(pwd)/log

iso8601=$(date +"%Y%m%dT%H%M%S")

# Clean screen
clear

# Ensure log folder exists
mkdir -p "$log"

# Execute and split output to stdout and stderr
dotnet bitonic.dll $1 2> "$log/$iso8601.log"

# References
# ==========
# 
# - `dotnet run` > Options > `--configuration`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-run
#
# - Redirecting Error Messages from Command Prompt: STDERR/STDOUT
# https://support.microsoft.com/en-us/help/110930/redirecting-error-messages-from-command-prompt-stderr-stdout
{% endhighlight %}


### Code Output:

{% highlight bash %}
[nix-shell:~/code/dotnet/bitonic/src/pub]$ ./bitonic.sh 
# Bitonicsort in F# (revised)

## CPU (cores, threads per core and threadshold size of array)
* Cores: 8, threads/core: 2 and threadshold/size: 65536.

## Init array (length = 2^20): [| 0; 1; 0; 1; … |].
* Length: 1048576.
* Result: 0.002216 seconds.
* Sorted: false.

## Init (parallel) array (length = 2^20): [| 0; 1; 0; 1; … |].
* Length: 1048576.
* Result: 0.009313 seconds.
* Sorted: false.

## Array.map id array.
* Result: 0.002644 seconds.
* Sorted: false.

## Array.Parallel.map id array.
* Result: 0.003007 seconds.
* Sorted: false.

## Classical Bitonic sort array with bad (buggy) version.
* Result: 11.758988 seconds.
* Sorted: true.

## Classical Bitonic sort array with sequential version.
* Result: 8.277300 seconds.
* Sorted: true.

## Classical Bitonic sort array with sequential in-place version.
* Sorted: false.
* Result: 8.661613 seconds.
* Sorted: true.

## Classical Bitonic sort array with parallel version.
* Result: 3.149078 seconds.
* Sorted: true.

## Classical Bitonic sort array with parallel in-place version.
* Sorted: false.
* Result: 3.567784 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with sequential version.
* Result: 5.196947 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with sequential in-place version.
* Length: 1048577.
* Sorted: false.
* Result: 4.935209 seconds.
* Sorted: true.
> Remark: Array length is odd (2^20 + 1)

## Arbitrary Bitonic sort array with parallel version.
* Result: 1.614365 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with parallel in-place version.
* Length: 1048577.
* Sorted: false.
* Result: 1.704201 seconds.
* Sorted: true.
> Remark: Array length is odd (2^20 + 1)

[nix-shell:~/code/dotnet/bitonic/src/pub]$ ./bitonic.sh 21
# Bitonicsort in F# (revised)

## CPU (cores, threads per core and threadshold size of array)
* Cores: 8, threads/core: 2 and threadshold/size: 131072.

## Init array (length = 2^21): [| 0; 1; 0; 1; … |].
* Length: 2097152.
* Result: 0.004351 seconds.
* Sorted: false.

## Init (parallel) array (length = 2^21): [| 0; 1; 0; 1; … |].
* Length: 2097152.
* Result: 0.010732 seconds.
* Sorted: false.

## Array.map id array.
* Result: 0.005175 seconds.
* Sorted: false.

## Array.Parallel.map id array.
* Result: 0.005399 seconds.
* Sorted: false.

## Classical Bitonic sort array with bad (buggy) version.
* Result: 29.816791 seconds.
* Sorted: true.

## Classical Bitonic sort array with sequential version.
* Result: 21.015821 seconds.
* Sorted: true.

## Classical Bitonic sort array with sequential in-place version.
* Sorted: false.
* Result: 20.716299 seconds.
* Sorted: true.

## Classical Bitonic sort array with parallel version.
* Result: 6.554269 seconds.
* Sorted: true.

## Classical Bitonic sort array with parallel in-place version.
* Sorted: false.
* Result: 6.552851 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with sequential version.
* Result: 11.454322 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with sequential in-place version.
* Length: 2097153.
* Sorted: false.
* Result: 11.482118 seconds.
* Sorted: true.
> Remark: Array length is odd (2^21 + 1)

## Arbitrary Bitonic sort array with parallel version.
* Result: 3.711691 seconds.
* Sorted: true.

## Arbitrary Bitonic sort array with parallel in-place version.
* Length: 2097153.
* Sorted: false.
* Result: 3.940170 seconds.
* Sorted: true.
> Remark: Array length is odd (2^21 + 1)
{% endhighlight %}


### References:

* Wikipedia:
  - [Bitonic sorter][wiki]
* Blog: Mr. Mathiesen:
  - [Bitonicsort in F#][blog]
* Hochschule Flensburg, University of Applied Sciences:
  - [Bitonic sorting network for n not a power of 2][oddi]
* Microsoft:
  - [How to: Write a Simple Parallel.For Loop][loop]
* GitHub:
  - [referencesource/mscorlib/system/threading/Tasks/Parallel.cs][parf]
  - [fsharp/src/fsharp/FSharp.Core/array.fs][iter]

[wiki]: https://en.wikipedia.org/wiki/Bitonic_sorter
[blog]: /articles/2013/09/17/bitonicsort-in-fsharp.html
[oddi]: https://www.inf.hs-flensburg.de/lang/algorithmen/sortieren/bitonic/oddn.htm
[loop]: https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/how-to-write-a-simple-parallel-for-loop
[parf]: https://github.com/microsoft/referencesource/blob/4251daa76e0aae7330139978648fc04f5c7b8ccb/mscorlib/system/threading/Tasks/Parallel.cs#L430-L441
[iter]: https://github.com/fsharp/fsharp/blob/3bc41f9e10f9abbdc1216e984a98e91aad351cff/src/fsharp/FSharp.Core/array.fs#L1338-L1347
