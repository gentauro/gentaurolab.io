---
layout: post
title: Hacking on PST File Format SDK
categories:
  - English
tags: 
  - pst
  - pst sdk
  - cpp
  - open source
time: "14:03"
---

### PST File Format SDK

Why did I tried out this framework [PST File Format
SDK](http://pstsdk.codeplex.com/)? I recently changed jobs so I wanted to make
an automated application that would retrieve some e-mails from a special folder
from inside my .PST file and convert the single e-mails + attachments to blog
posts.

I found out that this library is pretty much broken so I stopped working with it
but if somebody else wants to build it on their Mac OS X, here is a small
description on how to do it:

#### Error when compiling on Max OS X 10.8.2

{% highlight bash %}
[ mon@mba-2 pstsdk_0_3_0 ] CC=gcc-mp-4.4 CXX=g++-mp-4.4 cmake .
[ mon@mba-2 pstsdk_0_3_0 ] make 
Linking CXX executable pstsdk_test
Undefined symbols for architecture x86_64:
  "_libiconv", referenced from:
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in highlevel.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in highlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in pstlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in utiltest.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in utiltest.cpp.o
  "_libiconv_close", referenced from:
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in highlevel.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in highlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in pstlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in utiltest.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in utiltest.cpp.o
  "_libiconv_open", referenced from:
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in highlevel.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in highlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in pstlevel.cpp.o
      pstsdk::bytes_to_wstring(std::vector<unsigned char, std::allocator<unsigned char> > const&) in utiltest.cpp.o
      pstsdk::wstring_to_bytes(std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > const&) in utiltest.cpp.o
ld: symbol(s) not found for architecture x86_64
collect2: ld returned 1 exit status
make[2]: *** [test/pstsdk_test] Error 1
make[1]: *** [test/CMakeFiles/pstsdk_test.dir/all] Error 2
make: *** [all] Error 2
[ mon@mba-2 pstsdk_0_3_0 ] 
{% endhighlight %}

#### How to solve error

You need to ensure that the correct `iconv` is used,
`/opt/local/lib/libiconv.dylib` and not `/usr/lib/libiconv.dylib`. This can be
done by adding a **hint path** to the `find_library` function in
**CMakeLists.txt**:

{% highlight bash %}
# Use iconv if we have it.  This is required on non-Win32 platforms,
# because we don't necessarily know the encoding of wchar_t.
find_library(ICONV_LIBRARY
  NAMES iconv
  HINTS "/opt/local/lib/")
{% endhighlight %}

Once you have cleared out all the CMake files you can run once again:

{% highlight bash %}
[ mon@mba-2 pstsdk_0_3_0 ] CC=gcc-mp-4.4 CXX=g++-mp-4.4 cmake .
[ mon@mba-2 pstsdk_0_3_0 ] make
Scanning dependencies of target pstsdk_test
[ 14%] Building CXX object test/CMakeFiles/pstsdk_test.dir/btreetest.cpp.o
[ 28%] Building CXX object test/CMakeFiles/pstsdk_test.dir/databasetest.cpp.o
[ 42%] Building CXX object test/CMakeFiles/pstsdk_test.dir/disktest.cpp.o
[ 57%] Building CXX object test/CMakeFiles/pstsdk_test.dir/highlevel.cpp.o
[ 71%] Building CXX object test/CMakeFiles/pstsdk_test.dir/main.cpp.o
[ 85%] Building CXX object test/CMakeFiles/pstsdk_test.dir/pstlevel.cpp.o
[100%] Building CXX object test/CMakeFiles/pstsdk_test.dir/utiltest.cpp.o
Linking CXX executable pstsdk_test
[100%] Built target pstsdk_test
[ mon@mba-2 pstsdk_0_3_0 ] 
{% endhighlight %}

### Alternative: pstsdk.net: .NET port of PST File Format SDK

It looks like the author is also working on a .NET port of his MC++ version:

[pstsdk.net: .NET port of PST File Format
SDK](http://http://pstsdknet.codeplex.com/)

Remark: This version requires that the MC++ PST SDK is built on your local
computer so ...
