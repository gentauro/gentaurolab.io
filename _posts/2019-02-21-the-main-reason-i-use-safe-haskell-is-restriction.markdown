--- 
layout: post
title: The main reason I use Safe Haskell, is “restriction”
categories:
  - English
tags:
  -main
  -reason
  -safe
  -haskell
  -restriction
time: "12:35"
---

The main reason I try to code in `Haskell` with the `{-# LANGUAGE Safe #-}`
pragma turned on:

{% highlight text %}
07:45 I really miss the experimentation and the
07:49 ideas that come about from being
07:51 restricted I often find that it's
07:54 something that is true in my own
07:55 creative process as well when I restrict
07:58 myself in some arbitrary way I end up
08:01 having to come up with creative results
08:03 you know creative solutions to make it
08:06 something that's actually interesting as
08:08 opposed to just you know all you have
08:09 all the unlimited resources in the world
08:11 you can just come up with anything if
08:13 you can't do that there's something that
08:15 just triggers in the creative process
{% endhighlight %}

<div align="center">
<figure>
<img
src="/assets/img/posts/2019-02-21-the-main-reason-i-use-safe-haskell-restriction_8-bit-guy.png" />
<figcaption>
Lazy Game Reviews (LGR) at The 8-Bit Guys video (for source, see References below)
</figcaption>
</figure>
</div>

### Quote of the day (Simon Marlow):

> “For typical Haskell programmers, using `{-# LANGUAGE Safe #-}` will be like
> `-Wall`: something that is considered good practice from a hygiene point of
> view. If you don't need access to unsafe features, then it's better to write
> in the safe subset, where you have stronger guarantees. Just like `-Wall`, you
> get to choose whether to use `{-# LANGUAGE Safe #-}` or not.”


### References:

* Wiki Haskell:
  - [Safe Haskell][haskell]
* The 8-Bit Guy:
  - [Building my dream computer - Part 1 (starts at 7:45)][youtube]

[haskell]: https://wiki.haskell.org/Safe_Haskell
[youtube]: https://www.youtube.com/watch?v=ayh0qebfD2g&t=465
