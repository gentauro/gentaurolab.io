--- 
layout: post
title: Haskell - Immutable arrays built on top of Integers
categories:
  - English
tags:
  - snippet
  - immutable 
  - array
  - integer
time: "18:44"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2018-07-28-haskell-immutable-arrays-built-on-top-of-integers_iarray-strict-65000-32bit-elems.png" />
<figcaption>
65536 Word32 elements, at least 262144 bytes, stored in an IArray (Integer).
</figcaption>
</figure>
</div>

### Code Snippet

#### Data/IArray.hs

{% highlight haskell linenos %}
{-# LANGUAGE Safe   #-}
{-# LANGUAGE Strict #-}

--------------------------------------------------------------------------------

module Data.IArray
  ( IArray
  , add
  , drop
  , empty
  , head
  , idx
  , init
  , size
  , tail
  , toList
  ) where

--------------------------------------------------------------------------------

import           Prelude    hiding
    ( drop
    , head
    , init
    , tail
    )

import           Data.Bits
    ( Bits
    , FiniteBits
    , bitSizeMaybe
    , shiftL
    , shiftR
    , testBit
    , (.|.)
    )
import           Data.Maybe
    ( catMaybes
    , fromMaybe
    )

--------------------------------------------------------------------------------

type Container = Integer
type Length    = Integer
type BitSize   = Int
data IArray a  = IArray Container Length BitSize

--------------------------------------------------------------------------------

instance Show (IArray a) where
  show (IArray _ l s) =
    "IArray (length: " ++ show l ++ ", bit size: " ++ show s ++ ")"

--------------------------------------------------------------------------------

(.<.)
  :: Bits a
  => a
  -> Int
  -> a
(.>.)
  :: Bits a
  => a
  -> Int
  -> a

empty
  :: (Integral a, Bits a, FiniteBits a)
  => IArray a

init
  :: (Integral a, Bits a, FiniteBits a)
  => a
  -> IArray a

head
  :: (Integral a, Bits a, FiniteBits a)
  => IArray a
  -> Maybe a

tail
  :: (Integral a, Bits a, FiniteBits a)
  => IArray a
  -> Maybe (IArray a)

drop
  :: (Integral a, Bits a, FiniteBits a)
  => Int
  -> IArray a
  -> Maybe (IArray a)

size
  :: (Integral a, Bits a, FiniteBits a)
  => IArray a
  -> Integer

idx
  :: (Integral a, Bits a, FiniteBits a)
  => Int
  -> IArray a
  -> Maybe a

add
  :: (Integral a, Bits a, FiniteBits a)
  => a
  -> IArray a
  -> IArray a

toList
  :: (Integral a, Bits a, FiniteBits a)
  => IArray a
  -> [ a ]

--------------------------------------------------------------------------------

(.<.) x y = x `shiftL` y
(.>.) x y = x `shiftR` y

-- O(1)
empty =
  IArray 0 0 0

-- O(1)
init x =
  IArray (fromIntegral x) 1 (fromMaybe 0 $ bitSizeMaybe x)

-- O(1)
head ia@(IArray _ l _) =
  if l == 0
  then Nothing
  else idx 0 ia

-- O(n) as we need to re-create the underlaying Integer container
tail (IArray c l s) =
  if l == 0
  then Nothing
  else Just $ IArray (c .>. s) (l - 1) s

-- O(n) as we need to re-create the underlaying Integer container
drop n (IArray c l s) =
  if 0 <= n' && n' < l
  then Just $ IArray (c .>. (n * s)) (l - n') s
  else Nothing
  where
    n' = fromIntegral n

-- O(1)
size (IArray _ l _) =
  l

-- O(1) based on the assumption that bit lookups on Integers are also O(1)
idx n (IArray c l s) =
  if 0 <= n && fromIntegral n < l
  then Just $ aux s
  else Nothing
  where
    -- 1 .<. 3 + 0 .<. 2 + 1 .<. 1 + 0 .<. 0 (bin) equals 10 (dec)
    aux 0 =  bti $ testBit c (n * s    )
    aux i = (bti $ testBit c (n * s + i)) .<. i + aux (i - 1)
    bti False = 0
    bti True  = 1

-- O(n) as we need to re-create the underlaying Integer container
add x (IArray c l s) =
  IArray (fromIntegral x .<. n .|. c) (l + 1) b
  where
    n = fromIntegral l * b
    b =
      if 0 == s
      then fromMaybe 0 $ bitSizeMaybe x
      else s

-- O(n) based on the assumption that bit lookups on Integers are also O(1)
toList ia@(IArray _ l _) =
  catMaybes $ aux $ fromInteger l
  where
    aux 0 = [ idx 0 ia ]
    aux n =   idx n ia : aux (n - 1)
{% endhighlight %}

#### Main.hs

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Prelude     hiding
    ( drop
    , head
    , init
    , tail
    )

import           Data.Bits
    ( Bits
    , shiftL
    )
import           Data.Word
    ( Word32
    , Word8
    )

import           Data.IArray
    ( IArray
    , add
    , drop
    , empty
    , head
    , idx
    , init
    , tail
    , toList
    )

--------------------------------------------------------------------------------

main
  :: IO ()

--------------------------------------------------------------------------------

(.<.) :: Bits a => a -> Int -> a
(.<.) x y = x `shiftL` y

foo :: IArray Word8
foo =
  aux 255
  where
    aux 0 = add 0 $ empty
    aux n = add n $ aux (n - 1)

bar :: Maybe Word8
bar =
  tail foo >>= tail >>= tail >>= drop 16 >>= head

baz :: IArray Word32
baz =
  aux $ 1 .<. 16 - 1
  where
    aux 0 = init 0
    aux n = add  n $ aux (n - 1)

qux :: Maybe Word32
qux =
  idx (1 .<. 15 - 1) baz

main =
  do
    putStrLn $ show $ foo
    putStrLn $ show $ toList $ foo
    putStrLn $ show $ bar
    putStrLn $ show $ baz
    putStrLn $ show $ toList $ baz
    putStrLn $ show $ qux
{% endhighlight %}

#### build.bash

{% highlight bash %}
#!/bin/bash

clear

# iarray
ghc -Wall -Werror -O2 --make Main.hs -o iarray

# clean
find . -name '*.hi' -delete
find . -name '*.o'  -delete
{% endhighlight %}

### Code Output:

{% highlight text %}
user@personal:~/.../iarray$ ./iarray 
IArray (length: 256, bit size: 8)
[ 255,254,253,252,251,250,249,248,247,246,245,244,243,242,241,240,239,238, ...
... ,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0 ]
Just 19
IArray (length: 65536, bit size: 32)
[ 65535,65534,65533,65532,65531,65530,65529,65528,65527,65526,65525,65524, ...
... ,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0 ]
Just 32767
{% endhighlight %}


### References:

* Haskell Wiki:
  - [Performance/Integers][performance]
* Wilfred Hughes::Blog:
  - [The Fastest BigInt In The West][fastest]

[performance]: https://wiki.haskell.org/Performance/Integers
[fastest]: http://www.wilfred.me.uk/blog/2014/10/20/the-fastest-bigint-in-the-west/
