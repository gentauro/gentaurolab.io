---
layout: post
title: F# - FSharp is surprisingly expressive
categories:
  - English
tags:
  - snippet
  - f#
time: "22:02"
---

### Code Snippet:

{% highlight ocaml linenos %}
open System

type System.Int32 with
  member x.Months = DateTime.Today.AddMonths(x) - DateTime.Today
  member x.Years = DateTime.Today.AddYears(x) - DateTime.Today

type System.Double with
  member x.Days = DateTime.Today.AddDays(x) - DateTime.Today

type System.TimeSpan with
  member x.Ago = DateTime.Now.Add(-x)
  member x.FromNow = DateTime.Now.Add(x)
  member x.FromToday = DateTime.Today.Add(x)

let foo = DateTime.Now - 20 .Years - 0.5 .Days
let bar = 50 .Years.Ago
{% endhighlight %}

### Code output:

{% highlight text %}
type Int32 with
  member Months : System.TimeSpan
type Int32 with
  member Years : System.TimeSpan
type Double with
  member Days : System.TimeSpan
type TimeSpan with
  member Ago : System.DateTime
type TimeSpan with
  member FromNow : System.DateTime
type TimeSpan with
  member FromToday : System.DateTime
> val foo : System.DateTime = 03/06/1994 10:26:59
> val bar : System.DateTime = 03/05/1964 22:28:15
{% endhighlight %}

### References:

* Inspired by STEVE KLABNIK: [Rust is surprisingly
  expressive](http://words.steveklabnik.com/rust-is-surprisingly-expressive)