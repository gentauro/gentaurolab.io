--- 
layout: post
title: Haskell - Safe min-max heap only depending on Prelude
categories:
  - English
tags:
  - snippet
  - haskell
  - safe
  - min
  - max
  - heap
  - pairing
  - prelude
time: "12:59"
---

<div align="center">
<figure>
<img
src="/assets/img/posts/2020-07-12-haskell-safe-min-max-heaps-only-depending-on-prelude_min-max-heap.jpg" />
<figcaption>
Example of Min-max heap, author(s) Aishvora/HerrAdams at Wikipedia (CC BY-SA 3.0)
</figcaption>
</figure>
</div>


### Code Snippet

{% highlight haskell linenos %}
{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Pairing
  ( MinHeap
  , MaxHeap
    --
  , minroot
  , mincons
  , mindrop
  , minheap
  , minlist
    --
  , maxroot
  , maxcons
  , maxdrop
  , maxheap
  , maxlist
  )
where

--------------------------------------------------------------------------------

import           Prelude    hiding
  ( drop
  )

import           Data.List
  ( foldl'
  )
import           Data.Maybe
  ( fromMaybe
  )

--------------------------------------------------------------------------------

data Tree a
  = Leaf
  | Node a [Tree a]

type Heap = Tree

newtype MinHeap a = I (Heap a)
newtype MaxHeap a = D (Heap a)

--------------------------------------------------------------------------------

minroot
  :: MinHeap a
  -> Maybe   a
minroot (I h) =
  root h

mincons
  :: Ord     a
  =>         a
  -> MinHeap a
  -> MinHeap a
mincons x (I h) =
  I $ cons LT x h

mindrop
  ::        Ord     a
  =>        MinHeap a
  -> Maybe (MinHeap a)
mindrop (I h) =
  I <$> drop LT h

minheap
  :: Ord     a
  =>        [a]
  -> MinHeap a
minheap =
  I . heap LT

minlist
  :: Ord     a
  => MinHeap a
  ->        [a]
minlist (I h) =
  list LT h

--------------------------------------------------------------------------------

maxroot
  :: MaxHeap a
  -> Maybe   a
maxroot (D h) =
  root h

maxcons
  :: Ord     a
  =>         a
  -> MaxHeap a
  -> MaxHeap a
maxcons x (D h) =
  D $ cons GT x h

maxdrop
  ::        Ord     a
  =>        MaxHeap a
  -> Maybe (MaxHeap a)
maxdrop (D h) =
  D <$> drop GT h

maxheap
  :: Ord     a
  =>        [a]
  -> MaxHeap a
maxheap =
  D . heap GT

maxlist
  :: Ord     a
  => MaxHeap a
  ->        [a]
maxlist (D h) =
  list GT h

--------------------------------------------------------------------------------

-- HELPERS

--------------------------------------------------------------------------------

-- Pairing heap
--
-- https://en.wikipedia.org/wiki/Pairing_heap

root
  :: Heap  a
  -> Maybe a
root  Leaf       = Nothing
root (Node a _ ) = Just  a

cons
  :: Ord  a
  => Ordering
  ->      a
  -> Heap a
  -> Heap a
cons o a =
  meld o n
  where
    n = Node a []

drop
  ::        Ord  a
  =>        Ordering
  ->        Heap a
  -> Maybe (Heap a)
drop _  Leaf        = Nothing
drop o (Node _ hs ) = Just ph
  where
    ph = pair o hs

--------------------------------------------------------------------------------

heap
  :: Ord  a
  => Ordering
  ->     [a]
  -> Heap a
heap o =
  foldl' f Leaf
  where
    f = flip $ cons o

list
  :: Ord  a
  => Ordering
  -> Heap a
  ->     [a]
list o =
  fromMaybe [] . app
  where
    app h =
      aux
      <$> root   h
      <*> drop o h
    aux a hs =
      a : list o hs

--------------------------------------------------------------------------------

meld
  :: Ord  a
  => Ordering
  -> Heap a
  -> Heap a
  -> Heap a
meld _     Leaf           node       = node
meld _     node           Leaf       = node
meld o t1@(Node x xs) t2@(Node y ys) =
  if o == compare x y
  then Node x (t2 : xs)
  else Node y (t1 : ys)

pair
  ::  Ord  a
  => Ordering
  -> [Heap a]
  ->  Heap a
pair _ [ ]      = Leaf
pair _ [x]      = x
pair o (x:y:hs) =
  meld o t r
  where
    t = meld o x y
    r = pair o hs
{% endhighlight %}


### References:

* Wikipedia:
  - [Min-max heap][heap]
  - [Pairing heap][pair]

[heap]: https://en.wikipedia.org/wiki/Min-max_heap
[pair]: https://en.wikipedia.org/wiki/Pairing_heap
