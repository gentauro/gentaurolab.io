---
layout: post
title: Software presentation of Arkivarius
categories: 
  - English
tags:
  - arkivarius
  - presentations
time: "21:06"
---

### Overview (I wished I still had access to the source code ...)

<img src="/assets/img/posts/2007-08-11-software-presentation-arkivarius_arkivarius_overview.png" alt="Arkivarius">

### Part I

<object 
  width="425" 
  height="350" 
  data="http://www.youtube.com/v/6HYqQsthRRo" 
  type="application/x-shockwave-flash">
  <param name="src" value="http://www.youtube.com/v/6HYqQsthRRo" />
</object>

### Part II

<object 
  width="425" 
  height="350" 
  data="http://www.youtube.com/v/rACDGfhiI2I" 
  type="application/x-shockwave-flash">
  <param name="src" value="http://www.youtube.com/v/rACDGfhiI2I" />
</object>
