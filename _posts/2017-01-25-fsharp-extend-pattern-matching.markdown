--- 
layout: post
title: F# - Extend Pattern Matching
categories:
  - English
tags:
  - snippet
  - f#
time: "09:07"
---
### Code Snippet

{% highlight ocaml linenos %}
module Standard =
  let (@@) : ('a -> 'b option) -> ('a -> 'b option) -> 'a -> 'b option =
    fun f g x ->
      match f x with
        | None -> g x
        | some -> some
  
  let standard : string -> string option = function
    | ".txt" -> Some "text/plain"
    | ______ -> None
  
  type config =
    { mimeTypes : string -> string option }
    
  let patternMatch : string -> config -> string option =
    fun x { config.mimeTypes = f } ->
      f x
  
  let config = { mimeTypes = standard }

module Custom =
  open Standard
  
  let extended : string -> string option = function
    | ".avi" -> Some "video/avi"
    | ______ -> None
  
  let custom = standard @@ extended
  
  let config = { config with mimeTypes = custom }

Standard . config |> Standard . patternMatch ".avi"
Custom   . config |> Standard . patternMatch ".avi"
{% endhighlight %}

### Code output:

{% highlight text %}
> 
module Standard = begin
  val ( @@ ) : f:('a -> 'b option) -> g:('a -> 'b option) -> x:'a -> 'b option
  val standard : _arg1:string -> string option
  type config =
    {mimeTypes: string -> string option;}
  val patternMatch : x:string -> config -> string option
  val config : config = {mimeTypes = <fun:config@19>;}
end

> 
module Custom = begin
  val extended : _arg1:string -> string option
  val custom : (string -> string option)
  val config : Standard.config = {mimeTypes = <fun:custom@28-2>;}
end

> val it : string option = None
> val it : string option = Some "video/avi"
{% endhighlight %}

### References:

* Suave.IO:
  - [defaultMimeTypesMap](https://suave.io/files.html)
* Suave.IO @ GitHub:
  - [operator](https://github.com/SuaveIO/suave/blob/master/src/Suave/Operators.fs#L12-L13)
  - [concatenate](https://github.com/SuaveIO/suave/blob/master/src/Suave/WebPart.fs#L77-L80)
