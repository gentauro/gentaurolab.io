---
layout: post
title: F# - Penney's game (Coin flipping/tossing)
categories:
  - English
tags:
  - snippet
  - f#
time: "11:10"
---

### Code Snippet:

{% highlight ocaml linenos %}
type CoinFace = Head | Tail
type Coin = CoinFace * CoinFace
type CoinChoice = CoinFace * CoinFace * CoinFace
type Player = {Name:string; Choice:CoinChoice}
type TossCoin = Coin -> CoinFace
type CheatChoice = CoinChoice -> CoinChoice
type Game = Coin * Player * Player -> Player

let rand = System.Random()

let randChoice () =
  let rec randChoice' = function
    | 0 -> Head
    | _ -> Tail
  randChoice' (rand.Next() % 2)

let oppFace = function
  | Head -> Tail
  | Tail -> Head

let toss : TossCoin =
  fun (coin : Coin) ->
    match rand.Next() % 2 with
      | 0 -> coin |> fst
      | _ -> coin |> snd

let cheatChoice : CheatChoice =
  fun (coinChoice : CoinChoice) ->
    let x,y,z = coinChoice
    (z |> oppFace),x,y

let game : Game =
  fun (coin : Coin, p1 : Player, p2 : Player) ->
    let rec game' = function
      | z::y::x::[] ->
        printfn "Toss: %A, p1: %A, p2: %A" (x,y,z) p1.Choice p2.Choice
        match x,y,z with
          | _ when (x,y,z) = p1.Choice -> p1
          | _ when (x,y,z) = p2.Choice -> p2
          | _ -> (coin |> toss)::z::y::[] |> game'
      | xs -> (coin |> toss)::xs |> game'
    (coin |> toss)::[] |> game'

let simulation n =
  let coin = Head,Tail
  
  let rec simulation' acc = function
    | 0 -> acc
    | i ->
      let p1 = {Name = "A"; Choice = randChoice(),randChoice(),randChoice()}
      let p2 = {Name = "B"; Choice = p1.Choice |> cheatChoice}
      let winner = (coin,p1,p2) |> game |> fun x -> x.Name
      simulation' (winner::acc) (i-1)
  simulation' [] n |> List.sort

printfn "%A" (10 |> simulation)
{% endhighlight %}

### Code output:

{% highlight text %}
Toss: (Head, Tail, Tail), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Tail, Tail, Head), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Tail, Head, Head), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Head, Head, Tail), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Tail, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Head, Head, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Head, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Head, Head, Tail), p1: (Head, Head, Head), p2: (Tail, Head, Head)
Toss: (Head, Tail, Tail), p1: (Head, Head, Head), p2: (Tail, Head, Head)
Toss: (Tail, Tail, Head), p1: (Head, Head, Head), p2: (Tail, Head, Head)
Toss: (Tail, Head, Head), p1: (Head, Head, Head), p2: (Tail, Head, Head)
Toss: (Tail, Head, Head), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Head, Head, Tail), p1: (Head, Head, Tail), p2: (Head, Head, Head)
Toss: (Head, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Head, Head, Head), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Head, Head, Tail), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Head, Tail, Tail), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Tail, Head, Head), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Head, Head, Tail), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Head, Tail, Head), p1: (Tail, Head, Tail), p2: (Head, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Tail, Tail), p2: (Head, Tail, Tail)
Toss: (Tail, Head, Head), p1: (Tail, Tail, Tail), p2: (Head, Tail, Tail)
Toss: (Head, Head, Tail), p1: (Tail, Tail, Tail), p2: (Head, Tail, Tail)
Toss: (Head, Tail, Tail), p1: (Tail, Tail, Tail), p2: (Head, Tail, Tail)
Toss: (Head, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Tail), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Tail, Head), p1: (Tail, Head, Head), p2: (Tail, Tail, Head)
Toss: (Tail, Head, Tail), p1: (Head, Tail, Tail), p2: (Head, Head, Tail)
Toss: (Head, Tail, Head), p1: (Head, Tail, Tail), p2: (Head, Head, Tail)
Toss: (Tail, Head, Head), p1: (Head, Tail, Tail), p2: (Head, Head, Tail)
Toss: (Head, Head, Tail), p1: (Head, Tail, Tail), p2: (Head, Head, Tail)
The winner(s): ["A"; "A"; "B"; "B"; "B"; "B"; "B"; "B"; "B"; "B"]
{% endhighlight %}

### References:

* Wikipedia: [Penney's game](http://en.wikipedia.org/wiki/Penney's_game)