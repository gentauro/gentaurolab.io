--- 
layout: post
title: Haskell - Scripting with Stack and Haskell
categories:
  - English
tags:
  - snippet
  - scripting
  - stack
  - haskell
time: "16:24"
---

### Files

{% highlight text %}
mon@razerRamon:~/tmp/haskell/howtoscript$ ll
total 24K
-rw-rw-r-- 1 mon mon 445 Jul  6 13:02 Logic.hs
-rwxrwxr-x 1 mon mon 457 Jul  6 14:12 Script.hs*
mon@razerRamon:~/tmp/haskell/howtoscript$
{% endhighlight %}

### Haskell Code Snippet

{% highlight haskell linenos %}
module Logic (dimensions) where

{- Dependency to a Hackage pkg: https://hackage.haskell.org/package/terminfo -}
import qualified System.Console.Terminfo.Base as Term
import           System.Console.Terminfo.Cursor

dimensions :: IO (Int,Int)
dimensions =
  do
    term <- Term.setupTermFromEnv
    
    let (Just height) = Term.getCapability term termLines
    let (Just width)  = Term.getCapability term termColumns

    return (height,width)
{% endhighlight %}

{% highlight haskell linenos %}
#!/usr/bin/env stack
{- stack
   --resolver lts-8.21
   --install-ghc
   script
   --package terminfo
   --
   -Wall -Werror
-}

module Main (main) where

{- Import local file which has a dependency to a Hackage pkg:
   https://hackage.haskell.org/package/terminfo
-}
import qualified Logic as Terminal

main :: IO ()
main =
  do
    (height,width) <- Terminal.dimensions
    
    putStrLn ("Term height: " ++ (show height) ++ " & width: " ++ (show width))
{% endhighlight %}

### Haskell Code output:

{% highlight text %}
mon@razerRamon:~/tmp/haskell/howtoscript$ ./Script.hs 
Term height: 57 & width: 199
mon@razerRamon:~/tmp/haskell/howtoscript$ 
{% endhighlight %}

### References:

* The Haskell Tool Stack:
  - [script interpreter](https://docs.haskellstack.org/en/stable/GUIDE/#script-interpreter)
  - [ghc/runghc](https://docs.haskellstack.org/en/stable/GUIDE/#ghcrunghc)
  - [Read carefully: script vs. runghc](https://docs.haskellstack.org/en/stable/GUIDE/#stack-configuration-for-scripts)
