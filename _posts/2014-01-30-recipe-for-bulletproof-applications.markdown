---
layout: post
title: Recipe for bulletproof applications with F#
categories:
  -English
tags:
  -f#
  -fsharp
  -bulletproof
  -apps
time: "22:56"
---

### Argue for robustness

So many of us working on a daily basis with F# always claim that we are able to
make more robust and bulletproof applications with fewer lines of code than we
would need if we used the C's (C,C++,C#, ...). So how do we achieve this?

I will try to explain this in a less theoretical way so people don't get lost in
translation. Besides I will provide the usual <code>foo/bar</code> examples
as well as a basic real world example.

Let's start by defining a couple of functions:

{% highlight ocaml %}
let log a b = System.Console.WriteLine(sprintf "-Log: %A (%A)" a b)

let foo x    = try 2*x |> Some with ex -> log x ex; None
let bar x    = try 2+x |> Some with ex -> log x ex; None
let foobar x = try 2/x |> Some with ex -> log x ex; None
{% endhighlight %}

{% highlight text %}
val log : a:'a -> b:'b -> unit
val foo : x:int -> int option
val bar : x:int -> int option
val foobar : x:int -> int option
{% endhighlight %}

We can all agree that the function look pretty robust right? The main operation
is performed inside a <code>try/with</code> statement, for the C's think of it
as a <code>try/catch</code> statement. Now if the operation fails,
<code>2/0</code> is possible in <code>foobar</code>, the <code>log</code>
function will be called with the input parameter <code>x</code> and the
exception <code>ex</code>. What seems a bit strange in the functions is that
both operations, <code>try/with</code>, finishes in <code>Some/None</code>. This
is one of the powerful features of F#, <code>Some/None</code> is a union type
between the type and no-value. In other words, either you have a value of the
given type <code>Some of 'a</code> or you don't any value at all
<code>None</code>. If you are familiar to ML-like languages, you will have seen
this as <code>datatype 'a option = NONE | SOME of 'a</code>, in a identical form
for OCaml as <code>type 'a option = None | Some of 'a</code> (you might be
able to argue that F# is the OCaml .NET version) and finally as <code>data Maybe
a = Just a | Nothing</code> in Haskell.

**Remark**: Just for correctness, the <code>log</code> function is implemented
with the <code>Console.WriteLine</code> method, which is threadsafe and in
combination with <code>sprintf/"%A"</code>, to make it generic.

### Robustness but verbosity

Now that we have the robust functions. lets combine a couple of them together as
we do when we write code:

{% highlight ocaml %}
2 |> foo |> bar |> foobar
{% endhighlight %}

{% highlight text %}
error FS0001: Type mismatch. Expecting a
    int option -> 'a
but given a
    int -> int option
The type 'int option' does not match the type 'int'
{% endhighlight %}

We can see that we get a type error as the function <code>bar</code> takes an
<code>int</code> as input and not an <code>int option</code> type. Let's
re-write the code in a correct way:

{% highlight ocaml %}
2
|> foo
|> function | Some v -> bar v    | None -> None
|> function | Some v -> foobar v | None -> None
{% endhighlight %}

{% highlight text %}
val it : int option = Some 0
{% endhighlight %}

I think it's easy to argument for robustness and correctness but you might
think: *"Less code you say?"*. And you are right, this kind of implementation
would be really annoying to write for every single function you would have to
pipe the result to.

### Monads to the rescue

The more theoretical approach to simplify the code but still maintaining
correctness, would be to implement the <code>Maybe Monad</code> (monads are
called <code>Computation expressions</code> in F#):

{% highlight ocaml %}
type MaybeMonad() =
  member t.Bind(m,f) =
    match m with
    | Some v -> f v
    | None -> None
  member t.Return v = Some v
let maybe = MaybeMonad()
{% endhighlight %}

{% highlight text %}
type MaybeMonad =
  class
    new : unit -> MaybeMonad
    member Bind : m:'b option * f:('b -> 'c option) -> 'c option
    member Return : v:'a -> 'a option
  end
val maybe : MaybeMonad
{% endhighlight %}

Where we can use the monad to write the previous code as:

{% highlight ocaml %}
maybe{ let! x = foo 2
       let! y = bar x
       let! z = foobar y
       return z }
{% endhighlight %}

{% highlight text %}
val it : int option = Some 0
{% endhighlight %}

By using the monad we don't have to write <code>function | Some v ->
some_function v | None -> None</code> for each time we pipe the value but, it's
still some kind of annoying having to write all the temporary variables
<code>x,y,z</code> in order to get the final result. The ideal scenario would be
to write the following code:

{% highlight ocaml %}
maybe{ return 2 |> foo |> bar |> foobar }
{% endhighlight %}

{% highlight text %}
error FS0001: Type mismatch. Expecting a
    int option -> 'a
but given a
    int -> int option
The type 'int option' does not match the type 'int'
{% endhighlight %}

But this is not possible as we need to bind the functions together. Actually
that is what <code>let!</code> does. The <code>let!</code> operator is just
*syntactic sugar* for calling the <code>Bind</code> method.

**Remark**: The <code>Maybe Monad</code> can be implemented in less verbose code
by using the built-in <code>Option.bind</code> function:

{% highlight ocaml %}
type MaybeMonad() =
  member t.Bind(m,f) = Option.bind f m
  member t.Return v = Some v
let maybe = MaybeMonad()
{% endhighlight %}

### Infix operator to the rescue (>>=)

So how do we get as close to <code>2 |> foo |> bar |> foobar</code> but without
compromising on correctness and robustness? Well the answer is quite simple

What we need to do is to introduce the following infix operator:

{% highlight ocaml %}
let (>>=) m f = Option.bind f m
{% endhighlight %}

{% highlight text %}
val ( >>= ) : m:'a option -> f:('a -> 'b option) -> 'b option
{% endhighlight %}

Now we can combine functions together in the following manner:

{% highlight ocaml %}
2 |> Some >>= foo >>= bar >>= foobar
{% endhighlight %}

{% highlight text %}
> val it : int option = Some 0
{% endhighlight %}

Which is pretty close to what we wanted to achieve, <code>2 |> foo |> bar |>
foobar</code>, right?

Another thing to have in mind when using binded functions is to think of the
bind as how [**Short-circuit
evaluation**](http://en.wikipedia.org/wiki/Short-circuit_evaluation)
works. **SCE** denotes the semantics of some Boolean operators in some
programming languages in which the second argument is executed or evaluated only
if the first argument does not suffice to determine the value of the
expression. For example: when the first argument of the <code>AND</code>
function evaluates to <code>false </code>, the overall value must be
<code>false</code>; and when the first argument of the <code>OR</code> function
evaluates to <code>true</code>, the overall value must be
<code>true</code>. Binding functions is more or less the same, where the output
from the first function is bounded to the input of the second. If the first
function returns None, then the second is never called and None is returned for
the whole expression. Let's see this in an example using <code>foobar</code> and
<code>0</code> as input:

{% highlight ocaml %}
0 |> Some >>= foobar >>= foobar >>= foobar 
{% endhighlight %}

{% highlight text %}
> -Log: 0 (System.DivideByZeroException: Division by zero 
  at FSI_0045.foobar (Int32 x) [0x00000] in <filename unknown>:0 )
val it : int option = None
{% endhighlight %}

After <code>foobar</code> throws an exception and return <code>None</code>, none
of the other following <code>foobar</code> functions are evaluated. Cool right?

### Another infix operator to the rescue (|=)

As in real life you might want to get the value of the type and use it in other
frameworks that doesn't have support for <code>Some/None </code>. What you can
do is to do something like:

{% highlight ocaml %}
42 |> Some |> function | Some v -> printfn "%A" v | None -> ()
{% endhighlight %}

{% highlight text %}
> 42
val it : unit = ()
{% endhighlight %}

or

{% highlight ocaml %}
42 |> Some |> function | Some v -> v | None -> failwith "Some error"
{% endhighlight %}

{% highlight text %}
val it : int = 42
{% endhighlight %}

This will limit your code to <code>unit = ()</code> or to throw and
exception. which would be OK if it's encapsulated in a <code>try/with</code>
statement. But sometimes you will just want be able to assign a value that means
no change in the final result of the computation. For example: <code>0</code> in
a sum of integers, <code>1</code> in a product of integers, an empty list in a
concatenation, and so on. To achieve this I usually implement the following
infix operator:

{% highlight ocaml %}
let (|=) a b = match a with | Some v -> v | None -> b
{% endhighlight %}

{% highlight text %}
val ( |= ) : a:'a option -> b:'a -> 'a
{% endhighlight %}

This will now allow us to use the value as the given type and if there is no
value then use the specified default value:

{% highlight ocaml %}
42 |> Some >>= foo >>= bar >>= foobar |= 0
{% endhighlight %}

{% highlight text %}
val it : int = 0
{% endhighlight %}

**Remark**: As with the <code>Maybe Monad</code>, this infix operator can also
be implemented in less verbose code by using the built-in
<code>Option.fold</code> function:

{% highlight ocaml %}
let (|=) a b = a |> Option.fold(fun _ x -> x) b
{% endhighlight %}

### So let's use the infix operators on a basic real world example

Now that we have the receipt to create correct and robust one-liner functions,
let's define two functions for this example. The first will return
<code>Some</code> array of even numbers from an arbitrary array. And the second
will return <code>Some</code> array of the top 10 biggest numbers from an
arbitrary array.

{% highlight ocaml %}
let even a =
  try  a |> Array.filter(fun x -> x % 2 = 0) |> Some
  with ex -> log a ex; None

let top10 a =
  try  Array.sub (a |> Array.sortBy(~-)) 0 10 |> Some
  with ex -> log a ex; None
{% endhighlight %}

{% highlight text %}
val even : a:int [] -> int [] option
val top10 : a:int [] -> int [] option
{% endhighlight %}

For the first function it's easy to argument for it to never break. If the array
doesn't contain any even numbers, <code>Some</code> empty array will be
returned. But for the second function we can see that there will always be
returned a <code>Some</code> sub-array of size 10. What will happen when the
input array is of a smaller size? Let's execute the code:

{% highlight ocaml %}
[|0 .. 2000|] |> Some >>= even >>= top10 |= Array.empty

[|0 .. 12|] |> Some >>= even >>= top10 |= Array.empty
{% endhighlight %}

{% highlight text %}
> val it : int [] =
  [|2000; 1998; 1996; 1994; 1992; 1990; 1988; 1986; 1984; 1982|]
> -Log: [|0; 2; 4; 6; 8; 10; 12|] (System.ArgumentException: 
The index is outside the legal range.
Parameter name: count
  at Microsoft.FSharp.Collections.ArrayModule.GetSubArray[Int32]
    (System.Int32[] array, Int32 startIndex, Int32 count) [0x00000] 
      in <filename unknown>:0 
  at FSI_0015.top10 (System.Int32[] a) [0x00000] in <filename unknown>:0 )
val it : int [] = [||]
{% endhighlight %}

We can see that the first evaluation returns an array of ten even numbers from
2000 to 1982 while the second returns an empty array and logs the out of
boundary exception to the console.

**Remark**: Please never write code like this, it's always more desirable to
check for the size of the array than to get an out of boundary exception. This
was just to make a point of bulletproof functions and hereby applications by
using F#.

### Conclusion

Well now that I gave you the receipt for creating small robust and bulletproof
functions, or Lego blocks as I call them, that can easily be tested for
correctness and robustness, now it's your turn to create your blocks, combine
them to create bigger blocks and make robust applications. Happy coding and
remember to have <code>fun</code>.

### Where to go from here

Finally if you want to get a deeper understanding of what is happening here,
please spend an of your life watching this amazing video:

<iframe width="720" height="540" src="//www.youtube.com/embed/ZhuHCtR3xq8"
frameborder="0"> </iframe>
