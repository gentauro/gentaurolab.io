---
layout: titlepage
title: Blogs that I read
---

### Computer Science, Mathematics and Programming in general

#### Shtetl-Optimized, The Blog of Scott Aaronson:
* Quantum computers are not known to be able to solve NP-complete problems in
polynomial time, and can be simulated classically with exponential slowdown.
* Link: [http://www.scottaaronson.com/blog/](http://www.scottaaronson.com/blog/)

#### Bret Victor, Purveyor of impossible dreams:
* Link: [http://worrydream.com/](http://worrydream.com/)

#### Algorithmic Assertions - Craig Gidney's Computer Science Blog:
* Computer Science Blows My Mind
* Link: [https://algassert.com/](https://algassert.com/)

#### Probably Done Before, The Blog of Daniel Eklund:
* Link: [http://merrigrove.blogspot.dk/](http://merrigrove.blogspot.dk)

#### Math ∪ Code, The Blog of Sahand Saba:
* Link: [http://sahandsaba.com/](http://sahandsaba.com/)

#### The Blog of DANIEL WHITTAKER (CQRS):
* Link: [http://danielwhittaker.me/](http://danielwhittaker.me/)

#### The Blog of Pablo Castellano:
* pablog.me $ cat << EOF >> /dev/blog
* Link: [Pablo Castellano](https://pablog.me/)

### Elm

#### Rundis blog (Elm + Haskell):
* Link:
  [http://rundis.github.io/blog/](http://rundis.github.io/blog/)

#### LambdaCat blog (mostly Emacs + Elm but aslo Idris, Purescript, Phoenix, ...):
* Trouble LambdaCat is a blog which explains visually and succinctly
  interesting/useful/cool Functional Programming tidbits.
* Link:
  [http://www.lambdacat.com/](http://www.lambdacat.com/)

#### Ossi Hanhinen's blog (Elm + Elixir):
* Link:
  [https://ohanhi.com/](https://ohanhi.com/)

#### Ricardo García Vega's blog (Elm + Elixir):
* Code, love & boards
* Link:
  [http://codeloveandboards.com/](http://codeloveandboards.com/)

### F♯

#### Don Syme's WebLog on F# and Related Topics :
* Link: [https://blogs.msdn.microsoft.com/dsyme/](https://blogs.msdn.microsoft.com/dsyme/)

#### Scott Wlaschin's F# for fun and profit:
* The goal of this site is to introduce .NET developers to the joys of
  functional programming
* Link: [http://fsharpforfunandprofit.com/](http://fsharpforfunandprofit.com/)

#### Sergey Tihon's blog:
* On The Roads of Brain Driven Development with F#
* Link: [https://sergeytihon.com/](https://sergeytihon.com/)

#### Tomas Petricek's blog:
* Writing about practical F# coding and programming language research
* Link: [http://tomasp.net/blog/](http://tomasp.net/blog/)

#### David Raab's blog:
* Link: [http://sidburn.github.io/](http://sidburn.github.io/)

#### Ross McKinlay's blog:
* Delusional software ramblings
* Link: [http://www.pinksquirrellabs.com/](http://www.pinksquirrellabs.com/)

#### Rash's (Steffen Forkmann) blog:
* Rash thoughts about .NET, C#, F# and Dynamics NAV
* Link: [http://www.navision-blog.de/blog/tag/f-sharp/](http://www.navision-blog.de/blog/tag/f-sharp/)

#### Mavnn's blog:
* Stuff from my brain
* Link: [http://blog.mavnn.co.uk/](http://blog.mavnn.co.uk/)
