with import <nixpkgs> { };
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    jekyll
  ];
}
  
# Isolated Development Environment using Nix  
# 
# Source: https://ariya.io/2016/06/isolated-development-environment-using-nix
