# Perseguir el conocimiento en la programación

* Datólogo (estudioso de datos) y matemático de la Universidad de CPH

* Enseño a niñ@s de entre 7-17 años a programar (Coding Pirates en CPH)

* Inicios (Amstrad CPC 464) sin estructura (BASIC y GOTO)

* Imperativo, orientado a objetos, … (C/C++/C#, Java, JavaScript, Python, …)

* Funcional (Lisp, OCaml, F#, Elm, …)

* Teoría de categorías (Haskell, Agda, …)
  * «Las Matemáticas, de las Matemáticas» -- Eugenia Cheng
  * En su libro: The Joy of Abstraction, ISBN: 9781108477222

* SPISE MISU ApS utiliza este tipo de tecnologías (PF y TC)

* Mucha suerte y sobre todo, pasarlo bien !!!

* Nos veremos en julio en Suecia para el EGOI 2023

## 2023-03-17
