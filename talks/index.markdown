---
layout: titlepage
title: Talks that I have given
---

### 2024

#### El streaming JSON se encuentra con una AST visual
* Date: 2024-03-08
* [Final nacional de la IV OIFem (2024)](https://oifem.es/inicio.html)
* [Slides](/assets/talks/2024-03-08_el-json-streaming-se-encuentra-con-una-ast-visual.pdf)


### 2023

#### How to transform AVRO IDL data to multiple PARQUET files
* Date: 2023-04-25
* [foss-north 2023](https://foss-north.se/2023/speakers-and-talks.html#rsmathiesen)
* [Slides](/assets/talks/2023-04-25_foss-north_how-to-transform-avro-idl-data-to-multiple-parquet-files.pdf)
* [Video](https://www.youtube.com/watch?v=e2PyNEg3OpQ)
* [Code on GitHub](https://github.com/PandoraJewelry/Pandora.Apache.Avro.IDL.To.Apache.Parquet) (Open Source)
* [F# Weekly #2, 2025 – Pandora Jewelry uses F#](https://sergeytihon.com/2025/01/12/f-weekly-2-2025-pandora-jewelry-uses-f/)

#### Perseguir el conocimiento en la programación
* Date: 2023-03-17
* [Final nacional de la III OIFem (2023)](https://oifem.es/inicio.html)
* [Slides](/assets/talks/2023-03-17_perseguir-el-conocimiento-en-la-programacion_readme.md)
* [Code](/assets/talks/2023-03-17_perseguir-el-conocimiento-en-la-programacion_smart.hs)


### 2022

#### Making web apps with the MVU pattern
* Date: 2022-11-07
* [Girls in IT - Zealand](https://www.linkedin.com/events/onlineworkshop-makingwebappswit6994184980100698112/)
* [Slides](/assets/talks/2022-11-07_Making-web-apps-with-the-MVU-pattern.pdf)
* *Video coming soon*


### 2021

#### Como trabajar con privacidad de datos y hacerlo bien
* Date: 2021-10-17
* [HackMadrid%27 World.Party2k21](https://worldparty.hackmadrid.org/agenda.html#agenda)
* [Video](https://www.youtube.com/watch?v=iU5dfVRREUo&t=7806s) (02:10:06 - 03:11:59)

#### Mesa redonda Programación: Programar se basa en la ciencia dura; un arte desarrollarla
* Date: 2021-10-11
* [HackMadrid%27 World.Party2k21](https://worldparty.hackmadrid.org/agenda.html#agenda)
* [Video](https://www.youtube.com/watch?v=HelCaEdki8Q)
 

### 2020

#### Domain Driven Design (DDD) with Algebraic Data Types (ADT) (English)
* Date: 2020-08-15
* [foss-north 2020 take II](https://foss-north.se/2020ii/speakers-and-talks.html#rmathiesen)
* [Slides](/assets/talks/2020-11-01_Domain_Driven_Design_DDD_with_Algebraic_Data_Types_ADT.pdf)
* [Video](https://www.youtube.com/watch?v=2kOETAB-9xI&list=PL8Xzb2qPbjDF-YPFRrKUHnRIWMTY5afzH&index=3)
 

#### Hacking on PKF files (English)
* Date: 2020-08-15
* [BornHack 2020](https://bornhack.dk/bornhack-2020/program/hacking-on-pkf-files/)
* [Slides](/assets/talks/2020-08-15_bornhack-hacking-on-pkf-files.pdf)
* [Video](https://www.youtube.com/watch?v=T_3JeI5R8gA)


#### Hacking on PKF files (Spanish)
* Date: 2020-07-16
* [HackMadrid%27](https://www.meetup.com/HackMadrid-27/events/271140296/)
* [Slides](/assets/talks/2020-07-16_hackmadrid27_hacking-on-pkf-files.pdf)
* [Source](https://gitlab.com/spisemisu/pcx-utils)
* [Video](https://www.youtube.com/watch?v=PcA9o88xH_4)


#### CrmWebApiUtil (F# dotnet) + LINQ Provider (C# dotnet) = Cloud (Docker)
* Date: 2020-01-28
* [DFDS A/S](https://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/events/267133801/)
* [Slides](/assets/talks/2020-01-28_DFDS-CrmWebApiUtil-plus-LINQProvider.pdf)


### 2019

#### Community Day
* Date: 2019-12-13
* [Siteimprove](https://www.eventbrite.com/e/community-day-registration-79501243359)
* [Slides](/assets/talks/2019-12-13_Siteimprove-Community-Day.pdf)


#### Limiting side-effects of applications at compile-time
* Date: 2019-08-12
* [BornHack](https://bornhack.dk/bornhack-2019/program/#/event/limiting-side-effects-of-applications-at-compile-time/)
* [Slides](/assets/talks/2019-08-12_bornhack-limiting-side-effects-of-applications-at-compile-time.pdf)
* [Source](https://gitlab.com/spisemisu/bornhack-demo-2019)
* [Video](https://www.youtube.com/watch?v=0uop7cyingM)


#### Limiting side-effects of applications at compile-time
* Date: 2019-04-08
* [foss-north](https://foss-north.se/2019/speakers-and-talks.html#rsotomathiesen)
* [Slides](/assets/talks/2019-04-08_foss-north-limiting-side-effects-of-applications-at-compile-time.pdf)
* [Video](https://www.youtube.com/watch?v=XXx9nDc9L5k)


#### Uniproces - Developing applications that comply with the EU GDPR by technical means:
* Date: 2019-03-19
* [GOTO Nights CPH](https://www.meetup.com/GOTO-Nights-CPH/events/258383513/)
* [Slides](/assets/talks/2019-03-19_Uniprocess_developing_applications_that_comply_with_the_EU_GDPR_by_technical_means.pdf)


### 2018

#### Timeline of MF#K lifespan: 5th year anniversary and +1000 members
* Date: 2018-11-27
* [Unity Technologies](https://unity3d.com/)
* [Slides](/assets/talks/2018-11-27_Timeline_of_MFK_lifespan.pdf)


#### Uniproces - Developing applications that comply with the EU GDPR by technical means:
* Date: 2018-11-18
* [GOTO Nights CPH](https://www.meetup.com/GOTO-Nights-CPH/events/256342503/)
* [Slides](/assets/talks/2018-11-18_Uniprocess_developing_applications_that_comply_with_the_EU_GDPR_by_technical_means.pdf)


#### Uniproces - Desarrollando aplicaciones que cumplan con el RGPD:
* Date: 2018-09-21
* [HaskellMad](https://www.meetup.com/Haskell-MAD/events/254310290/)
* [Slides](/assets/talks/2018-09-21_Uniproces_desarrollando_aplicaciones_que_cumplan_con_el_RGPD.pdf)
* [Video](https://www.youtube.com/watch?v=ihnhP2hFSU4)


#### Intro Course in Haskell:
* Date: 2018-08-08
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2018-08-08_SpiseMisu.IntroCourseHaskell.pdf)
* [Source](https://gitlab.com/spisemisu/SpiseMisu.IntroCourseHaskell)


#### How to remove Facebook content, programmatically (Round II):
* Date: 2018-06-02
* [Univate](https://univate.dk/)
* [Slides](/assets/talks/2018-06-02_How_to_remove_Facebook_content_programmatically.pdf)


#### How to remove Facebook content, programmatically (Round II):
* Date: 2018-05-29
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2018-05-29_How_to_remove_Facebook_content_programmatically.pdf)


#### EU-GDPR, Databeskyttelsesforordningen:
* Date: 2018-04-03
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2018-04-03_prosa_eugdpr-databeskyttelsesforordningen.pdf)


### 2017

#### How do we make code fun and more intuitive?:
* Date: 2017-11-08
* [Øredev 2017](https://archive.oredev.org/2017/2017/sessions/how-do-we-make-code-fun-and-more-intuitive.html)
* [Slides](/assets/talks/2017-11-06_OEredevDeveloperConference.pdf)
* [Video](https://player.vimeo.com/video/241975644)


#### Intro Course in F#:
* Date: 2017-09-21
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2017-09-21_SpiseMisu.IntroCourseFSharp.pdf)
* [Source](https://gitlab.com/spisemisu/SpiseMisu.IntroCourseFSharp)


#### MF#K Meetup Group, from none to +750 members in just four years:
* Date: 2017-09-05
* [Techfestival](http://techfestival.co/event/growing-tending-open-source-communities/)
* [Slides](/assets/talks/2017-09-05_GTOC_MFKNoneTo750MembersFourYears.pdf)


#### Scratch, the most strongly-type-safe programming language:
* Date: 2017-08-26
* [BornHack](https://bornhack.dk/)
* [Slides](/assets/talks/2017-08-26_Scratch_the_most_typesafe_programming_language.pdf)
* [Video](https://www.youtube.com/watch?v=cPFnF8vH4Nw)


#### Data protection by design and by default:
* Date: 2017-07-28
* [Rocket Labs](http://rocketlabs.dk/)
* [Slides](/assets/talks/2017-07-27_Elm_Meetup_July_Rocket_Labs_Data_protection_by_design_and_by_default.pdf)


#### Puritas, A journey of a thousand miles towards side-effect free code - Audience Level: Advanced:
* Date: 2017-04-06
* [F# eXchange 2017](https://skillsmatter.com/conferences/8053-f-sharp-exchange-2017#program)
* [Slides](/assets/talks/2017-04-06_FSharpX_Puritas_a_journey_of_a_thousand_miles_towards_side-effect_free_code.pdf)
* [Video](https://skillsmatter.com/skillscasts/9754-puritas-a-journey-of-a-thousand-miles-towards-side-effect-free-code)


#### Sign Sign (sign2x). Multi-sign documents locally (privacy) relying on mathematics for security and correctness:
* Date: 2017-03-18
* [Open Source Days](https://opensourcedays.org/community/talk?speaker_id=86)
* [Slides](/assets/talks/2017-03-18_OSD_Sign_Sign_sign2x.pdf)


#### MF#K - Functional Programming Languages - Why, where and how:
* Date: 2017-01-31
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2017-01-31_MFK_Fun_Why_where_and_how.pdf)


### 2016

#### MF#K - Semantic Versioning for .NET libraries and NuGet packages (C#/F#):
* Date: 2016-11-29
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2016-11-29_Semantic_Versioning_for_dotNET_libraries_and_NuGet_packages.pdf)


#### Type Driven Development (TDD) and Idiomatic Data Structures @ SweNug:
* Date: 2016-11-23
* [FooCafé - Malmö](http://www.foocafe.org/malmoe/events/1292-type-driven-development-tdd-and-idiomatic-data-structures)
* [Slides](/assets/talks/2016-11-23_Type_Driven_Development_TDD_and_Idiomatic_Data_Structures.pdf)
* [Video](https://www.youtube.com/watch?v=Dy7s-hoW9PU)


#### Creating libraries for Elm, lessons learned:
* Date: 2016-10-26
* [Bownty ApS](https://bownty.dk/)
* [Slides](/assets/talks/2016-10-26_creating_libraries_for_Elm_lessons_learned.pdf)
* Video: *coming soon*


#### DG - Theory into practice (DTU & ITU students as guests):
* Date: 2016-04-08
* [Delegate A/S](https://www.delegate.dk/)
* [Slides](/assets/talks/2016-04-08_DG-Teori_anvendt_praksis.pdf)


#### A combination of SharePoint and CRM to ensure atomic transactions (Travel Agency example):
* Date: 2016-03-12
* [Office 365 Saturday Denmark (OSDK)](http://www.spsevents.org/city/Copenhagen/Copenhagen2016/_layouts/15/SPSEvents/Speakers/Session.aspx?SpeakerId=1365&ID=52&source=http%3a%2f%2fwww.spsevents.org%2fcity%2fCopenhagen%2fCopenhagen2016%2f_layouts%2f15%2fSPSEvents%2fSpeakers%2fSpeaker.aspx%3fID%3d1365%26IsDlg%3d1)
* [Slides](/assets/talks/2016-03-12_SPBG_Atomic_Transactions_Travel_Agency.pdf)


#### Delegate - Showcase 2/3 years old F# Codebase:
* Date: 2016-02-23
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2016-02-23_MFK-Delegate_Showcase_Fsharp_Codebase.pdf)


#### CRMUG - MS CRM Solution Packager
* Date: 2016-02-08
* [Denmark - CRMUG - Dynamics CRM User Group](https://www.crmug.com/communities/community-home/librarydocuments?LibraryKey=0276e545-2ff7-4ae4-bfb5-08ee174880b8)
* [Slides](/assets/talks/2016-02-08_CRMUG-MSCRM_SolutionPackager.pdf)


### 2015

#### Delegate.Sandbox - I/O side-effects safe computations in F#:
* Date: 2015-09-25
* [PROSA, Forbundet af IT-Professionelle](https://www.prosa.dk/)
* [Slides](/assets/talks/2015-09-25_MFK-Delegate.Sandbox_IOside-effects_safe_computations_in_Fsharp.pdf)


#### MF#K - Introduction to F# meeting @ GroupM:
* Date: 2015-07-09
* [Groupm Denmark A/S](http://groupm.dk/)
* [Slides](/assets/talks/2015-07-09_MFK-IntroductoryFSharpGROUPM.pdf)


#### Delegate A/S - Company Friday, F# anvendt i dagligdagen (DTU studerende som gæster):
* Date: 2015-04-10
* [Delegate A/S](https://www.delegate.dk/)
* [Slides](/assets/talks/2015-04-10_DG-Fsharp_anvendt_dagligdagen.pdf)


#### MF#K - Introduction to F# ERFA meeting @ PFA:
* Date: 2015-02-27
* [PFA Pension](https://pfa.dk/)
* [Slides](/assets/talks/2015-02-27_MFK-IntroductoryFSharpPFA.pdf)


#### MF#K - Introductory Course in F#:
* Date: 2015-02-24
* [SkillsHouse ApS](http://skillshouse.dk/)
* [Slides](/assets/talks/2015-02-24_MFK-IntroductoryCourseFSharp.pdf)


### 2014

#### All-round strongly-typed approach to MS CRM - CRM3005:
* Date: 2014-11-26
* [Channel9](https://channel9.msdn.com/Events/Microsoft-Campus-Days/Microsoft-Campus-Days-2014/CRM3005)
* [Slides](/assets/talks/2014-11-26_MSCampusDays_CRM3005.pdf)


#### DAXIF# - Delegate Automated Xrm Installation Framework:
* Date: 2014-11-21
* [Denmark - CRMUG - Dynamics CRM User Group](https://www.crmug.com/communities/community-home/librarydocuments?LibraryKey=0276e545-2ff7-4ae4-bfb5-08ee174880b8)
* [Slides](/assets/talks/2014-11-21_crmug_community-DAXIF_Delegate_Automated_Xrm_Installation_Framework.pdf)


#### Hvad kan jeg (du) blive?:
* Date: 2014-08-27
* [Datalogisk Institut - Københavns Universitet](http://www.diku.dk/)
* [Slides](/assets/talks/2014-08-27_DIKU-Hvad_kan_jeg_blive.pdf)


#### MF#K - Introductory Course in F#:
* Date: 2014-04-19
* [SkillsHouse ApS](http://skillshouse.dk/)
* [Slides](/assets/talks/2014-04-19_MFK-IntroductoryCourseFSharp.pdf)


#### Team building - En software afdeling er som et rugby hold:
* Date: 2014-04-08
* [Delegate A/S](https://www.delegate.dk/)
* [Slides](/assets/talks/2014-04-08_TeamBuilding-Et_software_team_er_som_et_rugby_hold.pdf)


#### FREE F# SEMINAR FOR DEVELOPERS:
* Date: 2014-02-27 
* [SkillsHouse ApS](http://skillshouse.dk/)
* [Slides](/assets/talks/2014-02-27_MFK_FREE_FSHARP_SEMINAR_FOR_DEVELOPERS.pdf)


### 2013

#### DAXIF# - Delegate Automated Xrm Installation Framework:
* Date: 2013-12-04
* [Microsoft Dynamics Community](https://www.microsoft.com/dynamics/customer/da-DK/community.aspx)
* [Slides DA](/assets/talks/2013-12-04_crm_partner_community_DAXIF_Delegate_Automated_Xrm_Installation_Framework.pdf)
* [Slides EN](/assets/talks/2013-12-04_crm_partner_community_DAXIF_Delegate_Automated_Xrm_Installation_Framework_EN.pdf)


#### Having F#un with JavaScript (FunScript):
* Date: 2013-03-10
* [Open Source Days 2013](http://lanyrd.com/2013/opensourcedays/scdzgc/)
* [Slides](/assets/talks/2013-03-10_Having_Fun_with_JavaScript_FunScript_OSD2013.pdf)


#### First day @ Delegate A/S:
* Date: 2013-01-31
* [Delegate A/S](https://www.delegate.dk/)
* [Slides](/assets/talks/2013-01-31_intro_rsm_and_crm_technical.pdf)


### 2008

#### Studieliv på DIKU:
* Date: 2008-11-04
* [Datalogisk Institut – Københavns Universitet](http://diku.dk)
* [Slides](/assets/talks/2008-11-04_Studieliv_paa_DIKU.pdf)
